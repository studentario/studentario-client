Cliente pro Studentario
=======================

Compilation
-----------

Studentario es compilate per le systema
[QBS](https://doc.qt.io/qbs/qml-qbsmodules-qbs.html), que pote esser
configurate per exequer iste commandos:

    qbs setup-toolchains --detect
    qbs setup-qt $(which qmake) qt5
    qbs config defaultProfile qt5

Post que QBS es configurate, on pote compilar Studentario simplemente per le
commando

    qbs

Si on vole exequer le tests, on debe primo habilitar los:

    qbs resolve project.buildTests:true

e postea on pote usar le commando `qbs -p check`.


Traductiones
------------

Le projecto es traducite per le platteforma [Weblate](https://weblate.org). Pro
adder un lingua:

1. Modifica le archivo `actualisa_traductions.sh` e adde le codice del lingua
   al variabile `LINGUAS`.
2. Exeque le commando

    ./actualisa_traductions.sh

3. Adde le archivo `data/i18n/studentario_<lingua>.ts` al repositorio.
