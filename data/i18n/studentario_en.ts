<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US" sourcelanguage="ia">
<context>
    <name>Actions</name>
    <message>
        <location filename="../../src/qml/Actions.qml" line="11"/>
        <source>Lista del studentes</source>
        <translation>Students list</translation>
    </message>
    <message>
        <location filename="../../src/qml/Actions.qml" line="20"/>
        <source>Administra le studentes</source>
        <translation>Edit the students</translation>
    </message>
    <message>
        <location filename="../../src/qml/Actions.qml" line="29"/>
        <source>Administra le inseniantes</source>
        <translation>Edit the teachers</translation>
    </message>
    <message>
        <location filename="../../src/qml/Actions.qml" line="38"/>
        <source>Administra le administratores</source>
        <translation>Edit the administrators</translation>
    </message>
    <message>
        <location filename="../../src/qml/Actions.qml" line="47"/>
        <source>Administra le directores</source>
        <translation>Edit the directors</translation>
    </message>
    <message>
        <location filename="../../src/qml/Actions.qml" line="56"/>
        <source>Administra le gruppos</source>
        <translation>Manage groups</translation>
    </message>
    <message>
        <location filename="../../src/qml/Actions.qml" line="64"/>
        <source>Lista del gruppos</source>
        <translation>Group list</translation>
    </message>
    <message>
        <location filename="../../src/qml/Actions.qml" line="72"/>
        <source>Administra le lectiones</source>
        <translation>Manage lessons</translation>
    </message>
    <message>
        <location filename="../../src/qml/Actions.qml" line="80"/>
        <location filename="../../src/qml/Actions.qml" line="90"/>
        <source>Mi lectiones</source>
        <translation>My lessons</translation>
    </message>
    <message>
        <location filename="../../src/qml/Actions.qml" line="100"/>
        <source>Administra le locationes</source>
        <translation>Manage locations</translation>
    </message>
    <message>
        <location filename="../../src/qml/Actions.qml" line="108"/>
        <source>Administra le etiquettas</source>
        <translation>Manage labels</translation>
    </message>
    <message>
        <location filename="../../src/qml/Actions.qml" line="118"/>
        <source>Administra le nivellos</source>
        <translation>Manage levels</translation>
    </message>
</context>
<context>
    <name>ActivationEditor</name>
    <message>
        <location filename="../../src/qml/ActivationEditor.qml" line="28"/>
        <source>Active a partir de %1</source>
        <translation>Active since %1</translation>
    </message>
    <message>
        <location filename="../../src/qml/ActivationEditor.qml" line="29"/>
        <source>Disactivate desde %1</source>
        <translation>Inactive since %1</translation>
    </message>
    <message>
        <location filename="../../src/qml/ActivationEditor.qml" line="30"/>
        <source>Numquam activate</source>
        <translation>Never active</translation>
    </message>
    <message>
        <location filename="../../src/qml/ActivationEditor.qml" line="38"/>
        <source>Vide le historia del activationes</source>
        <translation>See the history of activations</translation>
    </message>
</context>
<context>
    <name>ActivationItem</name>
    <message>
        <location filename="../../src/qml/ActivationItem.qml" line="14"/>
        <source>Data de activation</source>
        <translation>Activation date</translation>
    </message>
    <message>
        <location filename="../../src/qml/ActivationItem.qml" line="16"/>
        <source>Lassa le usator non activate</source>
        <translation>Leave the user as inactive</translation>
    </message>
    <message>
        <location filename="../../src/qml/ActivationItem.qml" line="16"/>
        <source>Data de disactivation</source>
        <translation>Deactivation date</translation>
    </message>
    <message>
        <location filename="../../src/qml/ActivationItem.qml" line="22"/>
        <source>Active</source>
        <translation>Active</translation>
    </message>
</context>
<context>
    <name>BackButton</name>
    <message>
        <location filename="../../src/qml/BackButton.qml" line="7"/>
        <source>‹</source>
        <translation>‹</translation>
    </message>
</context>
<context>
    <name>ContactInfo</name>
    <message>
        <location filename="../../src/qml/ContactInfo.js" line="5"/>
        <source>Nomine del accompaniator</source>
        <translation>Name of parent</translation>
    </message>
    <message>
        <location filename="../../src/qml/ContactInfo.js" line="6"/>
        <source>Nomine complete</source>
        <translation>Full name</translation>
    </message>
    <message>
        <location filename="../../src/qml/ContactInfo.js" line="13"/>
        <source>Adresse</source>
        <translation>Address</translation>
    </message>
    <message>
        <location filename="../../src/qml/ContactInfo.js" line="14"/>
        <source>Adresse de residentia</source>
        <translation>Home address</translation>
    </message>
    <message>
        <location filename="../../src/qml/ContactInfo.js" line="21"/>
        <source>Telephono</source>
        <translation>Phone</translation>
    </message>
    <message>
        <location filename="../../src/qml/ContactInfo.js" line="22"/>
        <source>In formato international: &quot;+7…&quot;</source>
        <translation>Including country code: «+44…»</translation>
    </message>
    <message>
        <location filename="../../src/qml/ContactInfo.js" line="29"/>
        <source>Posta electronic</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <location filename="../../src/qml/ContactInfo.js" line="30"/>
        <source>usator@exemplo.com</source>
        <translation>user@example.org</translation>
    </message>
</context>
<context>
    <name>ContactInfoEditor</name>
    <message>
        <location filename="../../src/qml/ContactInfoEditor.qml" line="35"/>
        <source>Nulle information de contacto</source>
        <translation>No contact information</translation>
    </message>
    <message>
        <location filename="../../src/qml/ContactInfoEditor.qml" line="40"/>
        <source>Adde un nove methodo de contacto</source>
        <translation>Add a new means of contact</translation>
    </message>
</context>
<context>
    <name>ContactInfoLinePopup</name>
    <message>
        <location filename="../../src/qml/ContactInfoLinePopup.qml" line="57"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>DateGenerator</name>
    <message>
        <location filename="../../src/qml/DateGenerator.qml" line="18"/>
        <source>Lunedi</source>
        <translation>Monday</translation>
    </message>
    <message>
        <location filename="../../src/qml/DateGenerator.qml" line="19"/>
        <source>Martedi</source>
        <translation>Tuesday</translation>
    </message>
    <message>
        <location filename="../../src/qml/DateGenerator.qml" line="20"/>
        <source>Mercuridi</source>
        <translation>Wednesday</translation>
    </message>
    <message>
        <location filename="../../src/qml/DateGenerator.qml" line="21"/>
        <source>Jovedi</source>
        <translation>Thursday</translation>
    </message>
    <message>
        <location filename="../../src/qml/DateGenerator.qml" line="22"/>
        <source>Venerdi</source>
        <translation>Friday</translation>
    </message>
    <message>
        <location filename="../../src/qml/DateGenerator.qml" line="23"/>
        <source>Sabbato</source>
        <translation>Saturday</translation>
    </message>
    <message>
        <location filename="../../src/qml/DateGenerator.qml" line="24"/>
        <source>Dominica</source>
        <translation>Sunday</translation>
    </message>
    <message>
        <location filename="../../src/qml/DateGenerator.qml" line="38"/>
        <source>A partir del die:</source>
        <translation>Since day:</translation>
    </message>
    <message>
        <location filename="../../src/qml/DateGenerator.qml" line="44"/>
        <source>Usque al die:</source>
        <translation>Till day:</translation>
    </message>
    <message>
        <location filename="../../src/qml/DateGenerator.qml" line="50"/>
        <source>Initio del curso</source>
        <translation>Course start</translation>
    </message>
    <message>
        <location filename="../../src/qml/DateGenerator.qml" line="62"/>
        <source>Fin del curso</source>
        <translation>Course end</translation>
    </message>
</context>
<context>
    <name>DateLabel</name>
    <message>
        <location filename="../../src/qml/DateLabel.qml" line="8"/>
        <source>Data non specificate</source>
        <translation>Date not set</translation>
    </message>
</context>
<context>
    <name>EditActivationHistory</name>
    <message>
        <location filename="../../src/qml/EditActivationHistory.qml" line="14"/>
        <source>Historia del activationes</source>
        <translation>History of activations</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditActivationHistory.qml" line="27"/>
        <source>Adde un nove status</source>
        <translation>Add a new status</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditActivationHistory.qml" line="38"/>
        <source>Adde</source>
        <translation>Add</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditActivationHistory.qml" line="50"/>
        <source>Activationes registrate</source>
        <translation>Recorded activations</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditActivationHistory.qml" line="68"/>
        <source>Nulle cambio de stato.</source>
        <translation>No changes of state.</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditActivationHistory.qml" line="99"/>
        <source>Le usator es jam in le stato desirate. Controla le data del cambio de stato.</source>
        <translation>The user is already in the desired state. Please check the date of state change.</translation>
    </message>
</context>
<context>
    <name>EditGroup</name>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="29"/>
        <source>Modifica nomine e description</source>
        <translation>Edit name and description</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="49"/>
        <source>Modifica gruppo</source>
        <translation>Edit group</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="49"/>
        <source>Nove gruppo</source>
        <translation>New group</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="50"/>
        <source>Gruppo %1</source>
        <translation>Group %1</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="71"/>
        <source>Nomine del gruppo</source>
        <translation>Group name</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="84"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="92"/>
        <source>Informationes libere super le gruppo</source>
        <translation>Free-form information about the group</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="106"/>
        <source>Location usual:</source>
        <translation>Usual location:</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="128"/>
        <source>Inseniantes</source>
        <translation>Teachers</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="129"/>
        <source>Nulle inseniante pro le gruppo. Pressar sur le symbolo &lt;b&gt;&quot;+&quot;&lt;/b&gt; pro adder un.</source>
        <translation>No teacher set for the group. Please press on the &lt;b&gt;&quot;+&quot;&lt;/b&gt; symbol to add one.</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="140"/>
        <source>Studentes</source>
        <translation>Students</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="141"/>
        <source>Nulle studente in le gruppo. Pressar sur le symbolo &lt;b&gt;&quot;+&quot;&lt;/b&gt; pro adder un.</source>
        <translation>No students in the group. Click on the &lt;b&gt;&quot;+&quot;&lt;/b&gt; symbol to add one.</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="160"/>
        <source>Salva</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="200"/>
        <source>Adde al gruppo</source>
        <translation>Add to group</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="242"/>
        <source>Le nomine debe haber al minus 2 litteras</source>
        <translation>The name must consist of at least 2 characters</translation>
    </message>
</context>
<context>
    <name>EditLessonDates</name>
    <message>
        <location filename="../../src/qml/EditLessonDates.qml" line="15"/>
        <source>Nove lection — datas</source>
        <translation>New lesson — dates</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonDates.qml" line="33"/>
        <source>Calendario</source>
        <translation>Calendar</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonDates.qml" line="37"/>
        <source>Generation</source>
        <translation>Generation</translation>
    </message>
</context>
<context>
    <name>EditLessonMembers</name>
    <message>
        <location filename="../../src/qml/EditLessonMembers.qml" line="23"/>
        <source>Membro del gruppo</source>
        <translation>Group member</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonMembers.qml" line="32"/>
        <source>Selige le gruppos participante</source>
        <translation>Select the attending groups</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonMembers.qml" line="39"/>
        <source>Gruppos selectionate:</source>
        <translation>Selected groups:</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonMembers.qml" line="57"/>
        <source>Inseniantes</source>
        <translation>Teachers</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonMembers.qml" line="58"/>
        <source>Nulle inseniante pro le lection. Pressar sur le symbolo &lt;b&gt;&quot;+&quot;&lt;/b&gt; pro adder un.</source>
        <translation>No teacher set for the lesson. Click on the &lt;b&gt;&quot;+&quot;&lt;/b&gt; symbol to add one.</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonMembers.qml" line="59"/>
        <source>Nulle inseniante pro le lection.</source>
        <translation>No teacher set for the lesson.</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonMembers.qml" line="69"/>
        <source>Studentes</source>
        <translation>Students</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonMembers.qml" line="70"/>
        <source>Nulle studente pro le lection. Pressar sur le symbolo &lt;b&gt;&quot;+&quot;&lt;/b&gt; pro adder un.</source>
        <translation>No students in the lesson. Click on the &lt;b&gt;&quot;+&quot;&lt;/b&gt; symbol to add one.</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonMembers.qml" line="71"/>
        <source>Nulle studente pro le lection.</source>
        <translation>No students in the lesson.</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonMembers.qml" line="197"/>
        <source>Adde al gruppo</source>
        <translation>Add to group</translation>
    </message>
</context>
<context>
    <name>EditLessonPage</name>
    <message>
        <location filename="../../src/qml/EditLessonPage.qml" line="27"/>
        <source>Crea un copia</source>
        <translation>Create a copy</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonPage.qml" line="32"/>
        <source>Creation de lectiones</source>
        <translation>Lesson creation</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonPage.qml" line="33"/>
        <source>Modifica lection</source>
        <translation>Edit lesson</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonPage.qml" line="33"/>
        <source>Nove lection</source>
        <translation>New lesson</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonPage.qml" line="55"/>
        <source>Data del lection:</source>
        <translation>Lesson date:</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonPage.qml" line="73"/>
        <source>Location:</source>
        <translation>Location:</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonPage.qml" line="106"/>
        <source>Proxime</source>
        <translation>Next</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonPage.qml" line="106"/>
        <source>Salva</source>
        <translation>Save</translation>
    </message>
</context>
<context>
    <name>EditLocation</name>
    <message>
        <location filename="../../src/qml/EditLocation.qml" line="18"/>
        <source>Modifica location</source>
        <translation>Edit location</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLocation.qml" line="18"/>
        <source>Nove location</source>
        <translation>New location</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLocation.qml" line="46"/>
        <source>Nomine del location</source>
        <translation>Location name</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLocation.qml" line="57"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLocation.qml" line="65"/>
        <source>Informationes libere super le location</source>
        <translation>Free-form information about the location</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLocation.qml" line="74"/>
        <source>Color:</source>
        <translation>Colour:</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLocation.qml" line="93"/>
        <source>Salva</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLocation.qml" line="102"/>
        <source>Le nomine debe haber al minus 2 litteras</source>
        <translation>The name must consist of at least 2 characters</translation>
    </message>
</context>
<context>
    <name>EditUser</name>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="27"/>
        <source>Modifica studente</source>
        <translation>Edit student</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="28"/>
        <source>Modifica parente (accompaniator)</source>
        <translation>Edit parent (accompanying person)</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="29"/>
        <source>Modifica inseniante</source>
        <translation>Edit teacher</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="30"/>
        <source>Modifica administrator</source>
        <translation>Edit administrator</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="31"/>
        <source>Modifica director</source>
        <translation>Edit director</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="35"/>
        <source>Nove studente</source>
        <translation>New student</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="36"/>
        <source>Nove parente (accompaniator)</source>
        <translation>New parent</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="37"/>
        <source>Nove inseniante</source>
        <translation>New teacher</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="38"/>
        <source>Nove administrator</source>
        <translation>New administrator</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="39"/>
        <source>Nove director</source>
        <translation>New director</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="76"/>
        <source>Nomine complete</source>
        <translation>Full name</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="88"/>
        <source>Informationes de accesso</source>
        <translation>Access info</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="94"/>
        <source>Modifica le datos de accesso</source>
        <translation>Edit access details</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="107"/>
        <source>Identificativo de accesso (p.ex. e-posta, telephono)</source>
        <translation>Authorization ID (phone, e-mail, etc.)</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="118"/>
        <source>Nove contrasigno (vacue pro non cambiar)</source>
        <translation>New password (leave empty to keep the old one)</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="119"/>
        <source>Contrasigno</source>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="125"/>
        <source>Mantene le contrasigno currente</source>
        <translation>Keep the current password</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="126"/>
        <source>Scribe le contrasigno del usator</source>
        <translation>Enter the user&apos;s password</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="134"/>
        <source>Rolos</source>
        <translation>Roles</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="154"/>
        <source>Informationes de contacto</source>
        <translation>Contact info</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="165"/>
        <source>Data de nascentia (anno/mense/die)</source>
        <translation>Date of birth (year/month/day)</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="171"/>
        <source>Selige le data de nascentia</source>
        <translation>Pick the date of birth</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="177"/>
        <source>Parolas-clave</source>
        <translation>Keywords</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="195"/>
        <source>Stato de activitate</source>
        <translation>Activation state</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="222"/>
        <source>Salva</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="247"/>
        <source>Scribe un nomine valide</source>
        <translation>Enter a valid name</translation>
    </message>
</context>
<context>
    <name>ErrorLabel</name>
    <message>
        <location filename="../../src/qml/ErrorLabel.qml" line="18"/>
        <source>Error: %1</source>
        <translation>Error: %1</translation>
    </message>
</context>
<context>
    <name>FindGroups</name>
    <message>
        <location filename="../../src/qml/FindGroups.qml" line="16"/>
        <source>Recerca gruppos</source>
        <translation>Find groups</translation>
    </message>
    <message>
        <location filename="../../src/qml/FindGroups.qml" line="34"/>
        <source>&lt;b&gt;%1&lt;/b&gt; - %2</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; - %2</translation>
    </message>
    <message numerus="yes">
        <location filename="../../src/qml/FindGroups.qml" line="47"/>
        <source>Adde le %n gruppo(s) selectionate</source>
        <translation>
            <numerusform>Add the %n selected group</numerusform>
            <numerusform>Add the %n selected groups</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>FindMembers</name>
    <message>
        <location filename="../../src/qml/FindMembers.qml" line="27"/>
        <source>Recerca studentes</source>
        <translation>Find students</translation>
    </message>
    <message>
        <location filename="../../src/qml/FindMembers.qml" line="28"/>
        <source>Recerca parentes</source>
        <translation>Find parents</translation>
    </message>
    <message>
        <location filename="../../src/qml/FindMembers.qml" line="29"/>
        <source>Recerca inseniantes</source>
        <translation>Find teachers</translation>
    </message>
    <message>
        <location filename="../../src/qml/FindMembers.qml" line="30"/>
        <source>Recerca administratores</source>
        <translation>Find administrators</translation>
    </message>
    <message>
        <location filename="../../src/qml/FindMembers.qml" line="31"/>
        <source>Recerca directores</source>
        <translation>Find directors</translation>
    </message>
</context>
<context>
    <name>FindTags</name>
    <message>
        <location filename="../../src/qml/FindTags.qml" line="39"/>
        <source>&lt;font color=&quot;%3&quot;&gt;■&lt;/font&gt; &lt;b&gt;%1&lt;/b&gt; - %2</source>
        <translation>&lt;font color=&quot;%3&quot;&gt;■&lt;/font&gt; &lt;b&gt;%1&lt;/b&gt; - %2</translation>
    </message>
    <message numerus="yes">
        <location filename="../../src/qml/FindTags.qml" line="52"/>
        <source>Adde le %n elemento(s) selectionate</source>
        <translation>
            <numerusform>Add the %n selected element</numerusform>
            <numerusform>Add the %n selected elements</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>GroupDelegate</name>
    <message>
        <location filename="../../src/qml/GroupDelegate.qml" line="11"/>
        <source>&lt;b&gt;%1&lt;/b&gt; - %2</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; - %2</translation>
    </message>
</context>
<context>
    <name>GroupsPage</name>
    <message>
        <location filename="../../src/qml/GroupsPage.qml" line="13"/>
        <source>Gruppos</source>
        <translation>Groups</translation>
    </message>
    <message>
        <location filename="../../src/qml/GroupsPage.qml" line="51"/>
        <source>Crea un nove gruppo</source>
        <translation>Craete a new group</translation>
    </message>
    <message>
        <location filename="../../src/qml/GroupsPage.qml" line="69"/>
        <source>Confirma le remotion</source>
        <translation>Confirm deletion</translation>
    </message>
    <message>
        <location filename="../../src/qml/GroupsPage.qml" line="75"/>
        <source>Esque tu es secur que tu vole deler le gruppo «%1»?</source>
        <translation>Are you sure you want to delete group «%1»?</translation>
    </message>
</context>
<context>
    <name>HeaderSearchButton</name>
    <message>
        <location filename="../../src/qml/HeaderSearchButton.qml" line="29"/>
        <source>Cerca</source>
        <translation>Find</translation>
    </message>
    <message>
        <location filename="../../src/qml/HeaderSearchButton.qml" line="45"/>
        <source>Search filter</source>
        <translation>Search filter</translation>
    </message>
</context>
<context>
    <name>HeaderSortUsersButton</name>
    <message>
        <location filename="../../src/qml/HeaderSortUsersButton.qml" line="12"/>
        <source>Cambia ordinamento</source>
        <translation>Change sorting order</translation>
    </message>
</context>
<context>
    <name>ImportTableHeader</name>
    <message>
        <location filename="../../src/qml/ImportTableHeader.qml" line="16"/>
        <source>Nomine e familia</source>
        <translation>Name and surname</translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportTableHeader.qml" line="21"/>
        <source>Adresse</source>
        <translation>Address</translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportTableHeader.qml" line="26"/>
        <source>Telephono</source>
        <translation>Phone</translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportTableHeader.qml" line="31"/>
        <source>Posta electronic</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportTableHeader.qml" line="36"/>
        <source>Accompaniator</source>
        <translation>Accompanying person</translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportTableHeader.qml" line="41"/>
        <source>Data de nascentia</source>
        <translation>Date of birth</translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportTableHeader.qml" line="46"/>
        <source>Data de activation</source>
        <translation>Activation date</translation>
    </message>
</context>
<context>
    <name>ImportUsers</name>
    <message>
        <location filename="../../src/qml/ImportUsers.qml" line="26"/>
        <source>Importa studentes</source>
        <translation>Import students</translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportUsers.qml" line="27"/>
        <source>Importa parentes (accompaniatores)</source>
        <translation>Import parents (accompanying persons)</translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportUsers.qml" line="28"/>
        <source>Importa inseniantes</source>
        <translation>Import teachers</translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportUsers.qml" line="29"/>
        <source>Importa administratores</source>
        <translation>Import administrators</translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportUsers.qml" line="30"/>
        <source>Importa directores</source>
        <translation>Import directors</translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportUsers.qml" line="41"/>
        <source>Importation in curso…</source>
        <translation>Import in progress…</translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportUsers.qml" line="50"/>
        <source>&lt;h3&gt;Importation terminate&lt;/h3&gt;&lt;br/&gt;Importate &lt;b&gt;%1&lt;/b&gt; elementos, &lt;b&gt;%2&lt;/b&gt; errores.</source>
        <translation>&lt;h3&gt;Import finished&lt;/h3&gt;&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt; elements imported, &lt;b&gt;%2&lt;/b&gt; errors.</translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportUsers.qml" line="71"/>
        <source>Aperi un archivo CSV</source>
        <translation>Open a CSV file</translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportUsers.qml" line="130"/>
        <source>Importa</source>
        <translation>Import</translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportUsers.qml" line="138"/>
        <source>Selige un archivo CSV</source>
        <translation>Choose a CSV file</translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportUsers.qml" line="270"/>
        <source>Nulle campo seligite pro le importation</source>
        <translation>No field chosen for the import</translation>
    </message>
</context>
<context>
    <name>IntervalView</name>
    <message>
        <location filename="../../src/qml/IntervalView.qml" line="14"/>
        <source>%1 - %2</source>
        <translation>%1 - %2</translation>
    </message>
</context>
<context>
    <name>LessonAttendancePage</name>
    <message>
        <location filename="../../src/qml/LessonAttendancePage.qml" line="14"/>
        <source>Registra participation</source>
        <translation>Record attendance</translation>
    </message>
    <message>
        <location filename="../../src/qml/LessonAttendancePage.qml" line="29"/>
        <source>Lection del &lt;b&gt;%1&lt;/b&gt; al horas &lt;b&gt;%2&lt;/b&gt;</source>
        <translation>Lesson of &lt;b&gt;%1&lt;/b&gt; at &lt;b&gt;%2&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../../src/qml/LessonAttendancePage.qml" line="56"/>
        <source>Salva</source>
        <translation>Save</translation>
    </message>
</context>
<context>
    <name>LessonDelegate</name>
    <message>
        <location filename="../../src/qml/LessonDelegate.qml" line="23"/>
        <source>%1 - %2</source>
        <translation>%1 - %2</translation>
    </message>
    <message>
        <location filename="../../src/qml/LessonDelegate.qml" line="75"/>
        <source>, </source>
        <translation>, </translation>
    </message>
</context>
<context>
    <name>LessonPlannerPage</name>
    <message>
        <location filename="../../src/qml/LessonPlannerPage.qml" line="18"/>
        <source>Per location</source>
        <translation>By location</translation>
    </message>
    <message>
        <location filename="../../src/qml/LessonPlannerPage.qml" line="19"/>
        <source>Per inseniante</source>
        <translation>By teacher</translation>
    </message>
    <message>
        <location filename="../../src/qml/LessonPlannerPage.qml" line="31"/>
        <source>Programmation del lectiones</source>
        <translation>Lesson scheduling</translation>
    </message>
</context>
<context>
    <name>LessonTimeGenerator</name>
    <message>
        <location filename="../../src/qml/LessonTimeGenerator.qml" line="32"/>
        <source>Adde le lectiones</source>
        <translation>Add lessons</translation>
    </message>
    <message>
        <location filename="../../src/qml/LessonTimeGenerator.qml" line="38"/>
        <source>Data e hora del lectiones</source>
        <translation>Date and time of the lessons</translation>
    </message>
    <message>
        <location filename="../../src/qml/LessonTimeGenerator.qml" line="54"/>
        <source>Crea iste lectiones</source>
        <translation>Create these lessons</translation>
    </message>
</context>
<context>
    <name>LessonsPage</name>
    <message>
        <location filename="../../src/qml/LessonsPage.qml" line="19"/>
        <source>Programmation de lectiones</source>
        <translation>Lesson scheduling</translation>
    </message>
    <message>
        <location filename="../../src/qml/LessonsPage.qml" line="37"/>
        <source>Lectiones</source>
        <translation>Lessons</translation>
    </message>
    <message>
        <location filename="../../src/qml/LessonsPage.qml" line="85"/>
        <source>Crea nove lectiones</source>
        <translation>Create new lessons</translation>
    </message>
    <message>
        <location filename="../../src/qml/LessonsPage.qml" line="102"/>
        <source>Confirma le remotion</source>
        <translation>Confirm removal</translation>
    </message>
    <message>
        <location filename="../../src/qml/LessonsPage.qml" line="108"/>
        <source>Esque tu es secur que tu vole deler le lection?</source>
        <translation>Are you sure you want to delete the lesson?</translation>
    </message>
</context>
<context>
    <name>LocationDelegate</name>
    <message>
        <location filename="../../src/qml/LocationDelegate.qml" line="12"/>
        <source>&lt;font color=&quot;%3&quot;&gt;■&lt;/font&gt; &lt;b&gt;%1&lt;/b&gt; - %2</source>
        <translation>&lt;font color=&quot;%3&quot;&gt;■&lt;/font&gt; &lt;b&gt;%1&lt;/b&gt; - %2</translation>
    </message>
</context>
<context>
    <name>LocationPicker</name>
    <message>
        <location filename="../../src/qml/LocationPicker.qml" line="17"/>
        <source>&lt;font color=&quot;%1&quot;&gt;■&lt;/font&gt; %2</source>
        <translation>&lt;font color=&quot;%1&quot;&gt;■&lt;/font&gt; %2</translation>
    </message>
    <message>
        <location filename="../../src/qml/LocationPicker.qml" line="23"/>
        <source>Non specificate</source>
        <translation>Unspecified</translation>
    </message>
    <message>
        <location filename="../../src/qml/LocationPicker.qml" line="38"/>
        <source>Selige un location</source>
        <translation>Choose a location</translation>
    </message>
    <message>
        <location filename="../../src/qml/LocationPicker.qml" line="71"/>
        <source>Nulle location es definite.</source>
        <translation>No location defined.</translation>
    </message>
</context>
<context>
    <name>LocationsPage</name>
    <message>
        <location filename="../../src/qml/LocationsPage.qml" line="13"/>
        <source>Locations</source>
        <translation>Locations</translation>
    </message>
    <message>
        <location filename="../../src/qml/LocationsPage.qml" line="50"/>
        <source>Crea un nove location</source>
        <translation>Create a new location</translation>
    </message>
    <message>
        <location filename="../../src/qml/LocationsPage.qml" line="68"/>
        <source>Confirma le remotion</source>
        <translation>Confirm deletion</translation>
    </message>
    <message>
        <location filename="../../src/qml/LocationsPage.qml" line="74"/>
        <source>Esque tu es secur que tu vole deler le location «%1»?</source>
        <translation>Are you sure you want to delete the location «%1»?</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <location filename="../../src/qml/LoginPage.qml" line="73"/>
        <source>Identificativo de accesso:</source>
        <translation>Login:</translation>
    </message>
    <message>
        <location filename="../../src/qml/LoginPage.qml" line="79"/>
        <source>Scribe tu &quot;login&quot;</source>
        <translation>Enter your username</translation>
    </message>
    <message>
        <location filename="../../src/qml/LoginPage.qml" line="85"/>
        <source>Contrasigno:</source>
        <translation>Password:</translation>
    </message>
    <message>
        <location filename="../../src/qml/LoginPage.qml" line="91"/>
        <source>Scribe tu contrasigno</source>
        <translation>Enter your password</translation>
    </message>
    <message>
        <location filename="../../src/qml/LoginPage.qml" line="106"/>
        <source>Entra</source>
        <translation>Log in</translation>
    </message>
    <message>
        <location filename="../../src/qml/LoginPage.qml" line="123"/>
        <source>Benvenite, %1</source>
        <translation>Welcome, %1</translation>
    </message>
    <message>
        <location filename="../../src/qml/LoginPage.qml" line="131"/>
        <source>Scribe tu PIN pro entrar</source>
        <translation>Enter your PIN to log in</translation>
    </message>
    <message>
        <location filename="../../src/qml/LoginPage.qml" line="152"/>
        <source>&lt;a href=&quot;pw&quot;&gt;Clicca hic pro entrar con nomine de usator e contrasigno&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;pw&quot;&gt;Click here to log in with username and password&lt;/a&gt;</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../../src/qml/MainPage.qml" line="15"/>
        <source>Pagina principal</source>
        <translation>Landing page</translation>
    </message>
</context>
<context>
    <name>PinCreationPage</name>
    <message>
        <location filename="../../src/qml/PinCreationPage.qml" line="44"/>
        <source>Benvenite, %1</source>
        <translation>Welcome, %1</translation>
    </message>
    <message>
        <location filename="../../src/qml/PinCreationPage.qml" line="52"/>
        <source>Selige un PIN pro activar le entrata rapide</source>
        <translation>Pick a PIN for quick logins</translation>
    </message>
    <message>
        <location filename="../../src/qml/PinCreationPage.qml" line="63"/>
        <source>Scribe tu nove PIN</source>
        <translation>Enter your new PIN</translation>
    </message>
    <message>
        <location filename="../../src/qml/PinCreationPage.qml" line="75"/>
        <source>Repete le PIN</source>
        <translation>Repeat the PIN</translation>
    </message>
    <message>
        <location filename="../../src/qml/PinCreationPage.qml" line="93"/>
        <source>Le nove PIN ha essite authorisate</source>
        <translation>The new PIN has been authorised</translation>
    </message>
    <message>
        <location filename="../../src/qml/PinCreationPage.qml" line="121"/>
        <source>Le PINs non es equal!</source>
        <translation>The PINs are not matching!</translation>
    </message>
</context>
<context>
    <name>PinInput</name>
    <message>
        <location filename="../../src/qml/PinInput.qml" line="27"/>
        <source>Le PIN es troppo curte!</source>
        <translation>The PIN is too short!</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message numerus="yes">
        <location filename="../../src/user_data_formatter.cpp" line="112"/>
        <source>%1 annos</source>
        <comment>Annos del usator</comment>
        <translation>
            <numerusform>%1 year</numerusform>
            <numerusform>%1 years</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>SortPopup</name>
    <message>
        <location filename="../../src/qml/SortPopup.qml" line="13"/>
        <source>Ordine alphabetic</source>
        <translation>Alphabetic order</translation>
    </message>
    <message>
        <location filename="../../src/qml/SortPopup.qml" line="14"/>
        <source>Ordine alphabetic inverse</source>
        <translation>Inverse alphabetic order</translation>
    </message>
    <message>
        <location filename="../../src/qml/SortPopup.qml" line="15"/>
        <source>Prima le plus nove</source>
        <translation>Newer first</translation>
    </message>
    <message>
        <location filename="../../src/qml/SortPopup.qml" line="16"/>
        <source>Prima le plus vetule</source>
        <translation>Older first</translation>
    </message>
</context>
<context>
    <name>StatusChangeDelegate</name>
    <message>
        <location filename="../../src/qml/StatusChangeDelegate.qml" line="24"/>
        <source>%1: %2</source>
        <translation>%1: %2</translation>
    </message>
    <message>
        <location filename="../../src/qml/StatusChangeDelegate.qml" line="33"/>
        <source>✅ Usator activate</source>
        <translation>✅ User activated</translation>
    </message>
    <message>
        <location filename="../../src/qml/StatusChangeDelegate.qml" line="37"/>
        <source>❌ Usator deactivate</source>
        <translation>❌ User deactivated</translation>
    </message>
    <message>
        <location filename="../../src/qml/StatusChangeDelegate.qml" line="41"/>
        <source>Evento non recognoscite (%1)</source>
        <translation>Unrecognized event (%1)</translation>
    </message>
</context>
<context>
    <name>Studentario::Error</name>
    <message>
        <location filename="../../src/error.cpp" line="38"/>
        <source>Le operation ha fallite. Assecura te de esser connectite a internet</source>
        <translation>The operation failed. Make sure you are connected to the Internet</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="45"/>
        <source>Nulle error</source>
        <translation>No errors</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="46"/>
        <source>Error incognite</source>
        <translation>Unknown error</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="47"/>
        <source>Error del base de datos</source>
        <translation>Database error</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="48"/>
        <source>Il manca un campo necessari</source>
        <translation>Fill in all required fields</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="49"/>
        <source>Accesso refusate</source>
        <translation>Access denied</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="50"/>
        <source>Nomine de usator non trovate</source>
        <translation>Could not find any such user</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="51"/>
        <source>Contrasigno errate</source>
        <translation>Wrong password</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="52"/>
        <source>On necessita le authentication</source>
        <translation>You must log in first</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="53"/>
        <source>Parametros invalide</source>
        <translation>Invalid parameters</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="54"/>
        <source>Usator non trovate</source>
        <translation>Could not find any such user</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="55"/>
        <source>Gruppo non trovate</source>
        <translation>Could not find any such group</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="56"/>
        <source>Nomine de usator jam usate</source>
        <translation>User name already in use</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="57"/>
        <source>Etiquetta non trovate</source>
        <translation>Label not found</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="58"/>
        <source>Lection non trovate</source>
        <translation>Lesson not found</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="59"/>
        <source>Nota al lection non trovate</source>
        <translation>Lesson note not found</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="60"/>
        <source>Le status del usator es jam fixate a iste valor</source>
        <translation>The user status is already set to this value</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="61"/>
        <source>Record de activation non trovate</source>
        <translation>Activation record not found</translation>
    </message>
</context>
<context>
    <name>Studentario::Roles</name>
    <message>
        <location filename="../../src/roles.cpp" line="38"/>
        <source>Studente</source>
        <translation>Student</translation>
    </message>
    <message>
        <location filename="../../src/roles.cpp" line="44"/>
        <source>Genitor</source>
        <translation>Parent</translation>
    </message>
    <message>
        <location filename="../../src/roles.cpp" line="50"/>
        <source>Inseniante</source>
        <translation>Teacher</translation>
    </message>
    <message>
        <location filename="../../src/roles.cpp" line="56"/>
        <source>Administrator</source>
        <translation>Administrator</translation>
    </message>
    <message>
        <location filename="../../src/roles.cpp" line="62"/>
        <source>Director</source>
        <translation>Director</translation>
    </message>
    <message>
        <location filename="../../src/roles.cpp" line="68"/>
        <source>Capo supreme</source>
        <translation>Supreme leader</translation>
    </message>
</context>
<context>
    <name>Studentario::Utils</name>
    <message>
        <location filename="../../src/utils.cpp" line="43"/>
        <source>%1d %2h</source>
        <translation>%1d %2h</translation>
    </message>
    <message>
        <location filename="../../src/utils.cpp" line="45"/>
        <source>%1h %2m</source>
        <translation>%1h %2m</translation>
    </message>
    <message>
        <location filename="../../src/utils.cpp" line="45"/>
        <source>%1m</source>
        <translation>%1m</translation>
    </message>
</context>
<context>
    <name>TagDelegate</name>
    <message>
        <location filename="../../src/qml/TagDelegate.qml" line="14"/>
        <source>&lt;font color=&quot;%3&quot;&gt;🗀&lt;/font&gt; &lt;b&gt;%1&lt;/b&gt; - %2</source>
        <translation>&lt;font color=&quot;%3&quot;&gt;🗀&lt;/font&gt; &lt;b&gt;%1&lt;/b&gt; - %2</translation>
    </message>
    <message>
        <location filename="../../src/qml/TagDelegate.qml" line="16"/>
        <source>&lt;font color=&quot;%3&quot;&gt;■&lt;/font&gt; &lt;b&gt;%1&lt;/b&gt; - %2</source>
        <translation>&lt;font color=&quot;%3&quot;&gt;■&lt;/font&gt; &lt;b&gt;%1&lt;/b&gt; - %2</translation>
    </message>
</context>
<context>
    <name>TagEditPage</name>
    <message>
        <location filename="../../src/qml/TagEditPage.qml" line="56"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../../src/qml/TagEditPage.qml" line="74"/>
        <source>Color:</source>
        <translation>Colour:</translation>
    </message>
    <message>
        <location filename="../../src/qml/TagEditPage.qml" line="94"/>
        <source>Salva</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../../src/qml/TagEditPage.qml" line="103"/>
        <source>Le nomine debe haber al minus 2 litteras</source>
        <translation>The name must consist of at least 2 characters</translation>
    </message>
</context>
<context>
    <name>TagSelector</name>
    <message>
        <location filename="../../src/qml/TagSelector.qml" line="92"/>
        <source>Pressar sur le symbolo &lt;b&gt;&quot;+&quot;&lt;/b&gt; pro adder un.</source>
        <translation>Click on the &lt;b&gt;&quot;+&quot;&lt;/b&gt; symbol to add one.</translation>
    </message>
</context>
<context>
    <name>TagType</name>
    <message>
        <location filename="../../src/qml/TagType.js" line="5"/>
        <source>Modifica etiquetta</source>
        <translation>Edit label</translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="6"/>
        <source>Etiquettas</source>
        <translation>Labels</translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="7"/>
        <source>Nove etiquetta</source>
        <translation>New label</translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="8"/>
        <source>Recerca etiquettas</source>
        <translation>Find labels</translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="9"/>
        <source>Nomine del etiquetta</source>
        <translation>Label name</translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="10"/>
        <source>Informationes libere super le etiquetta</source>
        <translation>Free-form information about the label</translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="11"/>
        <source>Crea un nove etiquetta</source>
        <translation>Create a new label</translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="12"/>
        <source>Esque tu es secur que tu vole deler le etiquetta «%1»?</source>
        <translation>Are you sure you want to delete the label «%1»?</translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="13"/>
        <source>Nulle etiquetta assignate</source>
        <translation>No label set</translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="18"/>
        <source>Modifica nivello</source>
        <translation>Edit level</translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="19"/>
        <source>Nivellos</source>
        <translation>Levels</translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="20"/>
        <source>Nove nivello</source>
        <translation>New level</translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="21"/>
        <source>Recerca nivellos</source>
        <translation>Find levels</translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="22"/>
        <source>Nomine del nivello</source>
        <translation>Level name</translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="23"/>
        <source>Informationes libere super le nivello</source>
        <translation>Free-form information about the level</translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="24"/>
        <source>Crea un nove nivello</source>
        <translation>Create a new level</translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="25"/>
        <source>Esque tu es secur que tu vole deler le nivello «%1»?</source>
        <translation>Are you sure you want to delete the level «%1»?</translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="26"/>
        <source>Nulle nivello assignate</source>
        <translation>No level set</translation>
    </message>
</context>
<context>
    <name>TagsPage</name>
    <message>
        <location filename="../../src/qml/TagsPage.qml" line="64"/>
        <source>Confirma le remotion</source>
        <translation>Confirm deletion</translation>
    </message>
</context>
<context>
    <name>TimeInterval</name>
    <message>
        <location filename="../../src/qml/TimeInterval.qml" line="12"/>
        <source>Horario initial:</source>
        <translation>Start time:</translation>
    </message>
    <message>
        <location filename="../../src/qml/TimeInterval.qml" line="22"/>
        <source>Horario final:</source>
        <translation>End time:</translation>
    </message>
</context>
<context>
    <name>UserInfo</name>
    <message>
        <location filename="../../src/qml/UserInfo.js" line="4"/>
        <source>Informationes de contacto</source>
        <translation>Contact info</translation>
    </message>
    <message>
        <location filename="../../src/qml/UserInfo.js" line="8"/>
        <source>Data de nascentia</source>
        <translation>Date of birth</translation>
    </message>
    <message>
        <location filename="../../src/qml/UserInfo.js" line="12"/>
        <source>Data de creation</source>
        <translation>Creation date</translation>
    </message>
</context>
<context>
    <name>UserTableHeaderButton</name>
    <message>
        <location filename="../../src/qml/UserTableHeaderButton.qml" line="13"/>
        <source>Modifica aspecto del tabula</source>
        <translation>Change table look</translation>
    </message>
    <message>
        <location filename="../../src/qml/UserTableHeaderButton.qml" line="21"/>
        <source>Datos a visualisar</source>
        <translation>Data to be shown</translation>
    </message>
    <message>
        <location filename="../../src/qml/UserTableHeaderButton.qml" line="35"/>
        <source>Selige le columnas que debe esser monstrate</source>
        <translation>Choose the columns that must be shown</translation>
    </message>
</context>
<context>
    <name>UsersPage</name>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="30"/>
        <source>Importa de un archivo CSV</source>
        <translation>Import from a CSV file</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="43"/>
        <source>Studentes</source>
        <translation>Students</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="44"/>
        <source>Parentes</source>
        <translation>Parents</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="45"/>
        <source>Inseniantes</source>
        <translation>Teachers</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="46"/>
        <source>Administratores</source>
        <translation>Administrators</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="47"/>
        <source>Directores</source>
        <translation>Directors</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="84"/>
        <source>Adde un nove studente</source>
        <translation>Add a new student</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="85"/>
        <source>Adde un nove parente</source>
        <translation>Add a new parent</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="86"/>
        <source>Adde un nove inseniante</source>
        <translation>Add a new teacher</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="87"/>
        <source>Adde un nove administrator</source>
        <translation>Add a new administrator</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="88"/>
        <source>Adde un nove director</source>
        <translation>Add a new director</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="109"/>
        <source>Confirma le remotion</source>
        <translation>Confirm removal</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="115"/>
        <source>Esque tu es secur que tu vole deler «%1»?</source>
        <translation>Are you sure you want to delete «%1»?</translation>
    </message>
</context>
<context>
    <name>ViewLessonPage</name>
    <message>
        <location filename="../../src/qml/ViewLessonPage.qml" line="23"/>
        <source>Modifica lection</source>
        <translation>Edit lesson</translation>
    </message>
    <message>
        <location filename="../../src/qml/ViewLessonPage.qml" line="23"/>
        <source>Nove lection</source>
        <translation>New lesson</translation>
    </message>
    <message>
        <location filename="../../src/qml/ViewLessonPage.qml" line="45"/>
        <source>Data del lection:</source>
        <translation>Lesson date:</translation>
    </message>
    <message>
        <location filename="../../src/qml/ViewLessonPage.qml" line="79"/>
        <source>Salva</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../../src/qml/ViewLessonPage.qml" line="79"/>
        <source>Proxime</source>
        <translation>Next</translation>
    </message>
</context>
<context>
    <name>ViewUser</name>
    <message>
        <location filename="../../src/qml/ViewUser.qml" line="24"/>
        <source>Modifica informationes</source>
        <translation>Edit information</translation>
    </message>
    <message>
        <location filename="../../src/qml/ViewUser.qml" line="43"/>
        <source>Studente</source>
        <translation>Student</translation>
    </message>
    <message>
        <location filename="../../src/qml/ViewUser.qml" line="44"/>
        <source>Parente (accompaniator)</source>
        <translation>Parent (accompanying person)</translation>
    </message>
    <message>
        <location filename="../../src/qml/ViewUser.qml" line="45"/>
        <source>Inseniante</source>
        <translation>Teacher</translation>
    </message>
    <message>
        <location filename="../../src/qml/ViewUser.qml" line="46"/>
        <source>Administrator</source>
        <translation>Administrator</translation>
    </message>
    <message>
        <location filename="../../src/qml/ViewUser.qml" line="47"/>
        <source>Director</source>
        <translation>Director</translation>
    </message>
    <message>
        <location filename="../../src/qml/ViewUser.qml" line="65"/>
        <source>Nomine complete</source>
        <translation>Full name</translation>
    </message>
    <message>
        <location filename="../../src/qml/ViewUser.qml" line="79"/>
        <source>Informationes de contacto</source>
        <translation>Contact info</translation>
    </message>
    <message>
        <location filename="../../src/qml/ViewUser.qml" line="90"/>
        <source>Data de nascentia</source>
        <translation>Date of birth</translation>
    </message>
    <message>
        <location filename="../../src/qml/ViewUser.qml" line="100"/>
        <source>Parolas-clave</source>
        <translation>Keywords</translation>
    </message>
</context>
<context>
    <name>studentario</name>
    <message>
        <location filename="../../src/qml/studentario.qml" line="11"/>
        <source>Studentario</source>
        <translation>Studentario</translation>
    </message>
</context>
</TS>
