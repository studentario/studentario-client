<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nb_NO" sourcelanguage="ia">
<context>
    <name>Actions</name>
    <message>
        <location filename="../../src/qml/Actions.qml" line="10"/>
        <source>Administra le studentes</source>
        <translation>Rediger studentene</translation>
    </message>
    <message>
        <location filename="../../src/qml/Actions.qml" line="19"/>
        <source>Administra le inseniantes</source>
        <translation>Rediger lærerne</translation>
    </message>
    <message>
        <location filename="../../src/qml/Actions.qml" line="28"/>
        <source>Administra le administratores</source>
        <translation>Rediger administratorne</translation>
    </message>
    <message>
        <location filename="../../src/qml/Actions.qml" line="37"/>
        <source>Administra le directores</source>
        <translation>Rediger direktørene</translation>
    </message>
</context>
<context>
    <name>BackButton</name>
    <message>
        <location filename="../../src/qml/BackButton.qml" line="5"/>
        <source>‹</source>
        <translation>‹</translation>
    </message>
</context>
<context>
    <name>ContactInfo</name>
    <message>
        <location filename="../../src/qml/ContactInfo.js" line="4"/>
        <source>Nomine del accompaniator</source>
        <translation type="unfinished">Navngi ledsager</translation>
    </message>
    <message>
        <location filename="../../src/qml/ContactInfo.js" line="5"/>
        <source>Nomine complete</source>
        <translation>Fult navn</translation>
    </message>
    <message>
        <location filename="../../src/qml/ContactInfo.js" line="12"/>
        <source>Telephono</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <location filename="../../src/qml/ContactInfo.js" line="13"/>
        <source>In formato international: &quot;+7…&quot;</source>
        <translation type="unfinished">Med retningsnummer: «+47…»</translation>
    </message>
    <message>
        <location filename="../../src/qml/ContactInfo.js" line="20"/>
        <source>Posta electronic</source>
        <translation>E-post</translation>
    </message>
    <message>
        <location filename="../../src/qml/ContactInfo.js" line="21"/>
        <source>usator@exemplo.com</source>
        <translation>bruker@eksempel.no</translation>
    </message>
</context>
<context>
    <name>ContactInfoEditor</name>
    <message>
        <location filename="../../src/qml/ContactInfoEditor.qml" line="29"/>
        <source>Adde un nove methodo de contacto</source>
        <translation>Legg til en ny kontaktmetode</translation>
    </message>
</context>
<context>
    <name>ContactInfoLinePopup</name>
    <message>
        <location filename="../../src/qml/ContactInfoLinePopup.qml" line="48"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>EditUser</name>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="24"/>
        <source>Modifica studente</source>
        <translation>Endre student</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="25"/>
        <source>Modifica parente (accompaniator)</source>
        <translation>Endre forelder (ledsager)</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="26"/>
        <source>Modifica inseniante</source>
        <translation>Endre lærer</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="27"/>
        <source>Modifica administrator</source>
        <translation>Endre administrator</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="28"/>
        <source>Modifica director</source>
        <translation>Endre direktør</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="32"/>
        <source>Nove studente</source>
        <translation>Ny student</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="33"/>
        <source>Nove parente (accompaniator)</source>
        <translation>Ny forelder (ledsager)</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="34"/>
        <source>Nove inseniante</source>
        <translation>Ny lærer</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="35"/>
        <source>Nove administrator</source>
        <translation>Ny administrator</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="36"/>
        <source>Nove director</source>
        <translation>Ny direktør</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="70"/>
        <source>Nomine complete</source>
        <translation>Navn endret</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="82"/>
        <source>Informationes de accesso</source>
        <translation>Kontaktinfo</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="88"/>
        <source>Modifica le datos de accesso</source>
        <translation type="unfinished">Endre kontaktinfo</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="101"/>
        <source>Identificativo de accesso (p.ex. e-posta, telephono)</source>
        <translation>Kontakttype (f.eks. telefon, e-post)</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="112"/>
        <source>Nove contrasigno (vacue pro non cambiar)</source>
        <translation>Nytt passord (la stå tomt hvis du ikke vil bytte)</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="113"/>
        <source>Contrasigno</source>
        <translation>Passord</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="119"/>
        <source>Mantene le contrasigno currente</source>
        <translation>Stadfest nåværende passord</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="120"/>
        <source>Scribe le contrasigno del usator</source>
        <translation>Skriv inn brukerens passord</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="128"/>
        <source>Rolos</source>
        <translation>Roller</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="148"/>
        <source>Informationes de contacto</source>
        <translation>Kontaktinfo</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="159"/>
        <source>Parolas clave</source>
        <translation type="unfinished">Passord</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="177"/>
        <source>Salva</source>
        <translation type="unfinished">Lagre</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="186"/>
        <source>Scribe un nomine valide</source>
        <translation>Skriv inn et gyldig navn</translation>
    </message>
</context>
<context>
    <name>ErrorLabel</name>
    <message>
        <location filename="../../src/qml/ErrorLabel.qml" line="18"/>
        <source>Error: %1</source>
        <translation>Feil: %1</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <location filename="../../src/qml/LoginPage.qml" line="72"/>
        <source>Identificativo de accesso:</source>
        <translation>Innloggingsdetaljer:</translation>
    </message>
    <message>
        <location filename="../../src/qml/LoginPage.qml" line="78"/>
        <source>Scribe tu &quot;login&quot;</source>
        <translation type="unfinished">Skriv inn brukernavn</translation>
    </message>
    <message>
        <location filename="../../src/qml/LoginPage.qml" line="84"/>
        <source>Contrasigno:</source>
        <translation>Passord:</translation>
    </message>
    <message>
        <location filename="../../src/qml/LoginPage.qml" line="90"/>
        <source>Scribe tu contrasigno</source>
        <translation>Skriv inn passordet ditt</translation>
    </message>
    <message>
        <location filename="../../src/qml/LoginPage.qml" line="105"/>
        <source>Entra</source>
        <translation>Logg inn</translation>
    </message>
    <message>
        <location filename="../../src/qml/LoginPage.qml" line="121"/>
        <source>Benvenite, %1</source>
        <translation>Velkommen, %1</translation>
    </message>
    <message>
        <location filename="../../src/qml/LoginPage.qml" line="128"/>
        <source>Scribe tu PIN pro entrar</source>
        <translation>Skriv inn din PIN for å logge inn</translation>
    </message>
    <message>
        <location filename="../../src/qml/LoginPage.qml" line="149"/>
        <source>&lt;a href=&quot;pw&quot;&gt;Clicca hic pro entrar con nomine de usator e contrasigno&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;pw&quot;&gt;Klikk her for å logge inn med brukernavn og passord&lt;/a&gt;</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../../src/qml/MainPage.qml" line="15"/>
        <source>Pagina principal</source>
        <translation>Hovedside</translation>
    </message>
</context>
<context>
    <name>PinCreationPage</name>
    <message>
        <location filename="../../src/qml/PinCreationPage.qml" line="43"/>
        <source>Benvenite, %1</source>
        <translation>Velkommen, %1</translation>
    </message>
    <message>
        <location filename="../../src/qml/PinCreationPage.qml" line="51"/>
        <source>Selige un PIN pro activar le entrata rapide</source>
        <translation>Velg en PIN for hurtiginnlogging</translation>
    </message>
    <message>
        <location filename="../../src/qml/PinCreationPage.qml" line="62"/>
        <source>Scribe tu nove PIN</source>
        <translation>Skriv din nye PIN</translation>
    </message>
    <message>
        <location filename="../../src/qml/PinCreationPage.qml" line="74"/>
        <source>Repete le PIN</source>
        <translation>Gjenta PIN</translation>
    </message>
    <message>
        <location filename="../../src/qml/PinCreationPage.qml" line="92"/>
        <source>Le nove PIN ha essite authorisate</source>
        <translation type="unfinished">Ny PIN brukt til innlogging</translation>
    </message>
    <message>
        <location filename="../../src/qml/PinCreationPage.qml" line="120"/>
        <source>Le PINs non es equal!</source>
        <translation>PIN samsvarer ikke</translation>
    </message>
</context>
<context>
    <name>PinInput</name>
    <message>
        <location filename="../../src/qml/PinInput.qml" line="27"/>
        <source>Le PIN es troppo curte!</source>
        <translation>Ny PIN er for kort.</translation>
    </message>
</context>
<context>
    <name>Studentario::Error</name>
    <message>
        <location filename="../../src/error.cpp" line="38"/>
        <source>Le operation ha fallite. Assecura te de esser connectite a internet</source>
        <translation>Kunne ikke utføre handlingen. Koble til Internett først.</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="45"/>
        <source>Nulle error</source>
        <translation>Ingen feil</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="46"/>
        <source>Error incognite</source>
        <translation>Ukjent feil</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="47"/>
        <source>Error del base de datos</source>
        <translation>Datofeil</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="48"/>
        <source>Il manca un campo necessari</source>
        <translation>Det mangler et påkrevd felt</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="49"/>
        <source>Accesso refusate</source>
        <translation>Tilgang nektet</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="50"/>
        <source>Nomine de usator non trovate</source>
        <translation>Fant ikke noen bruker med dette navnet</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="51"/>
        <source>Contrasigno errate</source>
        <translation>Feil passord</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="52"/>
        <source>On necessita le authentication</source>
        <translation type="unfinished">Du må logge inn</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="53"/>
        <source>Parametros invalide</source>
        <translation>Ugyldige parameter</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="54"/>
        <source>Usator non trovate</source>
        <translation>Fant ingen slik bruker</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="55"/>
        <source>Gruppo non trovate</source>
        <translation>Fant ingen slik gruppe</translation>
    </message>
</context>
<context>
    <name>Studentario::Roles</name>
    <message>
        <location filename="../../src/roles.cpp" line="38"/>
        <source>Studente</source>
        <translation>Student</translation>
    </message>
    <message>
        <location filename="../../src/roles.cpp" line="43"/>
        <source>Genitor</source>
        <translation>Forelder</translation>
    </message>
    <message>
        <location filename="../../src/roles.cpp" line="48"/>
        <source>Inseniante</source>
        <translation>Lærer</translation>
    </message>
    <message>
        <location filename="../../src/roles.cpp" line="53"/>
        <source>Administrator</source>
        <translation>Administrator</translation>
    </message>
    <message>
        <location filename="../../src/roles.cpp" line="58"/>
        <source>Director</source>
        <translation>Direktør</translation>
    </message>
    <message>
        <location filename="../../src/roles.cpp" line="63"/>
        <source>Capo supreme</source>
        <translation>Rektor</translation>
    </message>
</context>
<context>
    <name>UsersPage</name>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="16"/>
        <source>Studentes</source>
        <translation>Studenter</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="17"/>
        <source>Parentes</source>
        <translation>Foreldre</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="18"/>
        <source>Inseniantes</source>
        <translation>Lærere</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="19"/>
        <source>Administratores</source>
        <translation>Administratorer</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="20"/>
        <source>Directores</source>
        <translation>Direktører</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="57"/>
        <source>Adde un nove studente</source>
        <translation>Legg til en ny student</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="58"/>
        <source>Adde un nove parente</source>
        <translation>Legg til en ny forelder</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="59"/>
        <source>Adde un nove inseniante</source>
        <translation>Legg til en ny lærer</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="60"/>
        <source>Adde un nove administrator</source>
        <translation>Legg til en ny administrator</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="61"/>
        <source>Adde un nove director</source>
        <translation>Legg til en ny direktør</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="82"/>
        <source>Confirma le remotion</source>
        <translation>Bekreft fjerning</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="88"/>
        <source>Esque tu es secur que tu vole deler «%1»?</source>
        <translation type="unfinished">Vil du slette «%1»?</translation>
    </message>
</context>
<context>
    <name>studentario</name>
    <message>
        <location filename="../../src/qml/studentario.qml" line="11"/>
        <source>Studentario</source>
        <translation type="unfinished">Student</translation>
    </message>
</context>
</TS>
