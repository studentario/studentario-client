<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU" sourcelanguage="ia">
<context>
    <name>Actions</name>
    <message>
        <location filename="../../src/qml/Actions.qml" line="11"/>
        <source>Lista del studentes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/Actions.qml" line="20"/>
        <source>Administra le studentes</source>
        <translation>Изменить студентов</translation>
    </message>
    <message>
        <location filename="../../src/qml/Actions.qml" line="29"/>
        <source>Administra le inseniantes</source>
        <translation>Изменить учителей</translation>
    </message>
    <message>
        <location filename="../../src/qml/Actions.qml" line="38"/>
        <source>Administra le administratores</source>
        <translation>Изменить администраторов</translation>
    </message>
    <message>
        <location filename="../../src/qml/Actions.qml" line="47"/>
        <source>Administra le directores</source>
        <translation>Изменить директории</translation>
    </message>
    <message>
        <location filename="../../src/qml/Actions.qml" line="56"/>
        <source>Administra le gruppos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/Actions.qml" line="64"/>
        <source>Lista del gruppos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/Actions.qml" line="72"/>
        <source>Administra le lectiones</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/Actions.qml" line="80"/>
        <location filename="../../src/qml/Actions.qml" line="90"/>
        <source>Mi lectiones</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/Actions.qml" line="100"/>
        <source>Administra le locationes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/Actions.qml" line="108"/>
        <source>Administra le etiquettas</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/Actions.qml" line="118"/>
        <source>Administra le nivellos</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ActivationEditor</name>
    <message>
        <location filename="../../src/qml/ActivationEditor.qml" line="28"/>
        <source>Active a partir de %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/ActivationEditor.qml" line="29"/>
        <source>Disactivate desde %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/ActivationEditor.qml" line="30"/>
        <source>Numquam activate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/ActivationEditor.qml" line="38"/>
        <source>Vide le historia del activationes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ActivationItem</name>
    <message>
        <location filename="../../src/qml/ActivationItem.qml" line="14"/>
        <source>Data de activation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/ActivationItem.qml" line="16"/>
        <source>Lassa le usator non activate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/ActivationItem.qml" line="16"/>
        <source>Data de disactivation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/ActivationItem.qml" line="22"/>
        <source>Active</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BackButton</name>
    <message>
        <location filename="../../src/qml/BackButton.qml" line="7"/>
        <source>‹</source>
        <translation>‹</translation>
    </message>
</context>
<context>
    <name>ContactInfo</name>
    <message>
        <location filename="../../src/qml/ContactInfo.js" line="5"/>
        <source>Nomine del accompaniator</source>
        <translation>Имя родителя</translation>
    </message>
    <message>
        <location filename="../../src/qml/ContactInfo.js" line="6"/>
        <source>Nomine complete</source>
        <translation>Полное имя</translation>
    </message>
    <message>
        <location filename="../../src/qml/ContactInfo.js" line="13"/>
        <source>Adresse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/ContactInfo.js" line="14"/>
        <source>Adresse de residentia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/ContactInfo.js" line="21"/>
        <source>Telephono</source>
        <translation>Телефон</translation>
    </message>
    <message>
        <location filename="../../src/qml/ContactInfo.js" line="22"/>
        <source>In formato international: &quot;+7…&quot;</source>
        <translation>В международном формате: &quot;+7...&quot;</translation>
    </message>
    <message>
        <location filename="../../src/qml/ContactInfo.js" line="29"/>
        <source>Posta electronic</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <location filename="../../src/qml/ContactInfo.js" line="30"/>
        <source>usator@exemplo.com</source>
        <translation>user@example.org</translation>
    </message>
</context>
<context>
    <name>ContactInfoEditor</name>
    <message>
        <location filename="../../src/qml/ContactInfoEditor.qml" line="35"/>
        <source>Nulle information de contacto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/ContactInfoEditor.qml" line="40"/>
        <source>Adde un nove methodo de contacto</source>
        <translation>Добавит новый способ связи</translation>
    </message>
</context>
<context>
    <name>ContactInfoLinePopup</name>
    <message>
        <location filename="../../src/qml/ContactInfoLinePopup.qml" line="57"/>
        <source>OK</source>
        <translation>ОК</translation>
    </message>
</context>
<context>
    <name>DateGenerator</name>
    <message>
        <location filename="../../src/qml/DateGenerator.qml" line="18"/>
        <source>Lunedi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/DateGenerator.qml" line="19"/>
        <source>Martedi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/DateGenerator.qml" line="20"/>
        <source>Mercuridi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/DateGenerator.qml" line="21"/>
        <source>Jovedi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/DateGenerator.qml" line="22"/>
        <source>Venerdi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/DateGenerator.qml" line="23"/>
        <source>Sabbato</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/DateGenerator.qml" line="24"/>
        <source>Dominica</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/DateGenerator.qml" line="38"/>
        <source>A partir del die:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/DateGenerator.qml" line="44"/>
        <source>Usque al die:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/DateGenerator.qml" line="50"/>
        <source>Initio del curso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/DateGenerator.qml" line="62"/>
        <source>Fin del curso</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DateLabel</name>
    <message>
        <location filename="../../src/qml/DateLabel.qml" line="8"/>
        <source>Data non specificate</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditActivationHistory</name>
    <message>
        <location filename="../../src/qml/EditActivationHistory.qml" line="14"/>
        <source>Historia del activationes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditActivationHistory.qml" line="27"/>
        <source>Adde un nove status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditActivationHistory.qml" line="38"/>
        <source>Adde</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditActivationHistory.qml" line="50"/>
        <source>Activationes registrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditActivationHistory.qml" line="68"/>
        <source>Nulle cambio de stato.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditActivationHistory.qml" line="99"/>
        <source>Le usator es jam in le stato desirate. Controla le data del cambio de stato.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditGroup</name>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="29"/>
        <source>Modifica nomine e description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="49"/>
        <source>Modifica gruppo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="49"/>
        <source>Nove gruppo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="50"/>
        <source>Gruppo %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="71"/>
        <source>Nomine del gruppo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="84"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="92"/>
        <source>Informationes libere super le gruppo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="106"/>
        <source>Location usual:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="128"/>
        <source>Inseniantes</source>
        <translation type="unfinished">Учителя</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="129"/>
        <source>Nulle inseniante pro le gruppo. Pressar sur le symbolo &lt;b&gt;&quot;+&quot;&lt;/b&gt; pro adder un.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="140"/>
        <source>Studentes</source>
        <translation type="unfinished">Ученики</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="141"/>
        <source>Nulle studente in le gruppo. Pressar sur le symbolo &lt;b&gt;&quot;+&quot;&lt;/b&gt; pro adder un.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="160"/>
        <source>Salva</source>
        <translation type="unfinished">Сохранить</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="200"/>
        <source>Adde al gruppo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditGroup.qml" line="242"/>
        <source>Le nomine debe haber al minus 2 litteras</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditLessonDates</name>
    <message>
        <location filename="../../src/qml/EditLessonDates.qml" line="15"/>
        <source>Nove lection — datas</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonDates.qml" line="33"/>
        <source>Calendario</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonDates.qml" line="37"/>
        <source>Generation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditLessonMembers</name>
    <message>
        <location filename="../../src/qml/EditLessonMembers.qml" line="23"/>
        <source>Membro del gruppo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonMembers.qml" line="32"/>
        <source>Selige le gruppos participante</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonMembers.qml" line="39"/>
        <source>Gruppos selectionate:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonMembers.qml" line="57"/>
        <source>Inseniantes</source>
        <translation type="unfinished">Учителя</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonMembers.qml" line="58"/>
        <source>Nulle inseniante pro le lection. Pressar sur le symbolo &lt;b&gt;&quot;+&quot;&lt;/b&gt; pro adder un.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonMembers.qml" line="59"/>
        <source>Nulle inseniante pro le lection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonMembers.qml" line="69"/>
        <source>Studentes</source>
        <translation type="unfinished">Ученики</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonMembers.qml" line="70"/>
        <source>Nulle studente pro le lection. Pressar sur le symbolo &lt;b&gt;&quot;+&quot;&lt;/b&gt; pro adder un.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonMembers.qml" line="71"/>
        <source>Nulle studente pro le lection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonMembers.qml" line="197"/>
        <source>Adde al gruppo</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditLessonPage</name>
    <message>
        <location filename="../../src/qml/EditLessonPage.qml" line="27"/>
        <source>Crea un copia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonPage.qml" line="32"/>
        <source>Creation de lectiones</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonPage.qml" line="33"/>
        <source>Modifica lection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonPage.qml" line="33"/>
        <source>Nove lection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonPage.qml" line="55"/>
        <source>Data del lection:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonPage.qml" line="73"/>
        <source>Location:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonPage.qml" line="106"/>
        <source>Proxime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLessonPage.qml" line="106"/>
        <source>Salva</source>
        <translation type="unfinished">Сохранить</translation>
    </message>
</context>
<context>
    <name>EditLocation</name>
    <message>
        <location filename="../../src/qml/EditLocation.qml" line="18"/>
        <source>Modifica location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLocation.qml" line="18"/>
        <source>Nove location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLocation.qml" line="46"/>
        <source>Nomine del location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLocation.qml" line="57"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLocation.qml" line="65"/>
        <source>Informationes libere super le location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLocation.qml" line="74"/>
        <source>Color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLocation.qml" line="93"/>
        <source>Salva</source>
        <translation type="unfinished">Сохранить</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditLocation.qml" line="102"/>
        <source>Le nomine debe haber al minus 2 litteras</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditUser</name>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="27"/>
        <source>Modifica studente</source>
        <translation>Править ученика</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="28"/>
        <source>Modifica parente (accompaniator)</source>
        <translation>Править родителя</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="29"/>
        <source>Modifica inseniante</source>
        <translation>Править учителя</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="30"/>
        <source>Modifica administrator</source>
        <translation>Править администратора</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="31"/>
        <source>Modifica director</source>
        <translation>Править директора</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="35"/>
        <source>Nove studente</source>
        <translation>Новый студент</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="36"/>
        <source>Nove parente (accompaniator)</source>
        <translation>Новый родитель</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="37"/>
        <source>Nove inseniante</source>
        <translation>Новый учитель</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="38"/>
        <source>Nove administrator</source>
        <translation>Новый администратор</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="39"/>
        <source>Nove director</source>
        <translation>Новый директор</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="76"/>
        <source>Nomine complete</source>
        <translation>Полное имя</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="88"/>
        <source>Informationes de accesso</source>
        <translation>Управление доступом</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="94"/>
        <source>Modifica le datos de accesso</source>
        <translation>Изменить уровень доступа</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="107"/>
        <source>Identificativo de accesso (p.ex. e-posta, telephono)</source>
        <translation>Авторизация по (телефон, e-mail, и т.д.)</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="118"/>
        <source>Nove contrasigno (vacue pro non cambiar)</source>
        <translation>Новый пароль (не заполняйте, если не хотите изменять)</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="119"/>
        <source>Contrasigno</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="125"/>
        <source>Mantene le contrasigno currente</source>
        <translation>Оставить текущий пароль</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="126"/>
        <source>Scribe le contrasigno del usator</source>
        <translation>Введите пароль пользователя</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="134"/>
        <source>Rolos</source>
        <translation>Роли</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="154"/>
        <source>Informationes de contacto</source>
        <translation>Контакты</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="165"/>
        <source>Data de nascentia (anno/mense/die)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="171"/>
        <source>Selige le data de nascentia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="177"/>
        <source>Parolas-clave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="195"/>
        <source>Stato de activitate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="222"/>
        <source>Salva</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="../../src/qml/EditUser.qml" line="247"/>
        <source>Scribe un nomine valide</source>
        <translation>Введите верное число</translation>
    </message>
</context>
<context>
    <name>ErrorLabel</name>
    <message>
        <location filename="../../src/qml/ErrorLabel.qml" line="18"/>
        <source>Error: %1</source>
        <translation>Ошибка: %1</translation>
    </message>
</context>
<context>
    <name>FindGroups</name>
    <message>
        <location filename="../../src/qml/FindGroups.qml" line="16"/>
        <source>Recerca gruppos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/FindGroups.qml" line="34"/>
        <source>&lt;b&gt;%1&lt;/b&gt; - %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../../src/qml/FindGroups.qml" line="47"/>
        <source>Adde le %n gruppo(s) selectionate</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>FindMembers</name>
    <message>
        <location filename="../../src/qml/FindMembers.qml" line="27"/>
        <source>Recerca studentes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/FindMembers.qml" line="28"/>
        <source>Recerca parentes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/FindMembers.qml" line="29"/>
        <source>Recerca inseniantes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/FindMembers.qml" line="30"/>
        <source>Recerca administratores</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/FindMembers.qml" line="31"/>
        <source>Recerca directores</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FindTags</name>
    <message>
        <location filename="../../src/qml/FindTags.qml" line="39"/>
        <source>&lt;font color=&quot;%3&quot;&gt;■&lt;/font&gt; &lt;b&gt;%1&lt;/b&gt; - %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../../src/qml/FindTags.qml" line="52"/>
        <source>Adde le %n elemento(s) selectionate</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>GroupDelegate</name>
    <message>
        <location filename="../../src/qml/GroupDelegate.qml" line="11"/>
        <source>&lt;b&gt;%1&lt;/b&gt; - %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GroupsPage</name>
    <message>
        <location filename="../../src/qml/GroupsPage.qml" line="13"/>
        <source>Gruppos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/GroupsPage.qml" line="51"/>
        <source>Crea un nove gruppo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/GroupsPage.qml" line="69"/>
        <source>Confirma le remotion</source>
        <translation type="unfinished">Подтвердить удаление</translation>
    </message>
    <message>
        <location filename="../../src/qml/GroupsPage.qml" line="75"/>
        <source>Esque tu es secur que tu vole deler le gruppo «%1»?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HeaderSearchButton</name>
    <message>
        <location filename="../../src/qml/HeaderSearchButton.qml" line="29"/>
        <source>Cerca</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/HeaderSearchButton.qml" line="45"/>
        <source>Search filter</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HeaderSortUsersButton</name>
    <message>
        <location filename="../../src/qml/HeaderSortUsersButton.qml" line="12"/>
        <source>Cambia ordinamento</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImportTableHeader</name>
    <message>
        <location filename="../../src/qml/ImportTableHeader.qml" line="16"/>
        <source>Nomine e familia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportTableHeader.qml" line="21"/>
        <source>Adresse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportTableHeader.qml" line="26"/>
        <source>Telephono</source>
        <translation type="unfinished">Телефон</translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportTableHeader.qml" line="31"/>
        <source>Posta electronic</source>
        <translation type="unfinished">E-mail</translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportTableHeader.qml" line="36"/>
        <source>Accompaniator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportTableHeader.qml" line="41"/>
        <source>Data de nascentia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportTableHeader.qml" line="46"/>
        <source>Data de activation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImportUsers</name>
    <message>
        <location filename="../../src/qml/ImportUsers.qml" line="26"/>
        <source>Importa studentes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportUsers.qml" line="27"/>
        <source>Importa parentes (accompaniatores)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportUsers.qml" line="28"/>
        <source>Importa inseniantes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportUsers.qml" line="29"/>
        <source>Importa administratores</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportUsers.qml" line="30"/>
        <source>Importa directores</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportUsers.qml" line="41"/>
        <source>Importation in curso…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportUsers.qml" line="50"/>
        <source>&lt;h3&gt;Importation terminate&lt;/h3&gt;&lt;br/&gt;Importate &lt;b&gt;%1&lt;/b&gt; elementos, &lt;b&gt;%2&lt;/b&gt; errores.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportUsers.qml" line="71"/>
        <source>Aperi un archivo CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportUsers.qml" line="130"/>
        <source>Importa</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportUsers.qml" line="138"/>
        <source>Selige un archivo CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/ImportUsers.qml" line="270"/>
        <source>Nulle campo seligite pro le importation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IntervalView</name>
    <message>
        <location filename="../../src/qml/IntervalView.qml" line="14"/>
        <source>%1 - %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LessonAttendancePage</name>
    <message>
        <location filename="../../src/qml/LessonAttendancePage.qml" line="14"/>
        <source>Registra participation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/LessonAttendancePage.qml" line="29"/>
        <source>Lection del &lt;b&gt;%1&lt;/b&gt; al horas &lt;b&gt;%2&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/LessonAttendancePage.qml" line="56"/>
        <source>Salva</source>
        <translation type="unfinished">Сохранить</translation>
    </message>
</context>
<context>
    <name>LessonDelegate</name>
    <message>
        <location filename="../../src/qml/LessonDelegate.qml" line="23"/>
        <source>%1 - %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/LessonDelegate.qml" line="75"/>
        <source>, </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LessonPlannerPage</name>
    <message>
        <location filename="../../src/qml/LessonPlannerPage.qml" line="18"/>
        <source>Per location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/LessonPlannerPage.qml" line="19"/>
        <source>Per inseniante</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/LessonPlannerPage.qml" line="31"/>
        <source>Programmation del lectiones</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LessonTimeGenerator</name>
    <message>
        <location filename="../../src/qml/LessonTimeGenerator.qml" line="32"/>
        <source>Adde le lectiones</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/LessonTimeGenerator.qml" line="38"/>
        <source>Data e hora del lectiones</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/LessonTimeGenerator.qml" line="54"/>
        <source>Crea iste lectiones</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LessonsPage</name>
    <message>
        <location filename="../../src/qml/LessonsPage.qml" line="19"/>
        <source>Programmation de lectiones</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/LessonsPage.qml" line="37"/>
        <source>Lectiones</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/LessonsPage.qml" line="85"/>
        <source>Crea nove lectiones</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/LessonsPage.qml" line="102"/>
        <source>Confirma le remotion</source>
        <translation type="unfinished">Подтвердить удаление</translation>
    </message>
    <message>
        <location filename="../../src/qml/LessonsPage.qml" line="108"/>
        <source>Esque tu es secur que tu vole deler le lection?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LocationDelegate</name>
    <message>
        <location filename="../../src/qml/LocationDelegate.qml" line="12"/>
        <source>&lt;font color=&quot;%3&quot;&gt;■&lt;/font&gt; &lt;b&gt;%1&lt;/b&gt; - %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LocationPicker</name>
    <message>
        <location filename="../../src/qml/LocationPicker.qml" line="17"/>
        <source>&lt;font color=&quot;%1&quot;&gt;■&lt;/font&gt; %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/LocationPicker.qml" line="23"/>
        <source>Non specificate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/LocationPicker.qml" line="38"/>
        <source>Selige un location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/LocationPicker.qml" line="71"/>
        <source>Nulle location es definite.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LocationsPage</name>
    <message>
        <location filename="../../src/qml/LocationsPage.qml" line="13"/>
        <source>Locations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/LocationsPage.qml" line="50"/>
        <source>Crea un nove location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/LocationsPage.qml" line="68"/>
        <source>Confirma le remotion</source>
        <translation type="unfinished">Подтвердить удаление</translation>
    </message>
    <message>
        <location filename="../../src/qml/LocationsPage.qml" line="74"/>
        <source>Esque tu es secur que tu vole deler le location «%1»?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <location filename="../../src/qml/LoginPage.qml" line="73"/>
        <source>Identificativo de accesso:</source>
        <translation>Логин:</translation>
    </message>
    <message>
        <location filename="../../src/qml/LoginPage.qml" line="79"/>
        <source>Scribe tu &quot;login&quot;</source>
        <translation>Введите имя пользователя</translation>
    </message>
    <message>
        <location filename="../../src/qml/LoginPage.qml" line="85"/>
        <source>Contrasigno:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <location filename="../../src/qml/LoginPage.qml" line="91"/>
        <source>Scribe tu contrasigno</source>
        <translation>Введите пароль</translation>
    </message>
    <message>
        <location filename="../../src/qml/LoginPage.qml" line="106"/>
        <source>Entra</source>
        <translation>Войти</translation>
    </message>
    <message>
        <location filename="../../src/qml/LoginPage.qml" line="123"/>
        <source>Benvenite, %1</source>
        <translation>Добро пожаловать, %1</translation>
    </message>
    <message>
        <location filename="../../src/qml/LoginPage.qml" line="131"/>
        <source>Scribe tu PIN pro entrar</source>
        <translation>Введите пин-код для входа</translation>
    </message>
    <message>
        <location filename="../../src/qml/LoginPage.qml" line="152"/>
        <source>&lt;a href=&quot;pw&quot;&gt;Clicca hic pro entrar con nomine de usator e contrasigno&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;pw&quot;&gt;Нажмите здесь для входа по имени и паролю&lt;/a&gt;</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../../src/qml/MainPage.qml" line="15"/>
        <source>Pagina principal</source>
        <translation>Главная страница</translation>
    </message>
</context>
<context>
    <name>PinCreationPage</name>
    <message>
        <location filename="../../src/qml/PinCreationPage.qml" line="44"/>
        <source>Benvenite, %1</source>
        <translation>Добро пожаловать, %1</translation>
    </message>
    <message>
        <location filename="../../src/qml/PinCreationPage.qml" line="52"/>
        <source>Selige un PIN pro activar le entrata rapide</source>
        <translation>Задать пин-код для быстрого входа</translation>
    </message>
    <message>
        <location filename="../../src/qml/PinCreationPage.qml" line="63"/>
        <source>Scribe tu nove PIN</source>
        <translation>Введите новый пин-код</translation>
    </message>
    <message>
        <location filename="../../src/qml/PinCreationPage.qml" line="75"/>
        <source>Repete le PIN</source>
        <translation>Повторите пин-код</translation>
    </message>
    <message>
        <location filename="../../src/qml/PinCreationPage.qml" line="93"/>
        <source>Le nove PIN ha essite authorisate</source>
        <translation>Новый пин-код задан</translation>
    </message>
    <message>
        <location filename="../../src/qml/PinCreationPage.qml" line="121"/>
        <source>Le PINs non es equal!</source>
        <translation>Пин-коды не совпадают!</translation>
    </message>
</context>
<context>
    <name>PinInput</name>
    <message>
        <location filename="../../src/qml/PinInput.qml" line="27"/>
        <source>Le PIN es troppo curte!</source>
        <translation>Пин-код слишком короткий!</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message numerus="yes">
        <location filename="../../src/user_data_formatter.cpp" line="112"/>
        <source>%1 annos</source>
        <comment>Annos del usator</comment>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>SortPopup</name>
    <message>
        <location filename="../../src/qml/SortPopup.qml" line="13"/>
        <source>Ordine alphabetic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/SortPopup.qml" line="14"/>
        <source>Ordine alphabetic inverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/SortPopup.qml" line="15"/>
        <source>Prima le plus nove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/SortPopup.qml" line="16"/>
        <source>Prima le plus vetule</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StatusChangeDelegate</name>
    <message>
        <location filename="../../src/qml/StatusChangeDelegate.qml" line="24"/>
        <source>%1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/StatusChangeDelegate.qml" line="33"/>
        <source>✅ Usator activate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/StatusChangeDelegate.qml" line="37"/>
        <source>❌ Usator deactivate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/StatusChangeDelegate.qml" line="41"/>
        <source>Evento non recognoscite (%1)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Studentario::Error</name>
    <message>
        <location filename="../../src/error.cpp" line="38"/>
        <source>Le operation ha fallite. Assecura te de esser connectite a internet</source>
        <translation>Не удалась совершить операцию. Проверьте подключение к интернету</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="45"/>
        <source>Nulle error</source>
        <translation>Ошибок нет</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="46"/>
        <source>Error incognite</source>
        <translation>Неизвестная ошибка</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="47"/>
        <source>Error del base de datos</source>
        <translation>Ошибка базы данных</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="48"/>
        <source>Il manca un campo necessari</source>
        <translation>Заполните все обязательные поля</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="49"/>
        <source>Accesso refusate</source>
        <translation>Доступ запрещён</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="50"/>
        <source>Nomine de usator non trovate</source>
        <translation>Не удалось найти этого пользователя</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="51"/>
        <source>Contrasigno errate</source>
        <translation>Неверный пароль</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="52"/>
        <source>On necessita le authentication</source>
        <translation>Сначала надо войти в систему</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="53"/>
        <source>Parametros invalide</source>
        <translation>Неверные параметры</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="54"/>
        <source>Usator non trovate</source>
        <translation>Не удалось найти этого пользователя</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="55"/>
        <source>Gruppo non trovate</source>
        <translation>Не удалось найти эту группу</translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="56"/>
        <source>Nomine de usator jam usate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="57"/>
        <source>Etiquetta non trovate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="58"/>
        <source>Lection non trovate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="59"/>
        <source>Nota al lection non trovate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="60"/>
        <source>Le status del usator es jam fixate a iste valor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/error.cpp" line="61"/>
        <source>Record de activation non trovate</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Studentario::Roles</name>
    <message>
        <location filename="../../src/roles.cpp" line="38"/>
        <source>Studente</source>
        <translation>Студент</translation>
    </message>
    <message>
        <location filename="../../src/roles.cpp" line="44"/>
        <source>Genitor</source>
        <translation>Родитель</translation>
    </message>
    <message>
        <location filename="../../src/roles.cpp" line="50"/>
        <source>Inseniante</source>
        <translation>Учитель</translation>
    </message>
    <message>
        <location filename="../../src/roles.cpp" line="56"/>
        <source>Administrator</source>
        <translation>Администратор</translation>
    </message>
    <message>
        <location filename="../../src/roles.cpp" line="62"/>
        <source>Director</source>
        <translation>Директор</translation>
    </message>
    <message>
        <location filename="../../src/roles.cpp" line="68"/>
        <source>Capo supreme</source>
        <translation>Верховный лидер</translation>
    </message>
</context>
<context>
    <name>Studentario::Utils</name>
    <message>
        <location filename="../../src/utils.cpp" line="43"/>
        <source>%1d %2h</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/utils.cpp" line="45"/>
        <source>%1h %2m</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/utils.cpp" line="45"/>
        <source>%1m</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TagDelegate</name>
    <message>
        <location filename="../../src/qml/TagDelegate.qml" line="14"/>
        <source>&lt;font color=&quot;%3&quot;&gt;🗀&lt;/font&gt; &lt;b&gt;%1&lt;/b&gt; - %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/TagDelegate.qml" line="16"/>
        <source>&lt;font color=&quot;%3&quot;&gt;■&lt;/font&gt; &lt;b&gt;%1&lt;/b&gt; - %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TagEditPage</name>
    <message>
        <location filename="../../src/qml/TagEditPage.qml" line="56"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/TagEditPage.qml" line="74"/>
        <source>Color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/TagEditPage.qml" line="94"/>
        <source>Salva</source>
        <translation type="unfinished">Сохранить</translation>
    </message>
    <message>
        <location filename="../../src/qml/TagEditPage.qml" line="103"/>
        <source>Le nomine debe haber al minus 2 litteras</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TagSelector</name>
    <message>
        <location filename="../../src/qml/TagSelector.qml" line="92"/>
        <source>Pressar sur le symbolo &lt;b&gt;&quot;+&quot;&lt;/b&gt; pro adder un.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TagType</name>
    <message>
        <location filename="../../src/qml/TagType.js" line="5"/>
        <source>Modifica etiquetta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="6"/>
        <source>Etiquettas</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="7"/>
        <source>Nove etiquetta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="8"/>
        <source>Recerca etiquettas</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="9"/>
        <source>Nomine del etiquetta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="10"/>
        <source>Informationes libere super le etiquetta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="11"/>
        <source>Crea un nove etiquetta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="12"/>
        <source>Esque tu es secur que tu vole deler le etiquetta «%1»?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="13"/>
        <source>Nulle etiquetta assignate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="18"/>
        <source>Modifica nivello</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="19"/>
        <source>Nivellos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="20"/>
        <source>Nove nivello</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="21"/>
        <source>Recerca nivellos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="22"/>
        <source>Nomine del nivello</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="23"/>
        <source>Informationes libere super le nivello</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="24"/>
        <source>Crea un nove nivello</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="25"/>
        <source>Esque tu es secur que tu vole deler le nivello «%1»?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/TagType.js" line="26"/>
        <source>Nulle nivello assignate</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TagsPage</name>
    <message>
        <location filename="../../src/qml/TagsPage.qml" line="64"/>
        <source>Confirma le remotion</source>
        <translation type="unfinished">Подтвердить удаление</translation>
    </message>
</context>
<context>
    <name>TimeInterval</name>
    <message>
        <location filename="../../src/qml/TimeInterval.qml" line="12"/>
        <source>Horario initial:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/TimeInterval.qml" line="22"/>
        <source>Horario final:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UserInfo</name>
    <message>
        <location filename="../../src/qml/UserInfo.js" line="4"/>
        <source>Informationes de contacto</source>
        <translation type="unfinished">Контакты</translation>
    </message>
    <message>
        <location filename="../../src/qml/UserInfo.js" line="8"/>
        <source>Data de nascentia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/UserInfo.js" line="12"/>
        <source>Data de creation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UserTableHeaderButton</name>
    <message>
        <location filename="../../src/qml/UserTableHeaderButton.qml" line="13"/>
        <source>Modifica aspecto del tabula</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/UserTableHeaderButton.qml" line="21"/>
        <source>Datos a visualisar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/UserTableHeaderButton.qml" line="35"/>
        <source>Selige le columnas que debe esser monstrate</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UsersPage</name>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="30"/>
        <source>Importa de un archivo CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="43"/>
        <source>Studentes</source>
        <translation>Ученики</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="44"/>
        <source>Parentes</source>
        <translation>Родители</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="45"/>
        <source>Inseniantes</source>
        <translation>Учителя</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="46"/>
        <source>Administratores</source>
        <translation>Администраторы</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="47"/>
        <source>Directores</source>
        <translation>Директора</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="84"/>
        <source>Adde un nove studente</source>
        <translation>Добавить нового студента</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="85"/>
        <source>Adde un nove parente</source>
        <translation>Добавить нового родителя</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="86"/>
        <source>Adde un nove inseniante</source>
        <translation>Добавить нового учителя</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="87"/>
        <source>Adde un nove administrator</source>
        <translation>Добавить нового администратора</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="88"/>
        <source>Adde un nove director</source>
        <translation>Добавить нового директора</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="109"/>
        <source>Confirma le remotion</source>
        <translation>Подтвердить удаление</translation>
    </message>
    <message>
        <location filename="../../src/qml/UsersPage.qml" line="115"/>
        <source>Esque tu es secur que tu vole deler «%1»?</source>
        <translation>Вы точно хотите удалить «%1»?</translation>
    </message>
</context>
<context>
    <name>ViewLessonPage</name>
    <message>
        <location filename="../../src/qml/ViewLessonPage.qml" line="23"/>
        <source>Modifica lection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/ViewLessonPage.qml" line="23"/>
        <source>Nove lection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/ViewLessonPage.qml" line="45"/>
        <source>Data del lection:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/ViewLessonPage.qml" line="79"/>
        <source>Salva</source>
        <translation type="unfinished">Сохранить</translation>
    </message>
    <message>
        <location filename="../../src/qml/ViewLessonPage.qml" line="79"/>
        <source>Proxime</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ViewUser</name>
    <message>
        <location filename="../../src/qml/ViewUser.qml" line="24"/>
        <source>Modifica informationes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/ViewUser.qml" line="43"/>
        <source>Studente</source>
        <translation type="unfinished">Студент</translation>
    </message>
    <message>
        <location filename="../../src/qml/ViewUser.qml" line="44"/>
        <source>Parente (accompaniator)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/ViewUser.qml" line="45"/>
        <source>Inseniante</source>
        <translation type="unfinished">Учитель</translation>
    </message>
    <message>
        <location filename="../../src/qml/ViewUser.qml" line="46"/>
        <source>Administrator</source>
        <translation type="unfinished">Администратор</translation>
    </message>
    <message>
        <location filename="../../src/qml/ViewUser.qml" line="47"/>
        <source>Director</source>
        <translation type="unfinished">Директор</translation>
    </message>
    <message>
        <location filename="../../src/qml/ViewUser.qml" line="65"/>
        <source>Nomine complete</source>
        <translation type="unfinished">Полное имя</translation>
    </message>
    <message>
        <location filename="../../src/qml/ViewUser.qml" line="79"/>
        <source>Informationes de contacto</source>
        <translation type="unfinished">Контакты</translation>
    </message>
    <message>
        <location filename="../../src/qml/ViewUser.qml" line="90"/>
        <source>Data de nascentia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/qml/ViewUser.qml" line="100"/>
        <source>Parolas-clave</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>studentario</name>
    <message>
        <location filename="../../src/qml/studentario.qml" line="11"/>
        <source>Studentario</source>
        <translation>Studentario</translation>
    </message>
</context>
</TS>
