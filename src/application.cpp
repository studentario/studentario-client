/*
 * Copyright (C) 2022-2023 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Studentario.
 *
 * Studentario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Studentario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LinguaLonga.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "application.h"

#include "types.h"

#include <QDebug>
#include <QDir>
#include <QIcon>
#include <QProcess>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#ifdef Q_OS_ANDROID
#include <QSvgRenderer>
#endif
#include <QTimer>
#include <QTranslator>

using namespace Studentario;

namespace Studentario {

class ApplicationPrivate {
    Q_DECLARE_PUBLIC(Application)

public:
    ApplicationPrivate(Application *q);

    static QString embeddedConfigFile();

private:
    QTranslator m_translator;
    QQmlApplicationEngine m_engine;
    Application *q_ptr;
};

} // namespace

ApplicationPrivate::ApplicationPrivate(Application *q):
    q_ptr(q)
{
    m_translator.load(QLocale(), QStringLiteral("studentario"),
                      QStringLiteral("_"), QStringLiteral(":/i18n"));
    q->installTranslator(&m_translator);

    Studentario::registerTypes();

#ifdef Q_OS_ANDROID
    QSvgRenderer unusedRenderer;
#endif

    m_engine.rootContext()->setContextProperty(
        QStringLiteral("embeddedConfigFile"), embeddedConfigFile());
}

QString ApplicationPrivate::embeddedConfigFile()
{
    QDir embeddedConfigDir(":/config/");
    const QStringList entries = embeddedConfigDir.entryList(QDir::Files);
    qDebug() << "Found config entries:" << entries;
    return entries.isEmpty() ?
        QString() : embeddedConfigDir.filePath(entries.first());
}

Application::Application(int &argc, char **argv):
    // trick to invoke setAttribute() before QGuiApplication constructor
    QGuiApplication((setAttribute(Qt::AA_EnableHighDpiScaling),
                     argc), argv),
    d_ptr(new ApplicationPrivate(this))
{
    setOrganizationDomain("it.mardy");
    setApplicationDisplayName("Studentario");
    setWindowIcon(QIcon(":/icons/studentario"));

    if (qEnvironmentVariableIsSet("APPIMAGE")) {
        QTimer::singleShot(1000, this, [this]() {
            QProcess::execute(applicationDirPath() + "/desktop-integration.sh",
                              QStringList());
        });
    }
}

Application::~Application() = default;

void Application::load()
{
    Q_D(Application);
    d->m_engine.load(QUrl(QStringLiteral("qrc:/studentario.qml")));
}
