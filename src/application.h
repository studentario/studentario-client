/*
 * Copyright (C) 2022-2023 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Studentario.
 *
 * Studentario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Studentario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LinguaLonga.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef STUDENTARIO_APPLICATION_H
#define STUDENTARIO_APPLICATION_H

#include <QGuiApplication>

namespace Studentario {

class ApplicationPrivate;
class Application: public QGuiApplication
{
    Q_OBJECT

public:
    Application(int &argc, char **argv);
    ~Application();

    void load();

private:
    QScopedPointer<ApplicationPrivate> d_ptr;
    Q_DECLARE_PRIVATE(Application)
};

} // namespace

#endif // STUDENTARIO_APPLICATION_H
