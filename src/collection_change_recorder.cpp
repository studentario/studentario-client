/*
 * Copyright (C) 2023 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Studentario.
 *
 * Studentario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Studentario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Studentario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "collection_change_recorder.h"

#include <QDebug>
#include <QJsonValue>
#include <algorithm>

using namespace Studentario;

namespace Studentario {

class CollectionChangeRecorderPrivate
{
public:
    CollectionChangeRecorderPrivate();

    int itemId(const QJsonObject &o) const { return o[m_idField].toInt(); }
    int itemId(const QJsonValue &v) const { return itemId(v.toObject()); }

    QVector<int> currentItemIds() const;

    void emitSignals(CollectionChangeRecorder *q);
    void addItem(const QJsonObject &item);
    void removeItem(int id);

private:
    friend class CollectionChangeRecorder;
    QString m_idField;
    QJsonArray m_initialItems;
    QJsonArray m_addedItems;
    QVector<int> m_removedItemIds;
    bool m_addedItemsChanged;
    bool m_removedItemsChanged;
};

} // namespace

CollectionChangeRecorderPrivate::CollectionChangeRecorderPrivate():
    m_addedItemsChanged(false),
    m_removedItemsChanged(false)
{
}

QVector<int> CollectionChangeRecorderPrivate::currentItemIds() const
{
    QVector<int> ids;

    for (const QJsonValue &v: m_initialItems) {
        int id = itemId(v);
        if (!m_removedItemIds.contains(id)) {
            ids.append(id);
        }
    }

    for (const QJsonValue &v: m_addedItems) {
        ids.append(itemId(v));
    }

    return ids;
}

void CollectionChangeRecorderPrivate::emitSignals(CollectionChangeRecorder *q)
{
    if (!m_addedItemsChanged && !m_removedItemsChanged) return;

    if (m_addedItemsChanged) {
        m_addedItemsChanged = false;
        Q_EMIT q->addedItemsChanged();
    }
    if (m_removedItemsChanged) {
        m_removedItemsChanged = false;
        Q_EMIT q->removedItemsChanged();
    }
    Q_EMIT q->currentItemsChanged();
}

void CollectionChangeRecorderPrivate::addItem(const QJsonObject &item)
{
    int id = itemId(item);

    if (m_removedItemIds.removeOne(id)) {
        m_removedItemsChanged = true;
        return;
    }

    QVector<int> itemIds = currentItemIds();
    if (Q_UNLIKELY(itemIds.contains(id))) {
        qWarning() << "Attempt to add existing id" << id;
        return;
    }

    m_addedItems.append(item);
    m_addedItemsChanged = true;
}

void CollectionChangeRecorderPrivate::removeItem(int id)
{
    auto i = std::find_if(m_addedItems.begin(), m_addedItems.end(),
                          [this, id](const QJsonValue &v) {
        return itemId(v) == id;
    });

    if (i != m_addedItems.end()) {
        m_addedItems.erase(i);
        m_addedItemsChanged = true;
        return;
    }

    QVector<int> itemIds = currentItemIds();
    if (Q_UNLIKELY(!itemIds.contains(id))) {
        qWarning() << "Attempt to remove missing id" << id;
        return;
    }

    m_removedItemIds.append(id);
    m_removedItemsChanged = true;
}

CollectionChangeRecorder::CollectionChangeRecorder(QObject *parent):
    QObject(parent),
    d_ptr(new CollectionChangeRecorderPrivate())
{
}

CollectionChangeRecorder::~CollectionChangeRecorder() = default;

void CollectionChangeRecorder::setInitialItems(const QJsonArray &items)
{
    Q_D(CollectionChangeRecorder);
    d->m_initialItems = items;
    Q_EMIT initialItemsChanged();
    Q_EMIT currentItemsChanged();
}

QJsonArray CollectionChangeRecorder::initialItems() const
{
    Q_D(const CollectionChangeRecorder);
    return d->m_initialItems;
}

void CollectionChangeRecorder::setIdField(const QString &idField)
{
    Q_D(CollectionChangeRecorder);
    d->m_idField = idField;
    Q_EMIT idFieldChanged();
}

QString CollectionChangeRecorder::idField() const
{
    Q_D(const CollectionChangeRecorder);
    return d->m_idField;
}

QJsonArray CollectionChangeRecorder::currentItems() const
{
    Q_D(const CollectionChangeRecorder);

    QJsonArray items;
    for (const QJsonValue &v: d->m_initialItems) {
        int id = d->itemId(v);
        if (!d->m_removedItemIds.contains(id)) {
            items.append(v);
        }
    }

    for (const QJsonValue &v: d->m_addedItems) {
        items.append(v);
    }

    return items;
}

QVector<int> CollectionChangeRecorder::currentItemIds() const
{
    Q_D(const CollectionChangeRecorder);
    return d->currentItemIds();
}

QJsonArray CollectionChangeRecorder::addedItems() const
{
    Q_D(const CollectionChangeRecorder);
    return d->m_addedItems;
}

QVector<int> CollectionChangeRecorder::addedItemIds() const
{
    Q_D(const CollectionChangeRecorder);
    QVector<int> ids;
    for (const QJsonValue &v: d->m_addedItems) {
        ids.append(d->itemId(v));
    }
    return ids;
}

QJsonArray CollectionChangeRecorder::removedItems() const
{
    Q_D(const CollectionChangeRecorder);
    QJsonArray items;
    for (const QJsonValue &v: d->m_initialItems) {
        if (d->m_removedItemIds.contains(d->itemId(v))) {
            items.append(v);
        }
    }
    return items;
}

QVector<int> CollectionChangeRecorder::removedItemIds() const
{
    Q_D(const CollectionChangeRecorder);
    return d->m_removedItemIds;
}

void CollectionChangeRecorder::addItems(const QJsonArray &items)
{
    Q_D(CollectionChangeRecorder);
    for (const QJsonValue &v: items) {
        d->addItem(v.toObject());
    }
    d->emitSignals(this);
}

void CollectionChangeRecorder::addItem(const QJsonObject &item)
{
    Q_D(CollectionChangeRecorder);
    d->addItem(item);
    d->emitSignals(this);
}

void CollectionChangeRecorder::removeItem(int id)
{
    Q_D(CollectionChangeRecorder);
    d->removeItem(id);
    d->emitSignals(this);
}
