/*
 * Copyright (C) 2023 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Studentario.
 *
 * Studentario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Studentario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Studentario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STUDENTARIO_COLLECTION_CHANGE_RECORDER_H
#define STUDENTARIO_COLLECTION_CHANGE_RECORDER_H

#include <QJsonArray>
#include <QJsonObject>
#include <QObject>
#include <QScopedPointer>
#include <QString>
#include <QVector>

namespace Studentario {

class CollectionChangeRecorderPrivate;
class CollectionChangeRecorder: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QJsonArray initialItems READ initialItems WRITE setInitialItems
               NOTIFY initialItemsChanged)
    Q_PROPERTY(QJsonArray currentItems READ currentItems
               NOTIFY currentItemsChanged)
    Q_PROPERTY(QVector<int> currentItemIds READ currentItemIds
               NOTIFY currentItemsChanged)
    Q_PROPERTY(QJsonArray addedItems READ addedItems NOTIFY addedItemsChanged)
    Q_PROPERTY(QVector<int> addedItemIds READ addedItemIds
               NOTIFY addedItemsChanged)
    Q_PROPERTY(QJsonArray removedItems READ removedItems
               NOTIFY removedItemsChanged)
    Q_PROPERTY(QVector<int> removedItemIds READ removedItemIds
               NOTIFY removedItemsChanged)
    Q_PROPERTY(QString idField READ idField WRITE setIdField
               NOTIFY idFieldChanged)

public:
    CollectionChangeRecorder(QObject *parent = nullptr);
    virtual ~CollectionChangeRecorder();

    void setInitialItems(const QJsonArray &items);
    QJsonArray initialItems() const;

    void setIdField(const QString &idField);
    QString idField() const;

    QJsonArray currentItems() const;
    QVector<int> currentItemIds() const;

    QJsonArray addedItems() const;
    QVector<int> addedItemIds() const;

    QJsonArray removedItems() const;
    QVector<int> removedItemIds() const;

    Q_INVOKABLE void addItems(const QJsonArray &items);
    Q_INVOKABLE void addItem(const QJsonObject &item);

    Q_INVOKABLE void removeItem(int id);

Q_SIGNALS:
    void initialItemsChanged();
    void currentItemsChanged();
    void addedItemsChanged();
    void removedItemsChanged();
    void idFieldChanged();

private:
    QScopedPointer<CollectionChangeRecorderPrivate> d_ptr;
    Q_DECLARE_PRIVATE(CollectionChangeRecorder)
};

} // namespace

#endif // STUDENTARIO_COLLECTION_CHANGE_RECORDER_H
