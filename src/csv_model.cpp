/*
 * Copyright (C) 2023 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Studentario.
 *
 * Studentario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Studentario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Studentario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "csv_model.h"

#include "csv_parser.h"

#include <QDate>
#include <QDebug>
#include <QFile>
#include <QLocale>
#include <QMetaType>
#include <QVector>

using namespace Studentario;

namespace Studentario {

class CsvModelPrivate
{
    Q_DECLARE_PUBLIC(CsvModel)

public:
    CsvModelPrivate(CsvModel *q);

    void setFilePath(const QString &filePath);

    void fetchMore();

private:
    QHash<int, QByteArray> m_roles;
    QFile m_file;
    CsvParser m_parser;
    QVector<QStringList> m_data;
    QHash<int, int> m_columnTypes; // column index -> QMetaType ID
    QStringList m_dateFormats;
    CsvModel *q_ptr;
};

} // namespace

CsvModelPrivate::CsvModelPrivate(CsvModel *q):
    q_ptr(q)
{
    m_roles[CsvModel::ValueRole] = "value";
    m_roles[CsvModel::TypeRole] = "type";

    QLocale locale;
    m_dateFormats = QStringList {
        "yyyy-MM-dd",
        "yyyy/MM/dd",
        "dd-MM-yyyy",
        "dd/MM/yyyy",
        locale.dateFormat(QLocale::LongFormat),
        locale.dateFormat(QLocale::ShortFormat),
        "yyyy",
    };
}

void CsvModelPrivate::setFilePath(const QString &filePath)
{
    m_data.clear();
    m_columnTypes.clear();
    if (m_file.isOpen()) {
        m_file.close();
    }
    m_file.setFileName(filePath);
    bool ok = m_file.open(QIODevice::ReadOnly);
    if (Q_LIKELY(ok)) {
        m_parser.setInputDevice(&m_file);
#if (QT_VERSION < QT_VERSION_CHECK(5, 14, 0))
        // Workaround QTBUG-78273
        int initialRows = 5000;
#else
        int initialRows = 50;
#endif
        for (int i = 0; i < initialRows; i++) {
            QStringList row = m_parser.readNextRow();
            if (row.isEmpty()) break;
            m_data.append(row);
        }
    } else {
        qWarning() << "Error opening" << filePath << m_file.errorString();
        m_data.clear();
    }
}

void CsvModelPrivate::fetchMore()
{
    Q_Q(CsvModel);

    int oldCount = m_data.count();
    for (int i = 0; i < 10; i++) {
        QStringList row = m_parser.readNextRow();
        if (row.isEmpty()) break;
        m_data.append(row);
    }
    int newCount = m_data.count();
    if (newCount > oldCount) {
        q->beginInsertRows(QModelIndex(), oldCount, newCount - 1);
        q->endInsertRows();
    }
}

CsvModel::CsvModel(QObject *parent):
    QAbstractItemModel(parent),
    d_ptr(new CsvModelPrivate(this))
{
    QObject::connect(this, &QAbstractItemModel::modelReset,
                     this, &CsvModel::countChanged);
    QObject::connect(this, &QAbstractItemModel::rowsInserted,
                     this, &CsvModel::countChanged);
}

CsvModel::~CsvModel() = default;

void CsvModel::setFilePath(const QString &filePath)
{
    Q_D(CsvModel);
    beginResetModel();
    d->setFilePath(filePath);
    endResetModel();
    Q_EMIT filePathChanged();
}

QString CsvModel::filePath() const
{
    Q_D(const CsvModel);
    return d->m_file.fileName();
}

QStringList CsvModel::columnTitles() const
{
    Q_D(const CsvModel);
    return d->m_parser.columnTitles();
}

void CsvModel::setColumnType(int col, const QString &dataType)
{
    Q_D(CsvModel);
    int typeId = QMetaType::type(dataType.toUtf8());
    if (Q_UNLIKELY(typeId == 0)) {
        qWarning() << "Unknown data type" << dataType;
        return;
    }
    d->m_columnTypes[col] = typeId;
    Q_EMIT dataChanged(index(0, col), index(rowCount(), col));
}

QVariant CsvModel::get(int row, int col, const QString &roleName) const
{
    int role = roleNames().key(roleName.toLatin1(), -1);
    return data(index(row, col), role);
}

int CsvModel::rowCount(const QModelIndex &) const
{
    Q_D(const CsvModel);
    return d->m_data.count();
}

QVariant CsvModel::data(const QModelIndex &index, int role) const
{
    Q_D(const CsvModel);

    int row = index.row();
    int col = index.column();
    if (Q_UNLIKELY(row < 0 || row >= rowCount() ||
                   col < 0 || col >= columnCount())) {
        qWarning() << "Invalid index" << index;
        return QVariant();
    }

    const QString &stringData = d->m_data[row][col];
    int typeId = d->m_columnTypes.value(col, QMetaType::QString);

    switch (role) {
    case ValueRole:
        if (typeId == QMetaType::QString) {
            return stringData;
        } else if (typeId == QMetaType::QDate) {
            for (const QString &format: d->m_dateFormats) {
                QDate date = QDate::fromString(stringData, format);
                if (date.isValid()) return date;
            }
            qWarning() << "Could not parse date" << stringData;
        } else {
            qWarning() << "Unsupported type" << typeId;
        }
        return stringData;
    case TypeRole:
        return QMetaType::typeName(typeId);
    default:
        qWarning() << "Unknown role ID:" << role;
        return QVariant();
    }
}

QHash<int, QByteArray> CsvModel::roleNames() const
{
    Q_D(const CsvModel);
    return d->m_roles;
}

int CsvModel::columnCount(const QModelIndex &) const
{
    return columnTitles().count();
}

QModelIndex CsvModel::index(int row, int col, const QModelIndex &parent) const
{
    if (Q_UNLIKELY(row < 0 || row >= rowCount(parent))) return QModelIndex();
    if (Q_UNLIKELY(col < 0 || col >= columnCount(parent))) return QModelIndex();

    return createIndex(row, col);
}

QModelIndex CsvModel::parent(const QModelIndex &) const
{
    return QModelIndex();
}

bool CsvModel::canFetchMore(const QModelIndex &) const
{
    Q_D(const CsvModel);
    return !d->m_parser.atEnd() && !d->m_parser.hasError();
}

void CsvModel::fetchMore(const QModelIndex &)
{
    Q_D(CsvModel);
    d->fetchMore();
}
