/*
 * Copyright (C) 2023 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Studentario.
 *
 * Studentario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Studentario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Studentario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STUDENTARIO_CSV_MODEL_H
#define STUDENTARIO_CSV_MODEL_H

#include <QAbstractItemModel>
#include <QScopedPointer>
#include <QString>
#include <QVariantMap>

namespace Studentario {

class CsvModelPrivate;
class CsvModel: public QAbstractItemModel
{
    Q_OBJECT
    Q_PROPERTY(QString filePath READ filePath WRITE setFilePath
               NOTIFY filePathChanged)
    Q_PROPERTY(QStringList columnTitles READ columnTitles NOTIFY filePathChanged)
    Q_PROPERTY(QModelIndex rootIndex READ rootIndex CONSTANT)
    Q_PROPERTY(int loadedRowCount READ rowCount NOTIFY countChanged)

public:
    enum Roles {
        ValueRole = Qt::UserRole,
        TypeRole,
    };
    Q_ENUM(Roles)

    CsvModel(QObject *parent = nullptr);
    ~CsvModel();

    void setFilePath(const QString &filePath);
    QString filePath() const;

    QStringList columnTitles() const;

    QModelIndex rootIndex() const { return QModelIndex(); }

    Q_INVOKABLE void setColumnType(int col, const QString &dataType);

    Q_INVOKABLE QVariant get(int row, int col, const QString &roleName) const;
    QVariant data(const QModelIndex &index,
                  int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QHash<int, QByteArray> roleNames() const override;
    QModelIndex index(int row, int col,
                      const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;
    bool canFetchMore(const QModelIndex &parent) const override;
    void fetchMore(const QModelIndex &parent) override;

Q_SIGNALS:
    void filePathChanged();
    void countChanged();

private:
    QScopedPointer<CsvModelPrivate> d_ptr;
    Q_DECLARE_PRIVATE(CsvModel)
};

} // namespace

#endif // STUDENTARIO_CSV_MODEL_H
