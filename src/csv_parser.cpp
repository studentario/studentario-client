/*
 * Copyright (C) 2023 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Studentario.
 *
 * Studentario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Studentario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Studentario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "csv_parser.h"

#include <QDebug>
#include <QTextStream>

using namespace Studentario;

namespace Studentario {

class Parser {
public:
    void setSeparator(QChar sep) { m_separator = sep; }
    void addLine(const QString &line);

    bool isDone() const { return m_status == Done; }
    bool isError() const { return m_status == FormatError; }
    bool needsData() const { return m_status == NeedsData; }
    QStringList data() const { return m_data; }

private:
    enum Status {
        BeginField,
        InsideQuotes,
        UnquotedField,
        NeedsData,
        FormatError,
        Done,
    };

    QChar nextChar();
    void addCurrentField();

    QChar m_separator = ',';
    QString m_buffer;
    QString m_fieldBuffer;
    int m_currentIndex = 0;
    QStringList m_data;
    Status m_status = BeginField;
};

class CsvParserPrivate
{
public:
    CsvParserPrivate();

    void setInputDevice(QIODevice *device);

    void readTitles();
    QStringList readNextRow();

private:
    friend class CsvParser;
    QTextStream m_stream;
    QStringList m_titles;
    QString m_line;
    bool m_hasError;
};

} // namespace

void Parser::addLine(const QString &line)
{
    if (!m_buffer.isEmpty()) m_buffer += '\n';
    m_buffer += line;
    if (m_status == NeedsData) {
        m_status = InsideQuotes;
    }

    while (m_status != Done) {
        if (m_status == BeginField) {
            QChar ch = nextChar();
            if (ch.isNull()) {
                addCurrentField();
                m_status = Done;
            } else if (ch == m_separator) {
                // empty field
                addCurrentField();
            } else if (ch == '"') {
                m_status = InsideQuotes;
            } else {
                m_status = UnquotedField;
                m_fieldBuffer.clear();
                m_fieldBuffer += ch;
            }
        }

        if (m_status == InsideQuotes) {
            int nextQuote = m_buffer.indexOf('"', m_currentIndex);
            if (nextQuote < 0) { m_status = NeedsData; return; }

            int count = nextQuote - m_currentIndex;
            m_fieldBuffer += m_buffer.midRef(m_currentIndex, count);
            m_currentIndex = nextQuote + 1;

            // Determine if the field is finished or if we are just escaping
            // quotes
            QChar ch = nextChar();
            if (ch == '"') {
                m_fieldBuffer += ch;
            } else if (ch == m_separator || ch.isNull()) {
                addCurrentField();
                m_status = ch.isNull() ? Done : BeginField;
            } else {
                m_status = FormatError;
                return;
            }
        }

        if (m_status == UnquotedField) {
            int nextSeparator = m_buffer.indexOf(m_separator, m_currentIndex);
            int count = (nextSeparator >= 0) ?
                nextSeparator - m_currentIndex : -1;
            m_fieldBuffer += m_buffer.midRef(m_currentIndex, count);
            addCurrentField();
            if (nextSeparator >= 0) {
                m_currentIndex = nextSeparator + 1;
                m_status = BeginField;
            } else {
                m_status = Done;
            }
        }
    }
}

QChar Parser::nextChar()
{
    return m_currentIndex < m_buffer.length() ?
        m_buffer[m_currentIndex++] : QChar();
}

void Parser::addCurrentField()
{
    m_data.append(m_fieldBuffer);
    m_fieldBuffer.clear();
}

CsvParserPrivate::CsvParserPrivate():
    m_hasError(false)
{
}

void CsvParserPrivate::setInputDevice(QIODevice *device)
{
    m_hasError = false;
    m_stream.setAutoDetectUnicode(true);
    m_stream.setDevice(device);
    readTitles();
}

void CsvParserPrivate::readTitles()
{
    m_titles = readNextRow();
}

QStringList CsvParserPrivate::readNextRow()
{
    Parser parser;
    parser.setSeparator(',');

    if (m_stream.atEnd()) return QStringList();

    do {
        bool ok = m_stream.readLineInto(&m_line);
        if (Q_UNLIKELY(!ok)) {
            qWarning() << "Error reading line from CSV:" <<
                m_stream.device()->errorString();
            m_hasError = true;
            return QStringList();
        }

        parser.addLine(m_line);
    } while (parser.needsData());

    if (Q_UNLIKELY(parser.isError())) {
        qWarning() << "Invalid CSV format";
        m_hasError = true;
    }

    return parser.isDone() ? parser.data() : QStringList();
}

CsvParser::CsvParser():
    d_ptr(new CsvParserPrivate())
{
}

CsvParser::~CsvParser() = default;

void CsvParser::setInputDevice(QIODevice *device)
{
    Q_D(CsvParser);
    d->setInputDevice(device);
}

QIODevice *CsvParser::inputDevice() const
{
    Q_D(const CsvParser);
    return d->m_stream.device();
}

QStringList CsvParser::columnTitles() const
{
    Q_D(const CsvParser);
    return d->m_titles;
}

QStringList CsvParser::readNextRow()
{
    Q_D(CsvParser);
    return d->readNextRow();
}

bool CsvParser::hasError() const
{
    Q_D(const CsvParser);
    return d->m_hasError;
}

bool CsvParser::atEnd() const
{
    Q_D(const CsvParser);
    return d->m_stream.atEnd();
}
