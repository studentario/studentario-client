/*
 * Copyright (C) 2023 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Studentario.
 *
 * Studentario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Studentario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Studentario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STUDENTARIO_CSV_PARSER_H
#define STUDENTARIO_CSV_PARSER_H

#include <QScopedPointer>
#include <QString>
#include <QStringList>

class QIODevice;

namespace Studentario {

class CsvParserPrivate;
class CsvParser
{
public:
    CsvParser();
    virtual ~CsvParser();

    void setInputDevice(QIODevice *device);
    QIODevice *inputDevice() const;

    QStringList columnTitles() const;
    QStringList readNextRow();

    bool hasError() const;
    bool atEnd() const;

private:
    QScopedPointer<CsvParserPrivate> d_ptr;
    Q_DECLARE_PRIVATE(CsvParser)
};

} // namespace

#endif // STUDENTARIO_CSV_PARSER_H
