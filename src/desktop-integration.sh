#! /bin/sh

set -e

DESKTOP_DIR="$HOME/.local/share/applications"
DESKTOP_FILE="$DESKTOP_DIR/studentario.desktop"

if [ -f "$DESKTOP_FILE" ] && grep -q "^Exec=$APPIMAGE" "$DESKTOP_FILE"; then
    # Desktop file is up to date
    exit 0
fi

ICON_DIR="$HOME/.local/share/icons"
ICON_FILE="studentario.svg"

mkdir -p "$ICON_DIR"
cp "$APPDIR/usr/share/icons/hicolor/scalable/apps/$ICON_FILE" \
    "$ICON_DIR/"
update-icon-caches ~/.local/share/icons/ || true

mkdir -p "$DESKTOP_DIR"
sed \
    -e "s,^Exec.*,Exec=$APPIMAGE %F," \
    -e "s/^Icon.*/Icon=$ICON_FILE/" \
    -e "/^Path.*/d" \
    "$APPDIR/usr/share/applications/studentario.desktop" \
    > "$DESKTOP_FILE"
chmod a+x "$DESKTOP_FILE"
update-desktop-database "$DESKTOP_DIR" || true
