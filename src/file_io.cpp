/*
 * Copyright (C) 2020-2023 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of LinguaLonga.
 *
 * LinguaLonga is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LinguaLonga is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LinguaLonga.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "file_io.h"

#include <QDebug>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QSaveFile>

using namespace LinguaLonga;

namespace LinguaLonga {

class FileIOPrivate
{
    Q_DECLARE_PUBLIC(FileIO)

public:
    FileIOPrivate(FileIO *q);

private:
    QFileInfo m_fileInfo;
    FileIO *q_ptr;
};

} // namespace

FileIOPrivate::FileIOPrivate(FileIO *q):
    q_ptr(q)
{
}

FileIO::FileIO(QObject *parent):
    QObject(parent),
    d_ptr(new FileIOPrivate(this))
{
}

FileIO::~FileIO()
{
}

void FileIO::setFilePath(const QString &filePath)
{
    Q_D(FileIO);
    d->m_fileInfo.setFile(filePath);
    Q_EMIT filePathChanged();
    Q_EMIT contentsChanged();
}

QString FileIO::filePath() const
{
    Q_D(const FileIO);
    return d->m_fileInfo.filePath();
}

void FileIO::setContents(const QByteArray &contents)
{
    Q_D(FileIO);

    if (Q_UNLIKELY(!QDir::root().mkpath(d->m_fileInfo.path()))) {
        qWarning() << "Could not create parent dir!";
        return;
    }

    QSaveFile file(filePath());
    if (Q_UNLIKELY(!file.open(QIODevice::WriteOnly))) {
        qWarning() << "Cannot open" << filePath() << "for writing:" <<
            file.errorString();
        return;
    }
    file.write(contents);
    if (!file.commit()) {
        qWarning() << "Error writing to" << filePath() << ":" <<
            file.errorString();
        return;
    }

    Q_EMIT contentsChanged();
}

QByteArray FileIO::contents() const
{
    QFile file(filePath());
    if (!file.open(QIODevice::ReadOnly)) {
        return QByteArray();
    }
    return file.readAll();
}

void FileIO::reload()
{
    Q_EMIT contentsChanged();
}
