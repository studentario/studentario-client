/*
 * Copyright (C) 2020-2023 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of LinguaLonga.
 *
 * LinguaLonga is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LinguaLonga is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LinguaLonga.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LINGUALONGA_FILE_IO_H
#define LINGUALONGA_FILE_IO_H

#include <QByteArray>
#include <QObject>
#include <QString>

namespace LinguaLonga {

class FileIOPrivate;
class FileIO: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString filePath READ filePath WRITE setFilePath
               NOTIFY filePathChanged)
    Q_PROPERTY(QByteArray contents READ contents WRITE setContents
               NOTIFY contentsChanged)

public:
    FileIO(QObject *parent = 0);
    ~FileIO();

    void setFilePath(const QString &filePath);
    QString filePath() const;

    void setContents(const QByteArray &contents);
    QByteArray contents() const;

    Q_INVOKABLE void reload();

Q_SIGNALS:
    void filePathChanged();
    void contentsChanged();

private:
    QScopedPointer<FileIOPrivate> d_ptr;
    Q_DECLARE_PRIVATE(FileIO)
};

} // namespace

#endif // LINGUALONGA_FILE_IO_H
