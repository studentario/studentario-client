/*
 * Copyright (C) 2020-2023 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of LinguaLonga.
 *
 * LinguaLonga is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LinguaLonga is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LinguaLonga.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "json_storage.h"

#include <QDebug>
#include <QDir>
#include <QFileInfo>
#include <QJsonDocument>
#include <QQmlEngine>
#include <QStandardPaths>
#include <QString>

using namespace LinguaLonga;

namespace LinguaLonga {

class JsonStoragePrivate
{
    Q_DECLARE_PUBLIC(JsonStorage)

public:
    JsonStoragePrivate(JsonStorage *q);

    void update();

private:
    QStandardPaths::StandardLocation m_base;
    QString m_filePath;
    bool m_completed;
    QFileInfo m_storageFile;
    JsonStorage *q_ptr;
};

} // namespace

JsonStoragePrivate::JsonStoragePrivate(JsonStorage *q):
    m_base(QStandardPaths::QStandardPaths::HomeLocation),
	m_completed(false),
    q_ptr(q)
{
}

void JsonStoragePrivate::update()
{
    if (!m_completed) return;
    m_storageFile =
        QStandardPaths::writableLocation(m_base) + "/" + m_filePath;
}

JsonStorage::JsonStorage(QObject *parent):
    QObject(parent),
    d_ptr(new JsonStoragePrivate(this))
{
}

JsonStorage::~JsonStorage()
{
}

void JsonStorage::setBase(QStandardPaths::StandardLocation base)
{
    Q_D(JsonStorage);
    d->m_base = base;
    Q_EMIT baseChanged();
    d->update();
}

QStandardPaths::StandardLocation JsonStorage::base() const
{
    Q_D(const JsonStorage);
    return d->m_base;
}

QString JsonStorage::basePath() const
{
    Q_D(const JsonStorage);
    return QStandardPaths::writableLocation(d->m_base);
}

void JsonStorage::setFilePath(const QString &filePath)
{
    Q_D(JsonStorage);
    d->m_filePath = filePath;
    Q_EMIT filePathChanged();
    d->update();
}

QString JsonStorage::filePath() const
{
    Q_D(const JsonStorage);
    return d->m_filePath;
}

void JsonStorage::storeObject(const QJsonObject &object)
{
    Q_D(JsonStorage);

    if (Q_UNLIKELY(!QDir::root().mkpath(d->m_storageFile.path()))) {
        qWarning() << "Could not create cache dir!";
        return;
    }

    QFile file(d->m_storageFile.filePath());
    if (Q_UNLIKELY(!file.open(QIODevice::ReadWrite))) {
        qWarning() << "Could not create storage file" <<
            d->m_storageFile.filePath();
        return;
    }

    QJsonDocument doc(object);

    file.resize(0);
    file.write(doc.toJson());
}

QJsonObject JsonStorage::loadObject() const
{
    Q_D(const JsonStorage);
    QFile file(d->m_storageFile.filePath());
    if (!file.open(QIODevice::ReadOnly)) {
        return QJsonObject();
    }

    QJsonDocument doc = QJsonDocument::fromJson(file.readAll());
    return doc.object();
}

void JsonStorage::classBegin()
{
}

void JsonStorage::componentComplete()
{
    Q_D(JsonStorage);
    d->m_completed = true;
    d->update();
}
