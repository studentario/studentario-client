/*
 * Copyright (C) 2023 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Studentario.
 *
 * Studentario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Studentario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Studentario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "planner_model.h"

#include <QDebug>
#include <QMetaObject>
#include <QQmlPropertyMap>
#include <algorithm>
#include <list>
#include <tuple>

using namespace Studentario;

namespace Studentario {

class Activity: public QQmlPropertyMap
{
    Q_OBJECT
    Q_PROPERTY(int column READ column NOTIFY columnChanged)

public:
    Activity(const QVariantMap &data, PlannerModel *planner);

    int column() const;
    void updateRange(const QString &key, const QVariant &value);

protected:
    QVariant updateValue(const QString &key, const QVariant &input);

Q_SIGNALS:
    void columnChanged();

private:
    PlannerModel *m_planner;
};

class PlannerModelPrivate
{
public:
    PlannerModelPrivate();

    void computeOverlaps();
    void queueComputeOverlaps(PlannerModel *q);

private:
    friend class PlannerModel;
    QDateTime m_startTime;
    QDateTime m_endTime;
    QStringList m_columnValues;
    QString m_columnKey;
    QList<QObject*> m_activities;
    bool m_computeOverlapQueued;
};

} // namespace

Activity::Activity(const QVariantMap &data, PlannerModel *planner):
    QQmlPropertyMap(this, planner),
    m_planner(planner)
{
    QObject::connect(m_planner, &PlannerModel::layoutChanged,
                     this, &Activity::columnChanged);

    for (auto i = data.begin(); i != data.end(); i++) {
        insert(i.key(), i.value());
        if (i.key() == "startTime" || i.key() == "endTime") {
            updateRange(i.key(), i.value());
        }
    }
}

int Activity::column() const
{
    const QString key = m_planner->columnKey();
    const QStringList columnValues = m_planner->columnValues();
    return columnValues.indexOf(value(key).toString());
}

void Activity::updateRange(const QString &key, const QVariant &value)
{
    QDateTime t = value.toDateTime();
    /* Calcula le position, proportionalmente */
    const QDateTime startTime = m_planner->startTime();
    double x = double(startTime.secsTo(t)) /
        startTime.secsTo(m_planner->endTime());
    QString mappedKey = key == "startTime" ?
        QStringLiteral("startRatio") : QStringLiteral("endRatio");
    insert(mappedKey, x);
}

QVariant Activity::updateValue(const QString &key, const QVariant &input)
{
    if (key == "startTime" || key == "endTime") {
        updateRange(key, input);
    }

    return QQmlPropertyMap::updateValue(key, input);
}

PlannerModelPrivate::PlannerModelPrivate():
    m_computeOverlapQueued(false)
{
}

void PlannerModelPrivate::computeOverlaps() {
    if (Q_UNLIKELY(m_columnValues.isEmpty())) return;

    using TimeEntry = std::tuple<double, int, Activity*>;
    using ColumnTimes = std::list<TimeEntry>;
    QVector<ColumnTimes> times(m_columnValues.count());

    /* Placia le coordinatas de initio de fin del activitates in un lista (per
     * columna), ordinate.
     */
    for (QObject *o: m_activities) {
        Activity *activity = qobject_cast<Activity*>(o);
        activity->insert("overlapCount", 0);
        int column = activity->column();
        if (column < 0) continue;

        ColumnTimes &columnTimes = times[column];
        TimeEntry a {
            activity->value("startRatio").toDouble(),
            1,  // apertura
            activity
        };
        auto i = std::upper_bound(columnTimes.begin(), columnTimes.end(), a);
        i = columnTimes.insert(i, std::move(a));

        TimeEntry b {
            activity->value("endRatio").toDouble(),
            -1,  // clausura
            activity
        };
        i = std::upper_bound(i, columnTimes.end(), b);
        columnTimes.insert(i, std::move(b));
    }

    /* Postea, itera cata columna e mantene le conto de quante activitates ha
     * essite aperite e claudite: quando on activitate initia e le conto del
     * activitates currente es plus grande que un, il significa que nos ha un
     * overlap.
     */
    auto updateCount = [](const TimeEntry &t, int count) {
        Activity *activity = std::get<2>(t);
        int oldCount = activity->value("overlapCount").toInt();
        if (count > oldCount) {
            activity->insert("overlapCount", count);
        }
    };

    for (const ColumnTimes &columnTimes: qAsConst(times)) {
        int runningCount = 0;
        /* "oldest" es le plus vetule activitate que partecipa in le conflicto
         * e que lo creava. Illo numquam es addite al lista openActivities, pro
         * sparniar le actualisation de openActivities quando il non ha
         * conflictos (que es le caso usual).
         * Pro iste ration illo es tractate in maniera special: quando nos vide
         * que un activitate es claudite quando nos es in un conflicto ma nos
         * non lo trova in le lista openActivities, illo significa que iste
         * clausura reguarda le "oldest", e debe poner su conto de
         * superposition al maximo que occurreva durante le periodo que illo
         * esseva active (oldestActive == true).
         */
        bool oldestActive = false;
        int oldestOverlapCount = 0;
        std::list<const TimeEntry*> openActivities;
        for (const TimeEntry &t: columnTimes) {
            int increment = std::get<1>(t);
            runningCount += increment;
            if (increment > 0) { // apertura
                if (runningCount > 1) {
                    /* Nos ha un nove conflicto. */
                    openActivities.push_back(&t);
                } else {
                    oldestActive = true;
                }
            } else { // clausura
                if (runningCount >= 1) {
                    // Nos esseva in un conflicto
                    int overlapCount = runningCount + 1;
                    bool found = false;
                    auto i = openActivities.begin();
                    while (i != openActivities.end()) {
                        const TimeEntry *overlap = *i;
                        updateCount(*overlap, overlapCount);
                        if (std::get<2>(t) == std::get<2>(*overlap)) {
                            found = true;
                            i = openActivities.erase(i);
                        } else {
                            i++;
                        }
                    }

                    if (oldestActive) {
                        oldestOverlapCount = qMax(overlapCount, oldestOverlapCount);
                    }

                    if (!found) {
                        // Illo es le plus vetule
                        updateCount(t, oldestOverlapCount);
                        oldestOverlapCount = 0;
                        oldestActive = false;
                    }
                }
            }
        }
    }
}

void PlannerModelPrivate::queueComputeOverlaps(PlannerModel *q)
{
    if (m_computeOverlapQueued) return;

    m_computeOverlapQueued = true;
    QMetaObject::invokeMethod(q, [this]() {
        computeOverlaps();
        m_computeOverlapQueued = false;
    }, Qt::QueuedConnection);
}

PlannerModel::PlannerModel(QObject *parent):
    QObject(parent),
    d_ptr(new PlannerModelPrivate())
{
    Q_D(PlannerModel);
    QObject::connect(this, &PlannerModel::activitiesChanged,
                     [d, this]() { d->queueComputeOverlaps(this); });
    QObject::connect(this, &PlannerModel::layoutChanged,
                     [d, this]() { d->queueComputeOverlaps(this); });
}

PlannerModel::~PlannerModel() = default;

void PlannerModel::setStartTime(const QDateTime &startTime)
{
    Q_D(PlannerModel);
    d->m_startTime = startTime;
    Q_EMIT startTimeChanged();
}

QDateTime PlannerModel::startTime() const
{
    Q_D(const PlannerModel);
    return d->m_startTime;
}

void PlannerModel::setEndTime(const QDateTime &endTime)
{
    Q_D(PlannerModel);
    d->m_endTime = endTime;
    Q_EMIT endTimeChanged();
}

QDateTime PlannerModel::endTime() const
{
    Q_D(const PlannerModel);
    return d->m_endTime;
}

void PlannerModel::setColumnValues(const QStringList &columnValues)
{
    Q_D(PlannerModel);
    d->m_columnValues = columnValues;
    Q_EMIT layoutChanged();
}

QStringList PlannerModel::columnValues() const
{
    Q_D(const PlannerModel);
    return d->m_columnValues;
}

void PlannerModel::setColumnKey(const QString &columnKey)
{
    Q_D(PlannerModel);
    d->m_columnKey = columnKey;
    Q_EMIT layoutChanged();
}

QString PlannerModel::columnKey() const
{
    Q_D(const PlannerModel);
    return d->m_columnKey;
}

QList<QObject*> PlannerModel::activities() const
{
    Q_D(const PlannerModel);
    return d->m_activities;
}

void PlannerModel::addActivity(const QVariantMap &activity)
{
    Q_D(PlannerModel);

    d->m_activities.append(new Activity(activity, this));
    Q_EMIT activitiesChanged();
}

void PlannerModel::clear()
{
    Q_D(PlannerModel);
    qDeleteAll(d->m_activities);
    d->m_activities.clear();
    Q_EMIT activitiesChanged();
}

#include "planner_model.moc"
