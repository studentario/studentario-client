/*
 * Copyright (C) 2023 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Studentario.
 *
 * Studentario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Studentario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Studentario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STUDENTARIO_PLANNER_MODEL_H
#define STUDENTARIO_PLANNER_MODEL_H

#include <QDateTime>
#include <QList>
#include <QObject>
#include <QScopedPointer>
#include <QStringList>
#include <QVariantMap>

namespace Studentario {

class PlannerModelPrivate;
class PlannerModel: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QDateTime startTime READ startTime WRITE setStartTime
               NOTIFY startTimeChanged)
    Q_PROPERTY(QDateTime endTime READ endTime WRITE setEndTime
               NOTIFY endTimeChanged)
    Q_PROPERTY(QStringList columnValues READ columnValues WRITE setColumnValues
               NOTIFY layoutChanged)
    Q_PROPERTY(QString columnKey READ columnKey WRITE setColumnKey
               NOTIFY layoutChanged)
    Q_PROPERTY(QList<QObject*> activities READ activities NOTIFY activitiesChanged)

public:
    PlannerModel(QObject *parent = nullptr);
    virtual ~PlannerModel();

    void setStartTime(const QDateTime &startTime);
    QDateTime startTime() const;

    void setEndTime(const QDateTime &endTime);
    QDateTime endTime() const;

    void setColumnValues(const QStringList &columnValues);
    QStringList columnValues() const;

    void setColumnKey(const QString &columnKey);
    QString columnKey() const;

    QList<QObject*> activities() const;

    /* Illo debe continer duo proprietates "startTime" e "endTime". */
    Q_INVOKABLE void addActivity(const QVariantMap &activity);

    Q_INVOKABLE void clear();

Q_SIGNALS:
    void startTimeChanged();
    void endTimeChanged();
    void layoutChanged();
    void activitiesChanged();

private:
    QScopedPointer<PlannerModelPrivate> d_ptr;
    Q_DECLARE_PRIVATE(PlannerModel)
};

} // namespace

#endif // STUDENTARIO_PLANNER_MODEL_H
