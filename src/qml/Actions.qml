import QtQml 2.12
import QtQuick 2.12
import QtQuick.Controls 2.12
import it.mardy.studentario 1.0
import "TagType.js" as TagType

QtObject {
    property var site: null

    property var viewStudents: Action {
        text: qsTr("Lista del studentes")
        onTriggered: stackView.push(Qt.resolvedUrl("UsersPage.qml"), {
            'site': site,
            'editable': false,
            'role': Roles.Student,
        })
    }

    property var manageStudents: Action {
        text: qsTr("Administra le studentes")
        onTriggered: stackView.push(Qt.resolvedUrl("UsersPage.qml"), {
            'site': site,
            'editable': true,
            'role': Roles.Student,
        })
    }

    property var manageTeachers: Action {
        text: qsTr("Administra le inseniantes")
        onTriggered: stackView.push(Qt.resolvedUrl("UsersPage.qml"), {
            'site': site,
            'editable': true,
            'role': Roles.Teacher,
        })
    }

    property var manageAdmins: Action {
        text: qsTr("Administra le administratores")
        onTriggered: stackView.push(Qt.resolvedUrl("UsersPage.qml"), {
            'site': site,
            'editable': true,
            'role': Roles.Admin,
        })
    }

    property var manageDirectors: Action {
        text: qsTr("Administra le directores")
        onTriggered: stackView.push(Qt.resolvedUrl("UsersPage.qml"), {
            'site': site,
            'editable': true,
            'role': Roles.Director,
        })
    }

    property var manageGroups: Action {
        text: qsTr("Administra le gruppos")
        onTriggered: stackView.push(Qt.resolvedUrl("GroupsPage.qml"), {
            'site': site,
            'editable': true,
        })
    }

    property var viewGroups: Action {
        text: qsTr("Lista del gruppos")
        onTriggered: stackView.push(Qt.resolvedUrl("GroupsPage.qml"), {
            'site': site,
            'editable': false,
        })
    }

    property var manageLessons: Action {
        text: qsTr("Administra le lectiones")
        onTriggered: stackView.push(Qt.resolvedUrl("LessonsPage.qml"), {
            'site': site,
            'editable': true,
        })
    }

    property var viewTeacherLessons: Action {
        text: qsTr("Mi lectiones")
        onTriggered: stackView.push(Qt.resolvedUrl("LessonsPage.qml"), {
            'site': site,
            'editable': false,
            'role': Roles.Teacher,
            'userId': site.authenticatedUser.userId,
        })
    }

    property var viewStudentLessons: Action {
        text: qsTr("Mi lectiones")
        onTriggered: stackView.push(Qt.resolvedUrl("LessonsPage.qml"), {
            'site': site,
            'editable': false,
            'role': Roles.Student,
            'userId': site.authenticatedUser.userId,
        })
    }

    property var manageLocations: Action {
        text: qsTr("Administra le locationes")
        onTriggered: stackView.push(Qt.resolvedUrl("LocationsPage.qml"), {
            'site': site,
            'editable': true,
        })
    }

    property var manageTags: Action {
        text: qsTr("Administra le etiquettas")
        onTriggered: stackView.push(Qt.resolvedUrl("TagsPage.qml"), {
            'site': site,
            'parentTagId': -1,
            'tagType': TagType.byTypeId("tags"),
            'editable': true,
        })
    }

    property var manageLevels: Action {
        text: qsTr("Administra le nivellos")
        onTriggered: stackView.push(Qt.resolvedUrl("TagsPage.qml"), {
            'site': site,
            'parentTagId': site.tags.levelTagId,
            'tagType': TagType.byTypeId("level"),
            'editable': true,
        })
    }
}
