import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

ColumnLayout {
    id: root

    property int userId: -1
    property string currentStatus: ""
    property date statusChangeTime: null

    signal historyPageRequested()

    ActivationItem {
        id: activationItem
        Layout.fillWidth: true
        visible: root.userId < 0
        firstRecord: true
        currentStatus: "active"
    }

    Label {
        Layout.fillWidth: true
        visible: root.userId >= 0
        text: {
            var dateStr = Qt.formatDate(root.statusChangeTime)
            switch (root.currentStatus) {
            case "active": return qsTr("Active a partir de %1").arg(dateStr)
            case "inactive": return qsTr("Disactivate desde %1").arg(dateStr)
            default: return qsTr("Numquam activate")
            }
        }
    }

    Button {
        Layout.alignment: Qt.AlignRight
        visible: root.userId >= 0
        text: qsTr("Vide le historia del activationes")
        onClicked: root.historyPageRequested()
    }

    function sync() {
        if (userId < 0) {
            activationItem.sync()
            root.currentStatus = activationItem.currentStatus
            root.statusChangeTime = activationItem.statusChangeTime
        }
    }
}
