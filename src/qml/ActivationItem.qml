import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

RowLayout {
    id: root

    property string currentStatus: ""
    property date statusChangeTime: null
    property bool firstRecord: false

    property bool _leaveDeactivated: firstRecord && !activeField.checked
    property string _dateHint: {
        if (activeField.checked) return qsTr("Data de activation")
        return root.firstRecord ?
            qsTr("Lassa le usator non activate") : qsTr("Data de disactivation")
    }

    CheckBox {
        id: activeField
        Layout.fillWidth: true
        text: qsTr("Active")
        checkState: root.currentStatus == "active" ? Qt.Checked : Qt.Unchecked

        function statusStr() {
            switch (checkState) {
            case Qt.Checked: return "active"
            case Qt.Unchecked:
                return root.firstRecord ? "" : "inactive"
            }
        }
    }

    Label {
        text: parent._dateHint
        enabled: !root._leaveDeactivated
    }

    DatePicker {
        id: datePicker
        title: parent._dateHint
        selectedDate: isNaN(root.statusChangeTime) ? new Date() : root.statusChangeTime
        enabled: !root._leaveDeactivated
    }

    function sync() {
        currentStatus = activeField.statusStr()
        statusChangeTime = datePicker.selectedDate
    }
}
