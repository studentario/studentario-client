import QtQuick 2.9
import QtQuick.Controls 2.12

ToolButton {
    id: root
    action: Action {
        text: qsTr("‹")
        enabled: root.visible
        shortcut: StandardKey.Back
    }
}
