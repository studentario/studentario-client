import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import Qt.labs.calendar 1.0

Dialog {
    id: root

    property alias selectedDate: calendarView.selectedDate
    property alias minDate: calendarView.minDate
    property alias maxDate: calendarView.maxDate

    modal: true
    focus: true
    standardButtons: Dialog.Ok | Dialog.Cancel

    CalendarView {
        id: calendarView
        anchors.fill: parent
    }
}
