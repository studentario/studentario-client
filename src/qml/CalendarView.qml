import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import Qt.labs.calendar 1.0

GridLayout {
    id: root

    property alias selectedDate: monthView.selectedDate
    property alias selectedDates: monthView.selectedDates
    property alias selectMultiple: monthView.selectMultiple

    property date minDate
    property date maxDate: {
        var d = new Date(); d.setFullYear(d.getFullYear() + 10); return d
    }


    property bool _ready: false

    columns: 2

    Component.onCompleted: { _ready = true; updateCalendarView() }
    onSelectedDateChanged: if (_ready) updateCalendarView()

    SpinBox {
        id: yearControl
        focus: true
        value: monthView.currentItem.year
        from: root.minDate.getFullYear()
        to: root.maxDate.getFullYear()
        textFromValue: function (value, locale) { return "" + value }
        valueFromText: function(text, locale) {
            return Number.fromLocaleString(locale, text)
        }
        contentItem: TextInput {
            property var control: yearControl
            z: 2
            text: control.textFromValue(control.value, control.locale)
            font: control.font
            color: control.palette.text
            selectionColor: control.palette.highlight
            selectedTextColor: control.palette.highlightedText
            horizontalAlignment: Qt.AlignHCenter
            verticalAlignment: Qt.AlignVCenter
            readOnly: !control.editable
            validator: control.validator
            inputMethodHints: Qt.ImhDigitsOnly
            inputMask: "9999"
            onTextChanged: if (root._ready && acceptableInput) {
                control.value = control.valueFromText(text, control.locale)
                control.valueModified()
            }
        }
        editable: true
        onValueModified: monthView.activateMonth(value, -1)
        onActiveFocusChanged: if (activeFocus) contentItem.selectAll()
    }

    ComboBox {
        Layout.fillWidth: true
        model: {
            var months = []
            var d = new Date()
            for (var i = 0; i < 12; i++) {
                d.setMonth(i)
                months.push(Qt.formatDate(d, "MMMM"))
            }
            return months
        }
        currentIndex: monthView.currentItem.month
        onActivated: monthView.activateMonth(-1, currentIndex)
    }

    MonthView {
        id: monthView
        Layout.fillWidth: true
        Layout.fillHeight: true
        Layout.columnSpan: 2
        clip: true
        minDate: root.minDate
        maxDate: root.maxDate
    }

    function updateCalendarView() {
        if (monthView._blockPropagation) return
        var currentDate = isNaN(selectedDate) ? minDate : selectedDate
        var i = monthView.model.indexOf(currentDate)
        if (i >= 0) monthView.currentIndex = i
    }

    function clear() {
        selectedDates = []
    }
}
