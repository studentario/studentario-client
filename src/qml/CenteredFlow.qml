import QtQuick 2.12

Item {
    id: root

    property int rowSpacing: 2
    property int columnSpacing: 2
    property var selectedTags: []
    property var deselectedTags: []
    property var textColor: "black"
    property bool multiSelection: true

    signal pressAndHold(var mouse, int index, var pressedItem)

    implicitHeight: childrenRect.height

    onWidthChanged: reflowTimer.start()
    onChildrenChanged: reflowTimer.start()

    Timer {
        id: reflowTimer
        interval: 10
        onTriggered: root.reflow()
    }

    function reflow() {
        var rows = []
        var rowWidth = 0
        var rowHeight = 0
        var rowIndex = 0
        var rowItems = []
        var maxRowWidth = 0
        /* First, compute the lines' widths */
        for (var i = 0; i < children.length; i++) {
            var item = children[i]
            if (!item.visible || item.width == 0) continue;
            if (rowWidth + item.width + root.columnSpacing > root.width) {
                maxRowWidth = Math.max(maxRowWidth, rowWidth)
                rows[rowIndex] = {
                    "width": rowWidth,
                    "height": rowHeight,
                    "items": rowItems
                }
                rowIndex++
                rowWidth = 0
                rowHeight = 0
                rowItems = []
            }
            rowWidth += item.width + root.columnSpacing
            rowItems.push(item)
            if (item.implicitHeight > rowHeight) rowHeight = item.implicitHeight
        }

        if (rowItems.length > 0) {
            maxRowWidth = Math.max(maxRowWidth, rowWidth)
            rows[rowIndex] = {
                "width": rowWidth,
                "height": rowHeight,
                "items": rowItems
            }
        }

        var y = 0
        for (var i = 0; i < rows.length; i++) {
            var row = rows[i]
            var x = Math.round((root.width - row.width) / 2)
            for (var j = 0; j < row.items.length; j++) {
                row.items[j].x = x
                row.items[j].y = y
                row.items[j].height = row.height
                x += row.items[j].width + root.columnSpacing
            }
            y += row.height + root.rowSpacing
        }

        root.implicitWidth = maxRowWidth
    }
}
