import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Dialogs 1.0

Button {
    id: root

    property color selectedColor

    flat: true
    contentItem: Rectangle {
        color: root.selectedColor
        border { color: "black"; width: 1 }
    }
    onClicked: popup.open()

    ColorDialog {
        id: popup
        color: root.selectedColor
        showAlphaChannel: false
        onAccepted: {
            root.selectedColor = popup.color
        }
    }
}
