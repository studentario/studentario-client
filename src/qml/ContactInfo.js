var infoTypes = [
    /* NOTA: parte de iste information es replicate in UserDataFormatter */
    {
        typeId: "parentName",
        name: qsTr("Nomine del accompaniator"),
        placeholderText: qsTr("Nomine complete"),
        inputMethodHints: Qt.ImhNone,
        needsEncoding: true,
        showAsUrl: false
    },
    {
        typeId: "address",
        name: qsTr("Adresse"),
        placeholderText: qsTr("Adresse de residentia"),
        inputMethodHints: Qt.ImhNone,
        needsEncoding: true,
        showAsUrl: false
    },
    {
        typeId: "tel",
        name: qsTr("Telephono"),
        placeholderText: qsTr("In formato international: \"+7…\""),
        inputMethodHints: Qt.ImhDialableCharactersOnly,
        needsEncoding: false,
        showAsUrl: true
    },
    {
        typeId: "mailto",
        name: qsTr("Posta electronic"),
        placeholderText: qsTr("usator@exemplo.com"),
        inputMethodHints: Qt.ImhEmailCharactersOnly,
        needsEncoding: false,
        showAsUrl: true
    },
]

function byTypeId(typeId) {
    for (var i = 0; i < infoTypes.length; i++) {
        if (infoTypes[i].typeId == typeId) return infoTypes[i]
    }
    return {}
}

function parseUri(uri) {
    var parts = uri.split(':', 2)
    var type = byTypeId(parts[0])
    var value = type.needsEncoding ? decodeURIComponent(parts[1]) : parts[1]
    return { 'type': type, 'value': value }
}

function createUri(typeId, text) {
    var type = byTypeId(typeId)
    var value = type.needsEncoding ? encodeURIComponent(text) : text
    return typeId + ':' + value
}
