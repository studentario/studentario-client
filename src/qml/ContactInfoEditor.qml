import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

ColumnLayout {
    id: root

    property var contactInfo: []
    property bool editable: false

    spacing: 4

    Repeater {
        id: lineRepeater
        model: root.contactInfo
        ContactInfoLine {
            Layout.fillWidth: true
            uri: modelData
            editable: root.editable
            onClicked: if (editable) {
                editInfoPopup.contactUri = modelData
                editInfoPopup.targetIndex = index
                editInfoPopup.open()
            }
            onDeletionRequested: root.deleteLine(index)
        }
    }

    Label {
        Layout.fillWidth: true
        horizontalAlignment: Qt.AlignHCenter
        visible: !root.editable && lineRepeater.count == 0
        font.italic: true
        enabled: false
        text: qsTr("Nulle information de contacto")
    }

    Button {
        Layout.alignment: Qt.AlignHCenter
        text: qsTr("Adde un nove methodo de contacto")
        visible: root.editable
        onClicked: newInfoPopup.open()
    }

    ContactInfoLinePopup {
        id: newInfoPopup

        onSaveInfoRequested: {
            console.log("Adding URL: " + uri)
            var tmp = root.contactInfo
            tmp.push(uri)
            root.contactInfo = tmp
        }
    }

    ContactInfoLinePopup {
        id: editInfoPopup

        property int targetIndex: -1

        onSaveInfoRequested: {
            console.log("Saving URL: " + uri)
            var tmp = root.contactInfo
            tmp[targetIndex] = uri
            root.contactInfo = tmp
        }
    }

    function deleteLine(index) {
        // TODO: ask for confirmation
        var tmp = root.contactInfo
        tmp.splice(index, 1)
        root.contactInfo = tmp
    }
}
