import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "ContactInfo.js" as ContactInfo

SwipeDelegate {
    id: root

    property var uri: ""
    property bool editable: false
    property var _infoTypes: ContactInfo.infoTypes
    property var parsed: ContactInfo.parseUri(uri)

    signal deletionRequested()

    swipe.enabled: root.editable
    swipe.right: Button {
        anchors.left: root.background.right
        height: parent.height
        visible: root.editable
        flat: true
        icon {
            source: "qrc:/icons/delete"
            color: "transparent"
        }
        onClicked: root.deletionRequested()
    }

    contentItem: ColumnLayout {
        HelperLabel {
            id: typeLabel
            Layout.fillWidth: true
            text: parsed.type.name
        }

        Text {
            id: valueField
            Layout.alignment: Qt.AlignHCenter
            textFormat: Text.StyledText
            text: parsed.type.showAsUrl ? '<a href="%1">%2</a>'.arg(root.uri).arg(parsed.value) : parsed.value
            onLinkActivated: Qt.openUrlExternally(link)
        }
    }
}
