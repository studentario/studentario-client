import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Window 2.12
import "ContactInfo.js" as ContactInfo

Popup {
    id: root

    property string contactUri: ""

    signal saveInfoRequested(var uri)

    modal: true
    onContactUriChanged: {
        var parsed = ContactInfo.parseUri(contactUri)
        var typeId = parsed.type.typeId
        for (var i = 0; i < _infoTypes.length; i++) {
            if (_infoTypes[i].typeId == typeId) {
                typeBox.currentIndex = i
                break
            }
        }
        textField.text = parsed.value
    }
    property var _infoTypes: ContactInfo.infoTypes

    ColumnLayout {
        anchors.fill: parent

        ComboBox {
            id: typeBox
            Layout.fillWidth: true
            model: _infoTypes
            textRole: "name"
        }

        TextField {
            id: textField
            Layout.fillWidth: true
            Layout.minimumWidth: textMetrics.advanceWidth
            Layout.maximumWidth: Window.width - 50
            placeholderText: _infoTypes[typeBox.currentIndex].placeholderText
            inputMethodHints: _infoTypes[typeBox.currentIndex].inputMethodHints
            selectByMouse: true
            onAccepted: root.addInfo()

            TextMetrics {
                id: textMetrics
                font: textField.font
                text: "Alicun parolas pro mesurar le texto XXXXXXX"
            }
        }

        Button {
            Layout.alignment: Qt.AlignHCenter
            text: qsTr("OK")
            onClicked: root.addInfo()
        }
    }

    function addInfo() {
        var type = _infoTypes[typeBox.currentIndex].typeId
        var text = textField.text
        var uri = ContactInfo.createUri(type, text)
        root.saveInfoRequested(uri)
        textField.text = ""
        root.close()
    }
}
