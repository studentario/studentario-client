import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

ColumnLayout {
    id: root

    property var selectedDates: computeDates()

    CenteredFlow {
        Layout.fillWidth: true
        rowSpacing: 8
        columnSpacing: 8

        Repeater {
            id: dayRepeater
            model: [
                qsTr("Lunedi"),
                qsTr("Martedi"),
                qsTr("Mercuridi"),
                qsTr("Jovedi"),
                qsTr("Venerdi"),
                qsTr("Sabbato"),
                qsTr("Dominica"),
            ]

            Button {
                text: modelData
                checkable: true
            }
        }
    }

    GridLayout {
        columns: 2
        HelperLabel {
            Layout.fillWidth: true
            text: qsTr("A partir del die:")
            wrapMode: Text.Wrap
        }

        HelperLabel {
            Layout.fillWidth: true
            text: qsTr("Usque al die:")
            wrapMode: Text.Wrap
        }

        DatePicker {
            id: date0
            title: qsTr("Initio del curso")
            minDate: {
                var d = new Date(); d.setFullYear(d.getFullYear() - 1); return d
            }
            maxDate: {
                var d = new Date(); d.setFullYear(d.getFullYear() + 5); return d
            }
            selectedDate: new Date()
        }

        DatePicker {
            id: date1
            title: qsTr("Fin del curso")
            minDate: new Date()
            maxDate: date0.maxDate
            selectedDate: new Date()
        }
    }

    function selectedDays() {
        var tmp = []
        for (var i = 0; i < dayRepeater.count; i++) {
            var btn = dayRepeater.itemAt(i)
            if (btn.checked) {
                // in Javascript, dominica es le numero 0
                tmp.push(i == 6 ? 0 : (i + 1))
            }
        }
        return tmp
    }

    function dateInDays(d) {
        d.setHours(0)
        d.setMinutes(0)
        d.setSeconds(0)
        d.setMilliseconds(0)
        return d
    }

    function nextDay(d) {
        d.setDate(d.getDate() + 1)
        return d
    }

    function computeDates() {
        var tmp = []
        var days = selectedDays()
        if (days.length == 0) return tmp

        var startDate = dateInDays(date0.selectedDate)
        var endDate = dateInDays(date1.selectedDate)
        for (var d = startDate; d <= endDate; d = nextDay(d)) {
            if (days.indexOf(d.getDay()) < 0) continue
            tmp.push(new Date(d))
        }
        return tmp
    }

    function clear() {
        for (var i = 0; i < dayRepeater.count; i++) {
            var btn = dayRepeater.itemAt(i)
            btn.checked = false
        }
    }
}
