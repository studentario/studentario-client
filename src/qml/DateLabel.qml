import QtQuick 2.12
import QtQuick.Controls 2.12

Label {
    id: root

    property var date: null
    property string noDateText: qsTr("Data non specificate")

    property date _date: {
        if (typeof root.date == "string") {
            return Date.fromLocaleDateString(Qt.locale(), root.date,
                                             "yyyy-MM-dd")
        } else if (root.date !== null) {
            return root.date
        }
        return new Date(NaN)
    }
    property bool _dateIsValid: !isNaN(_date)

    text: _dateIsValid ? Qt.formatDate(_date, "yyyy / MM / dd") : noDateText
    enabled: _dateIsValid
    font.italic: !_dateIsValid
}
