import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

RowLayout {
    id: root

    property alias minDate: popup.minDate
    property alias maxDate: popup.maxDate
    property alias title: popup.title
    property date selectedDate: null

    spacing: 8

    TextField {
        id: dateField
        inputMethodHints: Qt.ImhDate
        inputMask: "9999 / 99 / 99"
        text: root.selectedDate ? root.formatDate(root.selectedDate) : ""
        onEditingFinished: selectedDate = Date.fromLocaleDateString(Qt.locale(), text, "yyyy / MM / dd")
        onCursorPositionChanged: if (Qt.platform.os == "android") {
            // remedio pro https://bugreports.qt.io/browse/QTBUG-59411
            if (cursorPosition == 5 || cursorPosition == 10) {
                cursorPosition += 3
            }
        }
    }

    Button {
        icon {
            source: "qrc:/icons/calendar"
            color: "transparent"
        }
        onClicked: {
            if (root.selectedDate) {
                popup.selectedDate = root.selectedDate
            }
            popup.open()
        }
    }

    CalendarPopup {
        id: popup
        anchors.centerIn: Overlay.overlay
        selectedDate: root.selectedDate
        onAccepted: {
            dateField.text = root.formatDate(selectedDate)
            root.selectedDate = selectedDate
        }
    }

    function formatDate(d) {
        return Qt.formatDate(d, "yyyy / MM / dd")
    }

    function updateFromJson(text) {
        selectedDate = Date.fromLocaleDateString(Qt.locale(), text, "yyyy-MM-dd")
    }
}
