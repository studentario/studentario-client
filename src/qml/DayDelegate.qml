import QtQuick 2.12
import QtQuick.Controls 2.12

Item {
    property alias text: label.text
    property int radius: Math.min(width, height) + 2
    property bool selected: false
    property bool inCurrentMonth: true

    opacity: enabled && inCurrentMonth ? 1.0 : 0.4

    Rectangle {
        anchors.centerIn: parent
        width: parent.radius
        height: parent.radius
        color: "blue"
        radius: width / 2
        visible: parent.selected
    }
    Label {
        id: label
        anchors.fill: parent
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.bold: parent.selected
        color: parent.selected ? "white" : "black"
    }
}
