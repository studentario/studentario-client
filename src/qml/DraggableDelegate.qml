import QtQuick 2.9
import QtQuick.Controls 2.12

Item {
    id: root

    property int index
    property bool overTopHalf: false
    /* borderHeight tells how deep into this item (in the y direction) a drag
     * can go while still being considered being into the in-between area
     * between this item and the one next to it. */
    property int borderHeight: acceptDrops ? 10 : (contentItem.height / 2)
    property var contentItem: contentWrapper.children[0]
    default property alias _c: contentWrapper.data
    property bool dragged: false
    property bool covered: this == ListView.view.coveredItem
    property bool acceptDrops: false
    // true if it's covered in the central drop area
    property bool coveredForDrop: covered && _maybeCoveredForDrop
    property Item draggedItemParent: null
    property int expansion: contentItem.implicitHeight / 2
    property int hotSpotX: 0
    property int hotSpotY: 0

    // This is true if the last hover was in the central area; but it may also
    // be that the item is not covered anymore: use coveredForDrop instead!
    property bool _maybeCoveredForDrop: false
    implicitWidth: contentItem.implicitWidth
    implicitHeight: contentItem.implicitHeight

    states: [
        State {
            when: root.dragged
            name: "dragging"
            ParentChange {
                target: contentWrapper
                parent: draggedItemParent
            }
            PropertyChanges {
                target: contentWrapper
                dragScale: 0.5
                opacity: 0.8
            }
            PropertyChanges {
                target: root
                implicitHeight: 2
            }
        },
        State {
            when: coveredForDrop
            name: "receivingDrop"
        },
        State {
            when: root.covered && !root.overTopHalf
            name: "expandedBottom"
            PropertyChanges {
                target: root
                implicitHeight: contentItem.implicitHeight + root.expansion
            }
        },
        State {
            when: root.covered
            name: "expandedTop"
            extend: "expandedBottom"
            PropertyChanges {
                target: contentItem
                y: root.expansion
            }
            PropertyChanges {
                target: root
                implicitHeight: contentItem.implicitHeight + root.expansion
            }
        }
    ]
    transitions: [
        Transition {
            NumberAnimation { properties: "scale,implicitHeight,y"; duration: 200 }
        }
    ]

    Item {
        id: contentWrapper
        property real dragScale: 1.0
        transform: Scale {
            origin { x: root.hotSpotX; y: root.hotSpotY }
            xScale: contentWrapper.dragScale; yScale: contentWrapper.dragScale
        }
        implicitWidth: children[0].implicitWidth
        implicitHeight: children[0].implicitHeight
    }

    function resetPositions() {
        contentWrapper.x = 0
        contentWrapper.y = 0
        contentItem.x = 0
        contentItem.y = 0
    }

    function updateDrag(drag) {
        if (acceptDrops && !dragged &&
            drag.y >= borderHeight &&
            drag.y <= contentItem.height - borderHeight) {
            _maybeCoveredForDrop = true
        } else {
            _maybeCoveredForDrop = false
            // Find whether we are over the top or the bottom half
            overTopHalf = (drag.y <= contentItem.height / 2)
        }
    }
}
