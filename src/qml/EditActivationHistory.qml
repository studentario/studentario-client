import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import it.mardy.studentario 1.0

Page {
    id: root

    property var site: null
    property int userId: -1
    property bool changed: false

    header: TitleHeader {}
    title: qsTr("Historia del activationes")

    property var _changes: []
    property bool _waiting: false

    Component.onCompleted: refresh()

    ColumnLayout {
        anchors { fill: parent; margins: 12 }
        enabled: !root._waiting

        HelperLabel {
            Layout.fillWidth: true
            text: qsTr("Adde un nove status")
        }

        ActivationItem {
            id: activationItem
            Layout.fillWidth: true
            currentStatus: "active"
        }

        Button {
            Layout.alignment: Qt.AlignRight
            text: qsTr("Adde")
            onClicked: root.addStatusChange()
        }

        ErrorLabel {
            id: errorLabel
            Layout.fillWidth: true
        }

        GroupBox {
            Layout.fillWidth: true
            Layout.fillHeight: true
            title: qsTr("Activationes registrate")

            ListView {
                id: listView
                anchors.fill: parent
                model: root._changes
                clip: true
                delegate: StatusChangeDelegate {
                    width: listView.width
                    statusChange: modelData
                    onDeletionRequested: root.deleteStatusChange(modelData.changeId)
                }

                Label {
                    anchors.fill: parent
                    horizontalAlignment: Qt.AlignHCenter
                    verticalAlignment: Qt.AlignVCenter
                    visible: listView.count == 0
                    text: qsTr("Nulle cambio de stato.")
                    font.italic: true
                    wrapMode: Text.Wrap
                    textFormat: Text.StyledText
                }
            }
        }
    }

    BusyIndicator {
        anchors.centerIn: parent
        running: root._waiting
    }

    function addStatusChange() {
        errorLabel.clear()
        activationItem.sync()
        root._waiting = true

        var change = {
            'status': activationItem.currentStatus,
            'time': activationItem.statusChangeTime,
        }
        site.addUserStatusChange(root.userId, change, function(reply) {
            console.log("addUserStatusChange: " + JSON.stringify(reply.json))
            if (reply.status == 200) {
                root.changed = true
                root.refresh()
            } else {
                if (reply.json.code == 15) {
                    errorLabel.message =
                        qsTr("Le usator es jam in le stato desirate. Controla le data del cambio de stato.")
                } else {
                    errorLabel.error = reply.json
                }
                root._waiting = false
            }
        })
    }

    function deleteStatusChange(changeId) {
        root._waiting = true
        site.deleteUserStatusChange(changeId, function(reply) {
            if (reply.status == 200) {
                root.changed = true
                root.refresh()
            } else {
                console.warn("Could not delete change: " +
                             JSON.stringify(reply.json))
            }
        })
    }

    function refresh() {
        root._waiting = true
        site.loadUserStatusChanges(userId, function(reply) {
            if (reply.status == 200) {
                root._changes = reply.json.data
            } else {
                console.warn("Error getting status changes")
                errorLabel.error = reply.json
                root._changes = []
            }
            root._waiting = false
        })
    }
}
