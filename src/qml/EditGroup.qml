import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import it.mardy.studentario 1.0
import "TagType.js" as TagType

Page {
    id: root

    property var site: null
    property int groupId: -1
    property string name: ""
    property string description: ""
    property var tags: []
    property var location: ({})
    property bool editable: true
    property bool showEditButton: false

    signal done()

    property bool _editing: groupId >= 0 && editable
    property int _pendingOps: 0
    property alias _students: studentChangeRecorder.currentItems
    property alias _teachers: teacherChangeRecorder.currentItems

    header: TitleHeader {
        HeaderButton {
            text: "\u{1f589}" // Pencil
            ToolTip.text: qsTr("Modifica nomine e description")
            visible: root.showEditButton
            onClicked: {
                var page = stackView.push(Qt.resolvedUrl("EditGroup.qml"), {
                    'site': root.site,
                    'groupId': root.groupId,
                    'name': root.name,
                    'description': root.description,
                    'tags': root.tags,
                    'location': root.location,
                })
                page.done.connect(function() {
                    root.refreshGroup()
                    root.refreshMembers()
                    root.StackView.view.pop()
                    root.done()
                })
            }
        }
    }
    title: editable ? (_editing ? qsTr("Modifica gruppo") : qsTr("Nove gruppo"))
                    : qsTr("Gruppo %1").arg(name)

    Component.onCompleted: {
        nameField.forceActiveFocus(Qt.TabFocusReason)
        refreshMembers()
    }

    ScrollView {
        anchors.fill: parent
        contentWidth: width
        contentHeight: layout.implicitHeight + layout.anchors.topMargin

        ColumnLayout {
            id: layout
            anchors {
                fill: parent
                leftMargin: 12; rightMargin: 12; topMargin: 12
            }

            HelperLabel {
                Layout.fillWidth: true
                text: qsTr("Nomine del gruppo")
                visible: root.editable
            }
            TextField {
                id: nameField
                Layout.fillWidth: true
                Layout.bottomMargin: 10
                text: root.name
                visible: root.editable
            }

            HelperLabel {
                Layout.fillWidth: true
                text: qsTr("Description")
            }
            TextArea {
                id: descriptionField
                Layout.fillWidth: true
                Layout.bottomMargin: 10
                KeyNavigation.priority: KeyNavigation.BeforeItem
                KeyNavigation.tab: saveButton
                placeholderText: qsTr("Informationes libere super le gruppo")
                wrapMode: TextEdit.Wrap
                visible: root.editable
            }
            Label {
                Layout.fillWidth: true
                Layout.bottomMargin: 10
                wrapMode: TextEdit.Wrap
                text: root.description
                visible: !descriptionField.visible
            }

            RowLayout {
                Label {
                    text: qsTr("Location usual:")
                }

                LocationPicker {
                    id: locationPicker
                    site: root.site
                    editable: root.editable
                }
            }

            TagSelector {
                id: tagSelector
                Layout.fillWidth: true
                site: root.site
                editable: root.editable
                tagType: TagType.byTypeId("level")
                allTagsData: root.tags
            }

            GroupMembersByRole {
                Layout.fillWidth: true
                Layout.bottomMargin: 10
                title: qsTr("Inseniantes")
                emptyHint: qsTr("Nulle inseniante pro le gruppo. Pressar sur le symbolo <b>\"+\"</b> pro adder un.")
                model: root._teachers
                editable: root.editable
                onAddRequested: root.openMemberFinder(Roles.Teacher)
                onRemoveRequested: root.removeMember(Roles.Teacher, userId)
                onUserClicked: root.viewUser(model, index, Roles.Teacher)
            }

            GroupMembersByRole {
                Layout.fillWidth: true
                Layout.bottomMargin: 10
                title: qsTr("Studentes")
                emptyHint: qsTr("Nulle studente in le gruppo. Pressar sur le symbolo <b>\"+\"</b> pro adder un.")
                model: root._students
                editable: root.editable
                onAddRequested: root.openMemberFinder(Roles.Student)
                onRemoveRequested: root.removeMember(Roles.Student, userId)
                onUserClicked: root.viewUser(model, index, Roles.Student)
            }

            ErrorLabel {
                id: errorLabel
                Layout.fillWidth: true
            }

            Button {
                id: saveButton
                Layout.topMargin: 12
                Layout.bottomMargin: 12
                Layout.alignment: Qt.AlignHCenter
                Layout.minimumWidth: parent.width / 2
                text: qsTr("Salva")
                onClicked: root.save()
                visible: root.editable
            }
        }
    }

    CollectionChangeRecorder {
        id: studentChangeRecorder
        idField: "userId"
    }

    CollectionChangeRecorder {
        id: teacherChangeRecorder
        idField: "userId"
    }

    function viewUser(model, index, role) {
        var page = stackView.push(Qt.resolvedUrl("ViewUser.qml"), {
            'site': root.site,
            'role': role,
            'userId': model[index].userId,
            'editable': root.editable,
        })
        page.StackView.deactivating.connect(function() {
            if (page.changed) root.refreshMembers()
        })
    }

    function roleChangeRecorder(role) {
        return role == Roles.Teacher ? teacherChangeRecorder : studentChangeRecorder
    }

    function openMemberFinder(role) {
        var changeRecorder = roleChangeRecorder(role)
        var excludedUserIds = changeRecorder.currentItemIds
        var page = stackView.push(Qt.resolvedUrl("FindMembers.qml"), {
            'site': root.site,
            'role': role,
            'excludedUserIds': excludedUserIds,
            'actionName': qsTr("Adde al gruppo"),
        })
        page.done.connect(function() {
            var users = root.site.userCache.usersFromIds(page.userIds)
            changeRecorder.addItems(users)
            root.StackView.view.pop()
        })
    }

    function removeMember(role, userId) {
        var changeRecorder = roleChangeRecorder(role)
        changeRecorder.removeItem(userId)
    }

    function refreshGroup() {
        site.loadGroup(groupId, function(reply) {
            if (reply.status == 200) {
                var group = reply.json.data
                root.name = group.name
                root.description = group.description
                root.tags = group.tags
                root.location = group.location
            }
        })
    }

    function refreshMembers() {
        site.loadGroupMembers(groupId, Roles.Student, function(reply) {
            if (reply.status == 200) {
                studentChangeRecorder.initialItems = reply.json.data
            }
        })
        site.loadGroupMembers(groupId, Roles.Teacher, function(reply) {
            if (reply.status == 200) {
                teacherChangeRecorder.initialItems = reply.json.data
            }
        })
    }

    function save() {
        errorLabel.clear()
        if (nameField.text.length < 2) {
            errorLabel.message = qsTr("Le nomine debe haber al minus 2 litteras")
            return
        }

        var opCallback = function(reply) {
            _pendingOps--
            if (reply.status == 200) {
                if (_pendingOps == 0) {
                    if (!errorLabel.visible) root.done()
                }
            } else {
                errorLabel.error = reply.json
            }
        }

        var callback = function(reply) {
            if (reply.status == 200) {
                console.log("Gruppo salvate")
                var groupId = _editing ? root.groupId : reply.json.data.groupId
                var tagTarget = {
                    "groupIds": [ groupId ],
                }
                _pendingOps = 1
                if (tagSelector.addedTags.length > 0) {
                    _pendingOps++
                    site.assignTags(tagSelector.addedTags, tagTarget, opCallback)
                }
                if (tagSelector.removedTags.length > 0) {
                    _pendingOps++
                    site.unassignTags(tagSelector.removedTags, tagTarget, opCallback)
                }

                var roles = [ Roles.Student, Roles.Teacher ]
                for (var i = 0; i < roles.length; i++) {
                    var role = roles[i]
                    var changes = roleChangeRecorder(role)
                    if (changes.addedItems.length > 0) {
                        _pendingOps++
                        site.addGroupMembers(groupId, role,
                                             changes.addedItemIds, opCallback)
                    }
                    if (changes.removedItemIds.length > 0) {
                        _pendingOps++
                        site.removeGroupMembers(groupId, role,
                                                changes.removedItemIds, opCallback)
                    }
                }

                _pendingOps--
                if (_pendingOps == 0) root.done()
            } else {
                errorLabel.error = reply.json
            }
        }

        var group = {
            'name': nameField.text,
            'description': descriptionField.text,
            'locationId': locationPicker.locationId,
        }

        if (!root._editing) {
            site.createGroup(group, callback)
        } else {
            site.updateGroup(root.groupId, group, callback)
        }
    }
}
