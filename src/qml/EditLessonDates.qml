import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import it.mardy.studentario 1.0
import "DateUtils.js" as DateUtils

Page {
    id: root

    property var model: null

    signal done()

    header: TitleHeader {}
    title: qsTr("Nove lection — datas")

    ScrollView {
        anchors.fill: parent
        contentWidth: width
        contentHeight: layout.implicitHeight + 24

        ColumnLayout {
            id: layout
            anchors {
                fill: parent
                leftMargin: 12; rightMargin: 12
            }

            TabBar {
                id: tabBar
                Layout.fillWidth: true
                TabButton {
                    text: qsTr("Calendario")
                }

                TabButton {
                    text: qsTr("Generation")
                }
            }

            StackLayout {
                Layout.fillWidth: true
                Layout.fillHeight: false
                currentIndex: tabBar.currentIndex

                LessonTimeGenerator {
                    intervalsModel: root.model
                    onCreationRequested: root.done()

                    CalendarView {
                        id: calendarView
                        anchors.horizontalCenter: parent.horizontalCenter
                        selectMultiple: true
                        minDate: DateUtils.today()
                    }
                }

                LessonTimeGenerator {
                    intervalsModel: root.model
                    onCreationRequested: root.done()

                    DateGenerator {
                        anchors.fill: parent
                    }
                }
            }
        }
    }
}
