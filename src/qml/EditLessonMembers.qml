import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import it.mardy.studentario 1.0

ColumnLayout {
    id: root

    property var site: null
    property bool editable: true
    property var groupIds: []
    property var teacherIds: []
    property var studentIds: []
    property var location: _groups.length > 0 ? _groups[0].location : null

    property var _groups: []
    property var _teachers: []
    property var _students: []

    property bool _initializing: true
    property var _extraData: {
        'immutable': true,
        'subText': qsTr("Membro del gruppo"),
    }

    onGroupIdsChanged: if (!_initializing) reloadGroups(groupIds)
    Component.onCompleted: _initializing = false

    Button {
        Layout.alignment: Qt.AlignHCenter
        Layout.bottomMargin: 12
        text: qsTr("Selige le gruppos participante")
        onClicked: root.addGroup()
        visible: root.editable
    }

    Label {
        Layout.fillWidth: true
        text: qsTr("Gruppos selectionate:")
        visible: root._groups.length > 0
    }

    Repeater {
        model: root._groups
        GroupDelegate {
            Layout.fillWidth: true
            Layout.leftMargin: 8
            name: modelData.name
            description: modelData.description
            editable: root.editable
            onDeletionRequested: root.removeGroup(index)
        }
    }

    GroupMembersByRole {
        Layout.fillWidth: true
        title: qsTr("Inseniantes")
        emptyHint: root.editable ? qsTr("Nulle inseniante pro le lection. Pressar sur le symbolo <b>\"+\"</b> pro adder un.")
                                 : qsTr("Nulle inseniante pro le lection.")
        model: _teachers
        editable: root.editable
        onAddRequested: root.addMembers(Roles.Teacher)
        onRemoveRequested: root.removeMember(Roles.Teacher, userId)
        onUserClicked: root.viewUser(model, index, Roles.Teacher)
    }

    GroupMembersByRole {
        Layout.fillWidth: true
        title: qsTr("Studentes")
        emptyHint: root.editable ? qsTr("Nulle studente pro le lection. Pressar sur le symbolo <b>\"+\"</b> pro adder un.")
                                 : qsTr("Nulle studente pro le lection.")
        model: _students
        editable: root.editable
        onAddRequested: root.addMembers(Roles.Student)
        onRemoveRequested: root.removeMember(Roles.Student, userId)
        onUserClicked: root.viewUser(model, index, Roles.Student)
    }

    function targetFromRole(role) {
        return role == Roles.Teacher ? "_teachers" : "_students"
    }

    function setupGroups(lesson) {
        var wasInitializing = _initializing
        var tmp = []
        for (var i = 0; i < lesson.groups.length; i++) {
            var group = lesson.groups[i].group
            tmp.push(group)
            groupIds.push(group.groupId)
        }
        _groups = tmp
        _initializing = wasInitializing
    }

    function setupUsers(role, users) {
        var tmp = []
        for (var i = 0; i < users.length; i++) {
            var invitation = users[i]
            var user = invitation.user
            if (!invitation.invited) {
                Object.assign(user, root._extraData)
            }
            tmp.push(user)
        }
        root[targetFromRole(role)] = tmp
    }

    function setupMembers(lesson) {
        if (!_existing) return
        setupGroups(lesson)
        setupUsers(Roles.Student, lesson.students)
        setupUsers(Roles.Teacher, lesson.teachers)
    }

    function removeImmutableItems(target) {
        var items = root[target]
        var tmp = []
        for (var i = 0; i < items.length; i++) {
            var item = items[i]
            if (!item.immutable) {
                tmp.push(item)
            }
        }
        root[target] = tmp
    }

    function reloadGroups(groupIds) {
        // Remove tote le usatores que proveni del gruppos
        removeImmutableItems('_teachers')
        removeImmutableItems('_students')

        var extra = root._extraData
        var loadedGroups = []
        // https://stackoverflow.com/questions/750486
        var loadedGroupCb = function(index) {
            return function(reply) {
                loadedGroups[index] = reply.json.data
                remainingGroups -= 1
                if (remainingGroups == 0) {
                    root._groups = loadedGroups
                }
            }
        }
        var remainingGroups = groupIds.length
        for (var i = 0; i < groupIds.length; i++) {
            var groupId = groupIds[i]
            site.loadGroup(groupId, loadedGroupCb(i))
            site.loadGroupMembers(groupId, Roles.Student, function(reply) {
                if (reply.status == 200) {
                    addUsers('_students', reply.json.data, extra)
                }
            })
            site.loadGroupMembers(groupId, Roles.Teacher, function(reply) {
                if (reply.status == 200) {
                    addUsers('_teachers', reply.json.data, extra)
                }
            })
        }
        // Si nulle gruppo es selectionate:
        if (remainingGroups == 0) {
            root._groups = loadedGroups
        }
    }

    function addUsers(dest, users, extra) {
        var tmp = root[dest]
        for (var i = 0; i < users.length; i++) {
            var user = users[i]
            Object.assign(user, extra)
            tmp.push(user)
        }
        root[dest] = tmp
    }

    function addGroup() {
        var page = stackView.push(Qt.resolvedUrl("FindGroups.qml"), {
            'site': root.site,
            'editable': root.editable,
            'groupIds': root.groupIds,
        })
        page.done.connect(function() {
            root.groupIds = page.groupIds
            stackView.pop()
        })
    }

    function removeGroup(index) {
        var tmp = groupIds
        tmp.splice(index, 1)
        groupIds = tmp
    }

    function addMembers(role) {
        var page = stackView.push(Qt.resolvedUrl("FindMembers.qml"), {
            'site': root.site,
            'role': role,
            'actionName': qsTr("Adde al gruppo")
        })
        page.done.connect(function() {
            var users = root.site.userCache.usersFromIds(page.userIds)
            addUsers(targetFromRole(role), users, {})
            stackView.pop()
        })
    }

    function removeMember(role, userId) {
        var target = (role == Roles.Teacher) ? '_teachers' : '_students'
        var tmp = root[target]
        for (var i = 0; i < tmp.length; i++) {
            var user = tmp[i]
            if (user.userId == userId) {
                if (!user.immutable) tmp.splice(i, 1)
                break
            }
        }
        root[target] = tmp
    }

    function viewUser(model, index, role) {
        var page = stackView.push(Qt.resolvedUrl("ViewUser.qml"), {
            'site': root.site,
            'role': role,
            'userId': model[index].userId,
            'editable': root.editable,
        })
        page.StackView.deactivating.connect(function() {
            if (page.changed) {
                var user = root.site.userCache.userFromId(model[index].userId)
                model[index] = user
                root[targetFromRole(role)] = model
            }
        })
    }

    function updateIds(target, users) {
        var tmp = []
        for (var i = 0; i < users.length; i++) {
            var user = users[i]
            if (!user.immutable) {
                tmp.push(user.userId)
            }
        }
        root[target] = tmp
    }

    function sync() {
        updateIds('teacherIds', _teachers)
        updateIds('studentIds', _students)
    }
}
