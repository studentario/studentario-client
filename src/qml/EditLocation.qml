import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import it.mardy.studentario 1.0

Page {
    id: root

    property var site: null
    property int locationId: -1
    property string name: ""

    signal done()

    property bool _editing: locationId >= 0

    header: TitleHeader {}
    title: _editing ?  qsTr("Modifica location") : qsTr("Nove location")

    Component.onCompleted: {
        if (_editing) {
            site.loadLocation(locationId, function(reply) {
                if (reply.status == 200) {
                    var location = reply.json.data
                    nameField.text = location.name
                    descriptionField.text = location.description
                    colorPicker.selectedColor = location.color
                }
            })
        }
        nameField.forceActiveFocus(Qt.TabFocusReason)
    }

    ScrollView {
        anchors.fill: parent
        contentWidth: width

        ColumnLayout {
            anchors {
                fill: parent
                leftMargin: 12; rightMargin: 12
            }

            HelperLabel {
                Layout.fillWidth: true
                text: qsTr("Nomine del location")
            }
            TextField {
                id: nameField
                Layout.fillWidth: true
                Layout.bottomMargin: 10
                text: root.name
            }

            HelperLabel {
                Layout.fillWidth: true
                text: qsTr("Description")
            }
            TextArea {
                id: descriptionField
                Layout.fillWidth: true
                Layout.bottomMargin: 10
                KeyNavigation.priority: KeyNavigation.BeforeItem
                KeyNavigation.tab: saveButton
                placeholderText: qsTr("Informationes libere super le location")
                wrapMode: TextEdit.Wrap
            }

            RowLayout {
                Layout.fillWidth: true
                Layout.bottomMargin: 10

                Label {
                    text: qsTr("Color:")
                }

                ColorPicker {
                    id: colorPicker
                }
            }

            ErrorLabel {
                id: errorLabel
                Layout.fillWidth: true
            }

            Button {
                id: saveButton
                Layout.topMargin: 12
                Layout.bottomMargin: 12
                Layout.alignment: Qt.AlignHCenter
                Layout.minimumWidth: parent.width / 2
                text: qsTr("Salva")
                onClicked: root.save()
            }
        }
    }

    function save() {
        errorLabel.clear()
        if (nameField.text.length < 2) {
            errorLabel.message = qsTr("Le nomine debe haber al minus 2 litteras")
            return
        }

        var callback = function(reply) {
            if (reply.status == 200) {
                root.done()
            } else {
                errorLabel.error = reply.json
            }
        }

        var location = {
            'name': nameField.text,
            'description': descriptionField.text,
            'color': colorPicker.selectedColor.toString(),
        }

        if (!root._editing) {
            site.createLocation(location, callback)
        } else {
            site.updateLocation(root.locationId, location, callback)
        }
    }
}
