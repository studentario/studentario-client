import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Page {
    id: root

    property var site: null
    property var groupIds: []

    signal done()

    header: TitleHeader {
        HeaderSearchButton { id: searchWidget }
    }
    title: qsTr("Recerca gruppos")

    Component.onCompleted: refresh()

    ColumnLayout {
        anchors {
            fill: parent
            leftMargin: 12; rightMargin: 12
        }

        ListView {
            id: listView
            Layout.fillWidth: true
            Layout.fillHeight: true
            ScrollBar.vertical: ScrollBar {}
            model: groupModel.filtered
            delegate: CheckDelegate {
                width: listView.width
                text: qsTr("<b>%1</b> - %2").arg(modelData.name).
                    arg(modelData.description)
                checked: root.groupIds.indexOf(modelData.groupId) >= 0
                onToggled: root.updateItem(modelData.groupId, checked)
            }
        }

        Button {
            id: doneButton
            Layout.topMargin: 12
            Layout.bottomMargin: 12
            Layout.alignment: Qt.AlignHCenter
            Layout.minimumWidth: parent.width / 2
            text: qsTr("Adde le %n gruppo(s) selectionate", "", root.groupIds.length)
            enabled: root.groupIds.length > 0
            onClicked: root.done()
        }
    }

    GroupModelFilter {
        id: groupModel
        searchPattern: searchWidget.searchText
    }

    function refresh() {
        site.queryGroups({}, function(reply) {
            if (reply.status == 200) {
                groupModel.allGroups = reply.json.data
            }
        })
    }

    function updateItem(groupId, checked) {
        var pos = groupIds.indexOf(groupId)

        // If the group is already in the right state, nothing to do
        if ((pos >= 0 && checked) || (pos < 0 && !checked)) return

        var tmp = groupIds
        if (checked) {
            tmp.push(groupId)
        } else {
            tmp.splice(pos, 1)
        }
        groupIds = tmp
    }
}
