import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import it.mardy.studentario 1.0

Page {
    id: root

    property var site: null
    property int role: -1
    property var excludedUserIds: []
    property var userIds: []
    property alias actionName: doneButton.text

    signal done()

    header: TitleHeader {
        HeaderSearchButton { id: searchWidget }

        HeaderSortUsersButton {
            id: sortButton
            onSortChanged: root.refresh()
        }
    }
    title: {
        switch (root.role) {
        case Roles.Student: return qsTr("Recerca studentes")
        case Roles.Parent: return qsTr("Recerca parentes")
        case Roles.Teacher: return qsTr("Recerca inseniantes")
        case Roles.Admin: return qsTr("Recerca administratores")
        case Roles.Director: return qsTr("Recerca directores")
        }
    }

    Component.onCompleted: refresh()

    ColumnLayout {
        anchors {
            fill: parent
            leftMargin: 12; rightMargin: 12
        }

        ListView {
            id: listView
            Layout.fillWidth: true
            Layout.fillHeight: true
            ScrollBar.vertical: ScrollBar {}
            model: userModel.filtered
            delegate: CheckDelegate {
                width: listView.width
                text: modelData.name
                checked: root.userIds.indexOf(modelData.userId) >= 0
                onToggled: root.updateItem(modelData.userId, checked)
            }
        }

        Button {
            id: doneButton
            Layout.topMargin: 12
            Layout.bottomMargin: 12
            Layout.alignment: Qt.AlignHCenter
            Layout.minimumWidth: parent.width / 2
            enabled: root.userIds.length > 0
            onClicked: root.done()
        }
    }

    UserModel {
        id: userModel
        searchPattern: searchWidget.searchText
    }

    function refresh() {
        var r = Roles.info(root.role)
        var filter = {
            "sort": sortButton.sortOrder,
            [r.field]: 1,
        }
        site.queryUsers(filter, function(reply) {
            if (reply.status == 200) {
                userModel.allUsers = removeExcluded(reply.json.data)
            }
        })
    }

    function removeExcluded(allUsers) {
        var users = []
        for (var i = 0; i < allUsers.length; i++) {
            var user = allUsers[i]
            var index = root.excludedUserIds.indexOf(user.userId)
            if (index < 0) {
                users.push(user)
            }
        }
        return users
    }

    function updateItem(userId, checked) {
        var pos = userIds.indexOf(userId)

        // If the user is already in the right state, nothing to do
        if ((pos >= 0 && checked) || (pos < 0 && !checked)) return

        var tmp = userIds
        if (checked) {
            tmp.push(userId)
        } else {
            tmp.splice(pos, 1)
        }
        tmp.sort(function(a,b) { return a-b })
        userIds = tmp
    }
}
