import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Page {
    id: root

    property var site: null
    property var tagType
    property int parentTagId: -1
    property var excludedTagIds: []

    property var tagIds: []
    property var selectedTags: []

    signal done()

    header: TitleHeader {
        HeaderSearchButton { id: searchWidget }
    }
    title: tagType.titleFind

    Component.onCompleted: refresh()

    ColumnLayout {
        anchors {
            fill: parent
            leftMargin: 12; rightMargin: 12
        }

        ListView {
            id: listView
            Layout.fillWidth: true
            Layout.fillHeight: true
            ScrollBar.vertical: ScrollBar {}
            model: tagModel.filtered
            delegate: CheckDelegate {
                width: listView.width
                text: qsTr("<font color=\"%3\">\u{25A0}</font> <b>%1</b> - %2")
                    .arg(modelData.name).arg(modelData.description).arg(modelData.color)
                checked: root.tagIds.indexOf(modelData.tagId) >= 0
                onToggled: root.updateItem(modelData.tagId, checked)
            }
        }

        Button {
            id: doneButton
            Layout.topMargin: 12
            Layout.bottomMargin: 12
            Layout.alignment: Qt.AlignHCenter
            Layout.minimumWidth: parent.width / 2
            text: qsTr("Adde le %n elemento(s) selectionate", "", root.tagIds.length)
            enabled: root.tagIds.length > 0
            onClicked: root.save()
        }
    }

    TagModelFilter {
        id: tagModel
        searchPattern: searchWidget.searchText
    }

    function refresh() {
        var filter = {
            'parentTagId': root.parentTagId,
        }
        site.queryTags(filter, function(reply) {
            if (reply.status == 200) {
                tagModel.allTags = removeExcluded(reply.json.data)
            }
        })
    }

    function removeExcluded(allTags) {
        var tags = []
        for (var i = 0; i < allTags.length; i++) {
            var tag = allTags[i]
            var index = root.excludedTagIds.indexOf(tag.tagId)
            if (index < 0) {
                tags.push(tag)
            }
        }
        return tags
    }

    function updateItem(tagId, checked) {
        var pos = tagIds.indexOf(tagId)

        // If the tag is already in the right state, nothing to do
        if ((pos >= 0 && checked) || (pos < 0 && !checked)) return

        var tmp = tagIds
        if (checked) {
            tmp.push(tagId)
        } else {
            tmp.splice(pos, 1)
        }
        tagIds = tmp
    }

    function save() {
        selectedTags = []
        for (var i = 0; i < tagModel.allTags.length; i++) {
            var tag = tagModel.allTags[i]
            var index = tagIds.indexOf(tag.tagId)
            if (index >= 0) {
                selectedTags.push(tag)
            }
        }
        done()
    }
}
