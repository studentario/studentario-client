import QtQuick 2.12
import QtQuick.Layouts 1.12

Column {
    id: root

    property int spacing: 0

    onWidthChanged: computeColumns()
    onChildrenChanged: computeColumns()

    default property list<Item> items
    property bool _computing: false
    property int _rows: 1

    Repeater {
        id: repeater
        model: root._rows

        RowLayout {
            anchors { left: parent.left; right: parent.right }
            spacing: root.spacing
        }
    }

    function computeColumns() {
        if (_computing || width <= 0) return
        _computing = true
        var row = 0
        var itemsInRow = 0
        var rowImplicitWidth = 0
        for (var i = 0; i < items.length; i++) {
            var item = items[i]
            console.log("Item " + item)
            var rowLayout = repeater.itemAt(row)
            if (rowImplicitWidth + spacing + item.implicitWidth > width && itemsInRow > 0) {
                row++
                if (row >= _rows) {
                    _rows = row + 1
                }
                rowLayout = repeater.itemAt(row)
                itemsInRow = 0
                rowImplicitWidth = 0
            }
            rowImplicitWidth += item.implicitWidth
            if (itemsInRow > 0) rowImplicitWidth += spacing
            item.parent = rowLayout
            itemsInRow++
        }
        _computing = false
    }
}
