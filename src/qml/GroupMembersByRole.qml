import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

ColumnLayout {
    id: root

    property alias title: titleLabel.text
    property alias model: listView.model
    property alias emptyHint: emptyLabel.text
    property bool editable: true

    signal addRequested()
    signal removeRequested(int userId)
    signal userClicked(int index)

    RowLayout {
        Layout.fillWidth: true

        Label {
            id: titleLabel
            Layout.fillWidth: true
            horizontalAlignment: Qt.AlignHCenter
            font.pointSize: 12
        }

        Button {
            text: '+'
            font.pointSize: 14
            flat: true
            visible: root.editable
            onClicked: root.addRequested()
        }
    }

    Frame {
        Layout.fillWidth: true
        ListView {
            id: listView
            anchors.fill: parent
            implicitHeight: count > 0 ? contentHeight : (emptyLabel.implicitHeight + 48)
            delegate: UserDelegate {
                width: ListView.view.width
                editable: root.editable && !modelData.immutable
                name: modelData.subText ?
                    "%1<br/><font color=\"%3\" size=\"1\">%2</font>"
                        .arg(modelData.name).arg(modelData.subText)
                        .arg(inactivePalette.buttonText) :
                    modelData.name
                onDeletionRequested: root.removeRequested(modelData.userId)
                onClicked: root.userClicked(index)
            }
            interactive: false

            Label {
                id: emptyLabel
                anchors { fill: parent; margins: 24}
                visible: listView.count == 0
                font.italic: true
                horizontalAlignment: Qt.AlignHCenter
                wrapMode: Text.Wrap
                textFormat: Text.StyledText
            }
        }
    }

    SystemPalette { id: inactivePalette; colorGroup: SystemPalette.Disabled }
}
