import QtQml 2.12

JsonModelFilter {
    id: root
    property alias allGroups: root.allItems

    filterItemCb: function(group, pattern) {
        var words = group.name.split(' ').concat(group.description.split(' '))
        return anyStringStartsWith(words, pattern)
    }
}
