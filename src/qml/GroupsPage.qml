import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import it.mardy.studentario 1.0

Page {
    id: root

    property var site: null
    property bool editable: false

    header: TitleHeader {}
    title: qsTr("Gruppos")

    Component.onCompleted: refresh()

    ListView {
        id: listView
        anchors.fill: parent
        header: root.editable ? createGroupComponent : null
        delegate: GroupDelegate {
            width: listView.width
            editable: root.editable
            name: modelData.name
            description: modelData.description
            onDeletionRequested: root.deleteGroup(modelData.groupId, modelData.name)
            onClicked: {
                var page = stackView.push(Qt.resolvedUrl("EditGroup.qml"), {
                    'site': root.site,
                    'groupId': modelData.groupId,
                    'editable': false,
                    'showEditButton': root.editable,
                    'name': modelData.name,
                    'description': modelData.description,
                    'tags': modelData.tags,
                    'location': modelData.location,
                })
                page.done.connect(function() {
                    root.refresh()
                })
            }
        }

        ScrollBar.vertical: ScrollBar {}
    }

    Component {
        id: createGroupComponent
        Button {
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("Crea un nove gruppo")
            onClicked: {
                var page = stackView.push(Qt.resolvedUrl("EditGroup.qml"), {
                    'site': root.site,
                })
                page.done.connect(function() {
                    root.refresh()
                    root.StackView.view.pop()
                })
            }
        }
    }

    Dialog {
        id: deleteConfirmationDialog
        property string groupName: ""
        property int groupId: -1

        title: qsTr("Confirma le remotion")
        standardButtons: Dialog.Yes | Dialog.No
        modal: true
        anchors.centerIn: parent

        Label {
            text: qsTr("Esque tu es secur que tu vole deler le gruppo «%1»?").
                arg(deleteConfirmationDialog.groupName)
            wrapMode: Text.Wrap
        }

        onAccepted: {
            site.deleteGroup(groupId, function(reply) {
                if (reply.status == 200) {
                    root.refresh()
                }
            })
        }
    }

    function deleteGroup(groupId, groupName) {
        deleteConfirmationDialog.groupName = groupName
        deleteConfirmationDialog.groupId = groupId
        deleteConfirmationDialog.open()
    }

    function refresh() {
        var filter = {}
        site.queryGroups(filter, function(reply) {
            if (reply.status == 200) {
                listView.model = reply.json.data
            }
        })
    }
}
