import QtQuick.Controls 2.12

ToolButton {
    font.pointSize: 16
    hoverEnabled: true
    ToolTip.delay: 1000
    ToolTip.visible: hovered
}
