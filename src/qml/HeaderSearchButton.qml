import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Item {
    id: root

    property alias searchText: textField.text
    property bool expanded: false

    implicitWidth: button.implicitWidth + (expanded ? layout.implicitWidth : 0)
    implicitHeight: button.implicitHeight
    clip: true

    Behavior on implicitWidth {
        NumberAnimation {
            duration: 300
            easing.type: Easing.InOutQuad
        }
    }

    ToolButton {
        id: button
        anchors {
            top: parent.top; bottom: parent.bottom
            right: parent.right
        }
        text: "\u{1F50D}" // lente
        ToolTip.text: qsTr("Cerca")
        onClicked: root.toggle()
    }

    RowLayout {
        id: layout
        anchors {
            top: parent.top; bottom: parent.bottom
            right: button.left
        }

        TextField {
            id: textField
            Layout.fillWidth: true
            visible: root.expanded
            onVisibleChanged: if (visible) forceActiveFocus()
            placeholderText: qsTr("Search filter")
            rightPadding: clearButton.implicitWidth
            MouseArea {
                id: clearButton
                anchors {
                    top: parent.top; bottom: parent.bottom;
                    right: parent.right
                }
                implicitWidth: clearLabel.implicitWidth + 4
                visible: textField.text != ""
                onClicked: textField.clear()
                Label {
                    id: clearLabel
                    anchors { fill: parent }
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: "\u{1f5d9}" // "x"
                }
            }
        }
    }

    function toggle() {
        expanded = !expanded
    }
}
