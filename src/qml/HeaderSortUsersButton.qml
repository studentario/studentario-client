import QtQuick 2.12
import QtQuick.Controls 2.12

HeaderButton {
    id: root

    property alias sortOrder: sortPopup.sortOrder

    signal sortChanged()

    text: "\u{21F5}" // fleccias pro ordinamento
    ToolTip.text: qsTr("Cambia ordinamento")
    onClicked: sortPopup.open()
    SortPopup {
        id: sortPopup
        onClosed: root.sortChanged()
    }
}
