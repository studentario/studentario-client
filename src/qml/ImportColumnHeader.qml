import QtQml.Models 2.12
import QtQuick 2.12
import QtQuick.Controls 2.12

MouseArea {
    id: root

    property alias title: label.text
    property alias fieldModel: comboBox.model
    property int mappedFieldIndex: 0

    implicitWidth: column.implicitWidth
    implicitHeight: column.implicitHeight

    Column {
        id: column
        anchors.fill: parent

        Label {
            id: label
            anchors { left: parent.left; right: parent.right }
            elide: Text.ElideRight
            font.bold: true
        }
        Label {
            anchors { left: parent.left; right: parent.right }
            text: "\u2193" // down arrow
            horizontalAlignment: Text.AlignHCenter
        }
        ComboBox {
            id: comboBox
            property bool initialized: false
            anchors { left: parent.left; right: parent.right }
            textRole: "label"
            contentItem: Text {
                text: comboBox.displayText
                font: comboBox.font
                textFormat: Text.StyledText
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                elide: Text.ElideRight
            }

            indicator: null
            popup.width: 300
            popup.margins: 2

            onCurrentIndexChanged: if (initialized) {
                root.mappedFieldIndex = currentIndex
            }
            Component.onCompleted: {
                currentIndex = root.mappedFieldIndex
                initialized = true
            }
        }
    }
}
