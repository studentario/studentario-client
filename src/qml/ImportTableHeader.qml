import QtQuick 2.12
import QtQuick.Controls 2.12

ListView {
    id: root

    property var tableView: null
    // maps column title -> DB field
    property var fieldMapping: ({})

    signal columnTypeChanged(int column, string type)

    property var _fieldIndexMapping: ({})
    property var _mappings: [
        {
            label: qsTr("Nomine e familia"),
            field: "name",
            type: "QString"
        },
        {
            label: qsTr("Adresse"),
            field: "contactInfo/address",
            type: "QString"
        },
        {
            label: qsTr("Telephono"),
            field: "contactInfo/tel",
            type: "QString"
        },
        {
            label: qsTr("Posta electronic"),
            field: "contactInfo/mailto",
            type: "QString"
        },
        {
            label: qsTr("Accompaniator"),
            field: "contactInfo/parentName",
            type: "QString"
        },
        {
            label: qsTr("Data de nascentia"),
            field: "birthDate",
            type: "QDate"
        },
        {
            label: qsTr("Data de activation"),
            field: "status/time",
            type: "QDate"
        }
    ]

    contentX: tableView.contentX
    orientation: ListView.Horizontal
    contentHeight: contentItem.childrenRect.height
    interactive: false
    delegate: TableTitleDelegate {
        id: titleDelegate
        property int _updateCount: 0
        width: _updateCount, tableView.columnWidthProvider(index) + 4
        palette: systemPalette
        onResized: {
            tableView.updateWidth(index, newWidth - 4)
            updateTimer.start()
        }
        ImportColumnHeader {
            anchors.fill: parent
            title: modelData
            fieldModel: fieldListModel
            mappedFieldIndex: {
                var index = _fieldIndexMapping[modelData]
                return index ? index : 0
            }
            onMappedFieldIndexChanged: {
                root._fieldIndexMapping[modelData] = mappedFieldIndex
                if (mappedFieldIndex > 0) {
                    var mapping = root._mappings[mappedFieldIndex - 1]
                    var fieldName = mapping.field
                    root.fieldMapping[modelData] = fieldName
                    if (mapping.type) {
                        root.columnTypeChanged(index, mapping.type)
                    }
                } else {
                    root.columnTypeChanged(index, "QString")
                }
            }
        }

        Timer {
            id: updateTimer
            interval: 2
            repeat: false
            onTriggered: titleDelegate._updateCount++
        }
    }

    Component.onCompleted: prepareModel()

    SystemPalette { id: systemPalette }

    ListModel {
        id: fieldListModel
        ListElement {
            label: "\u274c" // cross mark
            field: ""
        }
    }

    function prepareModel() {
        for (var i = 0; i < _mappings.length; i++) {
            var mapping = _mappings[i]
            fieldListModel.append(mapping)
        }
    }
}
