import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Dialogs 1.0
import QtQuick.Layouts 1.12
import it.mardy.studentario 1.0
import "ContactInfo.js" as ContactInfo

Page {
    id: root

    property var site: null
    property int role: -1
    property int userId: -1
    property string name: ""

    signal done()

    property int _lastImportedRow: -1
    property var _fieldByColumn: ({})
    property int _importedCount: 0
    property int _errorCount: 0

    header: TitleHeader {}
    title: {
        switch (root.role) {
        case Roles.Student: return qsTr("Importa studentes")
        case Roles.Parent: return qsTr("Importa parentes (accompaniatores)")
        case Roles.Teacher: return qsTr("Importa inseniantes")
        case Roles.Admin: return qsTr("Importa administratores")
        case Roles.Director: return qsTr("Importa directores")
        }
    }

    states: [
        State {
            name: "importing"
            PropertyChanges { target: importingPopup; visible: true }
            PropertyChanges { target: busyIndicator; running: true }
            PropertyChanges {
                target: popupLabel
                text: qsTr("Importation in curso…")
            }
        },
        State {
            name: "finished"
            extend: "importing"
            PropertyChanges { target: busyIndicator; visible: false }
            PropertyChanges {
                target: popupLabel
                text: qsTr("<h3>Importation terminate</h3><br/>Importate <b>%1</b> elementos, <b>%2</b> errores.").arg(root._importedCount).arg(root._errorCount)
            }
            PropertyChanges {
                target: importingPopup
                standardButtons: Dialog.Ok
            }
            PropertyChanges { target: importButton; enabled: false }
        }
    ]

    ColumnLayout {
        anchors {
            fill: parent
            leftMargin: 12; rightMargin: 12
        }

        Button {
            Layout.topMargin: 12
            Layout.bottomMargin: 12
            Layout.alignment: Qt.AlignHCenter
            Layout.minimumWidth: parent.width / 2
            text: qsTr("Aperi un archivo CSV")
            onClicked: fileDialog.open()
        }

        ErrorLabel {
            id: errorLabel
            Layout.fillWidth: true
        }

        ImportTableHeader {
            id: tableHeader
            Layout.fillWidth: true
            Layout.preferredHeight: contentHeight
            model: csvModel.columnTitles
            tableView: tableView
            onColumnTypeChanged: csvModel.setColumnType(column, type)
        }

        TableView {
            id: tableView
            Layout.fillWidth: true
            Layout.fillHeight: true
            ScrollBar.horizontal: ScrollBar {}
            ScrollBar.vertical: ScrollBar {}
            model: csvModel
            rowSpacing: 1
            columnSpacing: 4
            clip: true
            delegate: Item {
                implicitWidth: label.implicitWidth + 4
                implicitHeight: label.implicitHeight + 4
                Label {
                    id: label
                    anchors.fill: parent
                    text: model.value
                    elide: Text.ElideRight
                }
            }
            property var _columnWidths: []
            columnWidthProvider: function (column) {
                return _columnWidths.length > column ?
                    _columnWidths[column] : 120
            }

            function updateWidth(index, width) {
                if (_columnWidths.length < columns) {
                    _columnWidths = Array(columns).fill(120)
                }
                _columnWidths[index] = width
                forceLayout()
            }
        }

        Button {
            id: importButton
            Layout.topMargin: 12
            Layout.bottomMargin: 12
            Layout.alignment: Qt.AlignHCenter
            Layout.minimumWidth: parent.width / 2
            text: qsTr("Importa")
            enabled: tableView.rows > 0
            onClicked: root.importUsers()
        }
    }

    FileDialog {
        id: fileDialog
        title: qsTr("Selige un archivo CSV")
        selectExisting: true
        selectMultiple: false
        nameFilters: [ "Archivos CSV (*.csv)" ]
        onAccepted: {
            console.log("Dialog accepted: " + fileUrl)
            csvModel.filePath = fileUrl.toString().replace("file://", "")
        }

    }

    CsvModel { id: csvModel }

    Dialog {
        id: importingPopup
        width: 300
        height: 200
        anchors.centerIn: parent
        modal: true
        standardButtons: Dialog.Cancel
        onRejected: root.state = ""
        onAccepted: root.done()
        ColumnLayout {
            anchors.fill: parent
            BusyIndicator { id: busyIndicator }
            Label {
                id: popupLabel
                Layout.fillWidth: true
                wrapMode: Text.Wrap
                textFormat: Text.StyledText
            }
        }
    }

    function createUsers(users, callback) {
        var creationCallback = function(reply) {
            var responseText = JSON.stringify(reply.json)
            if (reply.status == 200) {
                console.log("Usatores salvate: " + responseText)
                var length = reply.json.length
                for (var i = 0; i < length; i++) {
                    var rowData = reply.json[i]
                    if (rowData.status == "ok") {
                        _importedCount++
                    } else {
                        _errorCount++
                    }
                }
                callback()
            } else {
                errorLabel.error = responseText
            }
        }

        site.createUser(users, creationCallback)
    }

    function importBatch(callback) {
        csvModel.fetchMore(csvModel.rootIndex)
        var loadedRows = csvModel.loadedRowCount
        var oldLastImportedRow = _lastImportedRow
        var roleField = Roles.info(role).field

        console.log("New batch")
        var users = []
        for (var row = _lastImportedRow + 1; row < loadedRows; row++) {
            var user = {}
            user[roleField] = true
            var contactInfo = []
            var status = {}
            for (var col in root._fieldByColumn) {
                var fieldName = root._fieldByColumn[col]
                var value = csvModel.get(row, col, "value")
                if (!value) continue

                if (fieldName.indexOf('/') > 0) {
                    // campo composite (p.ex. "contactInfo/addresse")
                    var parts = fieldName.split('/', 2)
                    if (parts[0] == "contactInfo") {
                        var typeId = parts[1]
                        var uri = ContactInfo.createUri(typeId, value)
                        contactInfo.push(uri)
                    } else if (parts[0] == "status") {
                        status[parts[1]] = value
                    } else {
                        console.warn("Unknown field " + parts[0])
                    }
                } else {
                    user[fieldName] = value
                }
            }
            if (contactInfo.length > 0) {
                user["contactInfo"] = contactInfo
            }
            if (Object.keys(status).length > 0) {
                status["status"] = "active"
                user["status"] = status
            }
            // Un minimo de validation
            if (!user.name) continue
            console.log("row " + row + ": " + JSON.stringify(user))
            users.push(user)
            _lastImportedRow = row
        }
        var hasItems = _lastImportedRow > oldLastImportedRow
        if (hasItems) {
            createUsers(users, function() {
                callback(hasItems)
            })
        } else {
            callback(hasItems)  // hasItems is false
        }
    }

    function importUsers() {
        errorLabel.clear()
        _importedCount = 0
        _errorCount = 0

        var columnTitles = csvModel.columnTitles
        var mappings = tableHeader.fieldMapping

        var fieldCount = 0
        for (var columnTitle in mappings) {
            var fieldName = mappings[columnTitle]
            var columnIndex = columnTitles.indexOf(columnTitle)
            root._fieldByColumn[columnIndex] = fieldName
            fieldCount++
        }

        if (fieldCount == 0) {
            errorLabel.message =
                qsTr("Nulle campo seligite pro le importation")
            return
        }

        root.state = "importing"
        var callback = function(hasItems) {
            if (root.state != "importing") {
                console.log("import cancelled")
            } else if (hasItems) {
                console.log("importing more")
                importBatch(callback)
            } else {
                console.log("done")
                root.state = "finished"
            }
        }
        importBatch(callback)
    }
}
