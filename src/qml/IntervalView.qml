import QtQuick 2.12
import QtQuick.Controls 2.12
import it.mardy.studentario 1.0

ListView {
    id: root

    signal deletionRequested(int index)

    implicitHeight: contentHeight
    implicitWidth: contentWidth
    delegate: SwipeDelegate {
        width: root.width
        text: qsTr("%1 - %2").arg(Qt.formatDateTime(model.start)).
            arg(Utils.formatDuration(model.end - model.start))
        swipe.right: Button {
            anchors.right: parent.right
            height: parent.height
            flat: true
            icon {
                source: "qrc:/icons/delete"
                color: "transparent"
            }
            onClicked: root.deletionRequested(index)
        }
    }
}
