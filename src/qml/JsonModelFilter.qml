import QtQml 2.12

QtObject {
    id: root
    property string searchPattern: ""
    property var allItems: []
    property var filtered: []
    property var filterItemCb: null

    property string _oldPattern: ""

    onSearchPatternChanged: {
        if (searchPattern.indexOf(_oldPattern) >= 0) {
            // not pote filtrar le resultatos precedente
            filterItems(filtered)
        } else {
            filterItems(allItems)
        }
    }
    onAllItemsChanged: filterItems(allItems)

    function simplify(s) {
        return s.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase()
    }

    function anyStringStartsWith(stringList, prefix) {
        for (var i = 0; i < stringList.length; i++) {
            var s = simplify(stringList[i])
            if (s.startsWith(prefix)) return true
        }
        return false
    }

    function filterItems(items) {
        var cleanPattern = searchPattern.toLowerCase()
        if (!cleanPattern) {
            filtered = allItems
            return;
        }
        var tmp = []
        for (var i = 0; i < items.length; i++) {
            var item = items[i]
            if (filterItemCb(item, cleanPattern)) {
                tmp.push(item)
            }
        }
        filtered = tmp
        _oldPattern = searchPattern
    }
}
