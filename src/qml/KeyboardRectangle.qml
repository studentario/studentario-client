import QtQuick 2.12
import QtQuick.Window 2.12

Item {
    id: keyboardRect
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.bottom: parent.bottom
    height: Qt.inputMethod.visible ?
        Math.max(Qt.inputMethod.keyboardRectangle.height / Screen.devicePixelRatio, 0) : 0

    Behavior on height {
        NumberAnimation {
            duration: 300
            easing.type: Easing.InOutQuad
        }
    }
}
