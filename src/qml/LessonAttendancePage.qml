import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Page {
    id: root

    property var site: null
    property var lessonData: ({})
    property var attendanceData: ({})
    property int lessonId: -1

    header: TitleHeader {}
    title: qsTr("Registra participation")

    Component.onCompleted: refresh()

    ScrollView {
        anchors.fill: parent
        contentWidth: width
        contentHeight: layout.implicitHeight + 24

        ColumnLayout {
            id: layout
            anchors { fill: parent; margins: 12 }

            Label {
                Layout.fillWidth: true
                text: qsTr("Lection del <b>%1</b> al horas <b>%2</b>")
                    .arg(Qt.formatDate(new Date(lessonData.startTime)))
                    .arg(Qt.formatTime(new Date(lessonData.startTime)))
                wrapMode: Text.Wrap
            }

            Repeater {
                id: studentRepeater
                model: attendanceData.students
                CheckDelegate {
                    Layout.fillWidth: true
                    text: modelData.user.name
                    checked: modelData.attended
                }
            }

            ErrorLabel {
                id: errorLabel
                Layout.fillWidth: true
            }

            Button {
                id: saveButton
                Layout.topMargin: 12
                Layout.bottomMargin: 12
                Layout.alignment: Qt.AlignHCenter
                Layout.minimumWidth: parent.width / 2
                text: qsTr("Salva")
                onClicked: root.save()
            }
        }
    }

    function refresh() {
        var filter = {
            'userFields': 'name',
        }

        site.queryLessonAttendance(lessonId, filter, function(reply) {
            if (reply.status == 200) {
                attendanceData = reply.json.data
                console.log("Got lesson attendance: " + JSON.stringify(reply.json.data))
            }
        })
    }

    function save() {
        errorLabel.clear()

        var participants = {}
        var students = []
        var model = studentRepeater.model
        for (var i = 0; i < model.length; i++) {
            var p = model[i]
            console.log("Participation: " + JSON.stringify(p))
            var a = {
                'attended': studentRepeater.itemAt(i).checked,
                'userId': p.user.userId,
            }
            students.push(a)
        }
        participants['students'] = students

        site.updateLessonAttendance(lessonId, participants, function(reply) {
            if (reply.status == 200) {
                root.StackView.view.pop()
            } else {
                errorLabel.error = reply.json
            }
        })
    }
}
