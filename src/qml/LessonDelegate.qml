import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import it.mardy.studentario 1.0

SwipeDelegate {
    id: root

    property bool editable: false
    property date startTime
    property date endTime
    property var groups: []
    property var students: []
    property var disabledPalette: null

    signal openRequested()
    signal editRequested()
    signal deletionRequested()

    contentItem: ColumnLayout {
        Label {
            Layout.fillWidth: true
            text: qsTr("%1 - %2")
                .arg(Qt.formatDateTime(root.startTime))
                .arg(Utils.formatDuration(root.endTime - root.startTime))
            font: root.font
            elide: Text.ElideRight
        }
        Label {
            Layout.fillWidth: true
            text: describeParticipants()
            font.pointSize: root.font.pointSize * 0.8
            elide: Text.ElideRight
            color: disabledPalette.buttonText
        }
    }
    swipe.enabled: root.editable
    swipe.left: Button {
        anchors.left: parent.left
        height: parent.height
        visible: root.editable
        flat: true
        text: "\u{1F4D3}"
        onClicked: { root.openRequested(); swipe.close() }
    }
    swipe.right: Button {
        anchors.right: parent.right
        height: parent.height
        enabled: root.editable
        flat: true
        icon {
            source: "qrc:/icons/delete"
            color: "transparent"
        }
        onClicked: root.deletionRequested()
    }
    onClicked: if (editable) {
        root.editRequested()
    } else {
        root.openRequested()
    }

    function describeParticipants() {
        var members = []
        for (var i = 0; i < groups.length; i++) {
            members.push(groups[i].group.name)
        }
        for (var i = 0; i < students.length; i++) {
            var student = students[i]
            // Adde slmente studentes qui non es membros del gruppos
            if (student.invited) {
                members.push(student.user.name)
            }
        }
        return members.join(qsTr(', '))
    }
}
