import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import it.mardy.studentario 1.0

ColumnLayout {
    id: root
    property date plannerDate
    property bool editable: false
    property var baseFilters: ({})
    property alias viewOffsetY: plannerTable.contentY

    property var columnTitles: []
    property alias columnValues: plannerModel.columnValues
    property alias columnKey: plannerModel.columnKey
    property alias activities: plannerModel.activities
    property var columnKeys: []

    property date endTime: endOfDay(plannerDate)

    signal createRequested(int columnIndex, date startTime)
    signal editRequested(var lesson)
    signal viewOffsetChangeRequested()

    Component.onCompleted: loadActivities()

    Connections {
        target: site
        onLessonsChanged: loadActivities()
    }

    Flickable {
        id: plannerTitleBar
        Layout.fillWidth: true
        implicitHeight: row.implicitHeight
        property int columnsStart: plannerTable.timeWidth
        contentHeight: row.height
        contentWidth: row.width
        contentX: plannerTable.contentX
        interactive: false

        Row {
            id: row
            x: plannerTitleBar.columnsStart
            Repeater {
                model: root.columnTitles
                Label {
                    width: plannerTable.columnWidth
                    text: modelData
                }
            }
        }
    }

    PlannerTable {
        id: plannerTable
        Layout.fillWidth: true
        Layout.fillHeight: true
        numColumns: root.columnTitles.length
        clip: true
        activities: plannerModel.activities
        onMovementEnded: root.viewOffsetChangeRequested()
        delegate: LessonPlannerItemDelegate {
            anchors { fill: parent; leftMargin: 2; rightMargin: 2 }
            lesson: activity
            palette: systemPalette
            columnKeys: root.columnKeys
            columnKey: root.columnKey
            onEditRequested: root.editRequested(activity)
        }

        onCreateLessonRequested: {
            if (column < 0 || column >= root.columnValues.length) {
                return
            }
            var startMs = plannerModel.startTime.getTime()
            var endMs = plannerModel.endTime.getTime()
            var time = new Date(startMs + startRatio * (endMs - startMs))
            root.createRequested(column, time)
        }
    }

    PlannerModel {
        id: plannerModel
        startTime: root.plannerDate
        endTime: root.endTime
    }

    SystemPalette { id: systemPalette }

    function endOfDay(d) {
        console.log("Planner date is " + Qt.formatDateTime(d))
        return new Date(d.getFullYear(), d.getMonth(), d.getDate(),
                        23, 59, 59)
    }

    function loadActivities() {
        plannerModel.clear()
        loadSavedActivities()
    }

    function loadSavedActivities() {
        var filter = {
            'since': Qt.formatDateTime(plannerDate, Qt.ISODate),
            'to': Qt.formatDateTime(endTime, Qt.ISODate),
            'userFields': 'name',
        }
        Object.assign(filter, root.baseFilters)

        site.queryLessons(filter, function(reply) {
            if (reply.status == 200) {
                var lessons = reply.json.data
                for (var i = 0; i < lessons.length; i++) {
                    var lesson = lessons[i]
                    if (lesson.teachers.length > 0) {
                        lesson['teacherName'] = lesson.teachers[0].user.name
                    }
                    lesson['locationName'] = lesson.location.name
                    plannerModel.addActivity(lesson)
                }
            }
        })
    }

    function describeLesson(lesson) {
        var desc = lesson.groups.map(function(g) { return g.group.name }).join(', ')
        var rows = [desc]
        for (var i = 0; i < columnKeys.length; i++) {
            var key = columnKeys[i]
            if (key == columnKey) continue;

            rows.push(lesson[key])
        }
        return rows.join('\n')
    }
}
