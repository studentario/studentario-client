import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

MouseArea {
    id: root

    property var palette
    property var lesson: null
    property string columnKey: ""
    property var columnKeys: []

    signal editRequested()

    // Forsan nos addera un popup con plus actiones: modifica, elimina, move...
    onClicked: editRequested()

    Rectangle {
        anchors.fill: parent
        border { color: palette.text; width: 1 }
        radius: 3
        color: root.lesson.overlapCount > 0 ? "red" : palette.base
        opacity: root.lesson.overlapCount > 0 ? 0.5 : 1.0
    }

    Label {
        anchors { fill: parent; margins: 2 }
        text: root.describeLesson(activity)
    }

    function describeLesson(lesson) {
        var desc = lesson.groups.map(function(g) { return g.group.name }).join(', ')
        var rows = [desc]
        for (var i = 0; i < columnKeys.length; i++) {
            var key = columnKeys[i]
            if (key == columnKey) continue;

            rows.push(lesson[key])
        }
        return rows.join('\n')
    }
}
