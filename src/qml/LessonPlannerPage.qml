import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Page {
    id: root

    property var site: null
    property bool editable: false
    property int role: -1
    property int userId: -1

    property var columnValues: ready ? columnValueMap[columnKey] : []
    property int columnKeyIndex: 0
    property var columnKeys: ["locationName", "teacherName"]
    property var columnIdKeys: ["locationId", "teacherId"]
    property var columnKeyChoices: [
        qsTr("Per location"),
        qsTr("Per inseniante")
    ]
    property string columnKey: columnKeys[columnKeyIndex]
    property var columnValueMap: ({})
    property var locations: [] // Or we could have a locationCache in Site
    property bool ready: false

    signal createRequested(var lessonData)
    signal editRequested(var lesson)
    signal done()

    header: TitleHeader {}
    title: qsTr("Programmation del lectiones")

    Component.onCompleted: loadColumns()

    ColumnLayout {
        anchors.fill: parent

        RowLayout {
            Layout.fillWidth: true
            Layout.leftMargin: 8
            Layout.rightMargin: 8

            Button {
                text: "<"
                onClicked: listView.decrementCurrentIndex()
            }

            Label {
                text: Qt.formatDate(listView.currentItem.plannerDate)
            }

            Button {
                text: ">"
                onClicked: listView.incrementCurrentIndex()
            }

            Item {
                Layout.fillWidth: true
            }

            ComboBox {
                Layout.preferredWidth: Math.max(implicitWidth, 200)
                model: root.columnKeyChoices
                onActivated: root.columnKeyIndex = currentIndex
            }
        }

        ListView {
            id: listView

            property int viewOffsetY: 0
            Layout.fillWidth: true
            Layout.fillHeight: true
            model: 365 * 2
            currentIndex: root.indexForToday()
            delegate: LessonPlannerDelegate {
                // Si le largor es nulle, tote delegatos va esser instantiate!
                width: Math.max(listView.width, 10)
                height: listView.height
                editable: root.editable
                plannerDate: root.dateByIndex(index)
                columnTitles: root.columnValues
                columnValues: root.columnValues
                columnKey: root.columnKey
                columnKeys: root.columnKeys
                viewOffsetY: listView.viewOffsetY
                onViewOffsetChangeRequested: listView.viewOffsetY = viewOffsetY
                onEditRequested: root.editRequested(lesson)
                onCreateRequested: root.handleCreationRequest(columnIndex, startTime)
            }
            orientation: Qt.Horizontal
            interactive: false
            highlightRangeMode: ListView.StrictlyEnforceRange
            highlightMoveDuration: 500
            highlightMoveVelocity: -1
            cacheBuffer: 0
        }
    }

    function refresh() {
        listView.currentItem.loadActivities()
    }

    function dateByIndex(index) {
        var now = new Date()
        return new Date(now.getFullYear(), 0, index + 1, 0, 0, 0)
    }

    function indexForToday() {
        var now = new Date()
        now.setHours(0, 0, 0)
        var start = new Date(now.getFullYear(), 0, 1)
        var diff = now - start
        var oneDay = 1000 * 60 * 60 * 24
        return Math.floor(diff / oneDay)
    }

    function loadColumns() {
        var pendingCalls = 2

        function checkReady(reply, dataExtractionCb) {
            if (reply.status == 200) {
                dataExtractionCb(reply)
                pendingCalls--
                if (pendingCalls == 0) ready = true
            }
        }

        site.queryLocations({}, function(reply) {
            checkReady(reply, function(reply) {
                var tmp = reply.json.data.map(function(l) { return l.name })
                root.columnValueMap["locationName"] = tmp
                root.locations = reply.json.data
            })
        })

        site.queryUsers({'isTeacher': 1, 'sort': 'name'},
                        function(reply) {
            checkReady(reply, function(reply) {
                var tmp = reply.json.data.map(function(u) { return u.name })
                root.columnValueMap["teacherName"] = tmp
                tmp = reply.json.data.map(function(u) { return u.userId })
                root.columnValueMap["teacherId"] = tmp
            })
        })
    }

    function handleCreationRequest(column, startTime) {
        var lessonDuration = 60 // minutes
        var startMinutes = startTime.getMinutes()
        startMinutes = Math.round(startMinutes / 5) * 5
        startTime.setMinutes(startMinutes, 0, 0)
        var endTime = new Date(startTime.getTime() + lessonDuration * 60000)
        var lessonData = {
            'lessonId': -1,
            'startTime': startTime,
            'endTime': endTime,
            'groups': [],
            'students': [],
            'teachers': [],
        }
        switch (columnKey) {
        case "locationName":
            lessonData.location = root.locations[column]
            break;
        case "teacherName":
            var teacherId = root.columnValueMap["teacherId"][column]
            var teacher = root.site.userCache.userFromId(teacherId)
            lessonData.teachers = [{'user': teacher}]
            break;
        }
        console.log("Creating lesson with " + JSON.stringify(lessonData))
        root.createRequested(lessonData)
    }
}
