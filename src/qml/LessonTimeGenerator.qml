import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

ColumnLayout {
    id: root

    default property alias data: contentItem.data
    property var dateSelector: contentItem.children[0]
    property var intervalsModel: null

    signal creationRequested()

    TimeInterval {
        id: timeInterval
        Layout.alignment: Qt.AlignHCenter
    }

    Item {
        id: contentItem
        Layout.fillWidth: true
        implicitWidth: contentItem.children[0].implicitWidth
        implicitHeight: contentItem.children[0].implicitHeight
    }

    Button {
        Layout.topMargin: 12
        Layout.bottomMargin: 12
        Layout.alignment: Qt.AlignHCenter
        Layout.minimumWidth: parent.width / 2
        enabled: root.dateSelector.selectedDates.length > 0
        text: qsTr("Adde le lectiones")
        onClicked: { root.addIntervals(); root.dateSelector.clear() }
    }

    GroupBox {
        Layout.fillWidth: true
        title: qsTr("Data e hora del lectiones")

        IntervalView {
            anchors.fill: parent
            model: root.intervalsModel
            interactive: false
            onDeletionRequested: root.deleteInterval(index)
        }
    }

    Button {
        Layout.topMargin: 12
        Layout.bottomMargin: 12
        Layout.alignment: Qt.AlignHCenter
        Layout.minimumWidth: parent.width / 2
        enabled: root.intervalsModel.count > 0
        text: qsTr("Crea iste lectiones")
        onClicked: root.creationRequested()
    }

    function setMinutes(d, minutes) {
        var t = new Date(d)
        t.setMinutes(minutes)
        return t
    }

    function findInsertionPoint(model, startIndex, time) {
        var i
        var absTime = time.getTime()
        for (i = startIndex; i < model.count; i++) {
            var t = model.get(i).start.getTime()
            if (absTime < t) {
                break
            }
        }
        return i
    }

    function addIntervals() {
        var tmp = []
        var dates = root.dateSelector.selectedDates
        var modelIndex = 0
        for (var i = 0; i < dates.length; i++) {
            var d = dates[i]
            var startTime = setMinutes(d, timeInterval.startMinutes)
            var endTime = setMinutes(d, timeInterval.endMinutes)
            modelIndex = findInsertionPoint(model, modelIndex, startTime)
            model.insert(modelIndex, {
                'start': startTime,
                'end': endTime,
            })
            modelIndex++
        }
    }

    function deleteInterval(index) {
        model.remove(index)
    }
}
