import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import it.mardy.studentario 1.0
import "DateUtils.js" as DateUtils

Page {
    id: root

    property var site: null
    property bool editable: false
    property int role: -1
    property int userId: -1
    property int pastDaysCount: -1 // show all past lessons

    header: TitleHeader {
        HeaderButton {
            text: "\u{1F3A2}" // Montanias russe :-)
            ToolTip.text: qsTr("Programmation de lectiones")
            visible: root.editable
            onClicked: {
                var page = stackView.push(Qt.resolvedUrl("LessonPlannerPage.qml"), {
                    'site': root.site,
                })
                page.editRequested.connect(function(lesson) {
                    root.editLesson(lesson)
                })
                page.createRequested.connect(function(lesson) {
                    root.editLesson(lesson)
                })
                page.done.connect(function() {
                    stackView.pop()
                })
            }
        }
    }
    title: qsTr("Lectiones")

    Component.onCompleted: refresh()

    Connections {
        target: site
        onLessonsChanged: refresh()
    }

    ListView {
        id: listView
        anchors.fill: parent
        header: root.editable ? createLessonComponent : null
        delegate: editableLessonDelegate
        highlightRangeMode: ListView.ApplyRange
        preferredHighlightBegin: height / 4
        preferredHighlightEnd: height * 3 / 4
        highlightMoveDuration: 300
        ScrollBar.vertical: ScrollBar {}
    }

    Component {
        id: editableLessonDelegate
        LessonDelegate {
            width: listView.width
            editable: root.editable
            startTime: new Date(modelData.startTime)
            endTime: new Date(modelData.endTime)
            highlighted: ListView.isCurrentItem
            groups: modelData.groups
            students: modelData.students
            disabledPalette: inactivePalette
            onDeletionRequested: root.deleteLesson(modelData.lessonId)
            onEditRequested: root.editLesson(modelData)
            onOpenRequested: {
                stackView.push(Qt.resolvedUrl("LessonAttendancePage.qml"), {
                    'site': root.site,
                    'lessonData': modelData,
                    'lessonId': modelData.lessonId,
                })
            }
        }
    }

    Component {
        id: createLessonComponent
        Button {
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("Crea nove lectiones")
            onClicked: {
                var page = stackView.push(Qt.resolvedUrl("EditLessonPage.qml"), {
                    'site': root.site,
                    'editable': true,
                })
                page.done.connect(function() {
                    root.StackView.view.pop()
                })
            }
        }
    }

    Dialog {
        id: deleteConfirmationDialog
        property int lessonId: -1

        title: qsTr("Confirma le remotion")
        standardButtons: Dialog.Yes | Dialog.No
        modal: true
        anchors.centerIn: parent

        Label {
            text: qsTr("Esque tu es secur que tu vole deler le lection?")
            wrapMode: Text.Wrap
        }

        onAccepted: {
            site.deleteLesson(lessonId, function(reply) {
                if (reply.status != 200) {
                    // TODO: monstrar un error
                }
            })
        }
    }

    Connections {
        target: Qt.application
        onStateChanged: computeCurrentIndex()
    }

    SystemPalette { id: inactivePalette; colorGroup: SystemPalette.Disabled }

    function deleteLesson(lessonId) {
        deleteConfirmationDialog.lessonId = lessonId
        deleteConfirmationDialog.open()
    }

    function editLesson(lesson, extraProperties) {
        var properties = {
            'site': root.site,
            'lessonData': lesson,
            'editable': root.editable,
        }
        Object.assign(properties, extraProperties)
        var page = stackView.push(Qt.resolvedUrl("EditLessonPage.qml"),
                                  properties)
        page.copyRequested.connect(function() {
            root.StackView.view.pop()
            root.copyLesson(page.lessonData)
        })
        page.done.connect(function() {
            root.StackView.view.pop()
        })
    }

    function copyLesson(lesson) {
        var l = {}
        Object.assign(l, lesson)
        l.lessonId = -1
        return editLesson(l, {'generationMode': true})
    }

    function refresh() {
        var filter = {
            'userFields': 'name',
        }
        if (userId > 0) {
            var fieldName = role == Roles.Student ? "studentId" : "teacherId"
            filter[fieldName] = userId
        }

        if (pastDaysCount >= 0) {
            var since = DateUtils.today()
            since.setDate(since.getDate() - pastDaysCount)
            filter.since = Qt.formatDateTime(since, Qt.ISODate)
        }
        site.queryLessons(filter, function(reply) {
            if (reply.status == 200) {
                listView.model = reply.json.data
                console.log("Got lessons: " + JSON.stringify(reply.json.data))
                computeCurrentIndex()
            }
        })
    }

    function computeCurrentIndex() {
        var model = listView.model
        var previous = -1
        var current = -1
        var next = -1
        var now = new Date()
        for (var i = 0; i < model.length; i++) {
            var lesson = model[i]
            var startTime = new Date(lesson.startTime)
            var endTime = new Date(lesson.endTime)
            if (endTime < now) {
                previous = i
            } else if (startTime <= now && endTime >= now) {
                current = i
            } else if (startTime > now && next < 0) {
                next = i
            }
        }

        if (current >= 0) {
            listView.currentIndex = current
        } else if (previous >= 0) {
            listView.currentIndex = previous
        } else if (next >= 0) {
            listView.currentIndex = next
        } else {
            listView.currentIndex = -1
        }
    }
}
