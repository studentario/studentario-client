import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

RowLayout {
    id: root

    property var site: null
    property bool editable: true
    property int locationId: locationData ? locationData.locationId : -1
    property var locationData

    spacing: 8

    Label {
        visible: root.locationId > 0
        text: qsTr("<font color=\"%1\">\u{25A0}</font> %2")
            .arg(root.locationData.color).arg(root.locationData.name)
    }

    Label {
        visible: root.locationId <= 0
        text: qsTr("Non specificate")
        font.italic: true
        enabled: false
    }

    Button {
        text: "\u{1F589}"
        visible: root.editable
        onClicked: popup.open()
    }

    Dialog {
        id: popup
        anchors.centerIn: Overlay.overlay
        modal: true
        title: qsTr("Selige un location")
        onOpened: {
            var filter = {}
            site.queryLocations(filter, function(reply) {
                if (reply.status == 200) {
                    listView.model = reply.json.data
                }
            })
        }

        ListView {
            id: listView
            anchors.fill: parent
            implicitWidth: Math.max(300, emptyLabel.implicitWidth + 20)
            implicitHeight: Math.max(contentHeight, emptyLabel.implicitHeight + 20)
            clip: true
            delegate: LocationDelegate {
                width: listView.width
                name: modelData.name
                description: modelData.description
                color: modelData.color
                onClicked: {
                    root.locationData = modelData
                    popup.close()
                }
            }

            Label {
                id: emptyLabel
                anchors.fill: parent
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                visible: listView.count == 0
                text: qsTr("Nulle location es definite.")
                font.italic: true
                wrapMode: Text.Wrap
                textFormat: Text.StyledText
            }
        }
    }
}
