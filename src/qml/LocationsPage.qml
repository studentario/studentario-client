import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import it.mardy.studentario 1.0

Page {
    id: root

    property var site: null
    property bool editable: false

    header: TitleHeader {}
    title: qsTr("Locations")

    Component.onCompleted: refresh()

    ListView {
        id: listView
        anchors.fill: parent
        header: root.editable ? createLocationComponent : null
        delegate: LocationDelegate {
            width: listView.width
            editable: root.editable
            name: modelData.name
            description: modelData.description
            color: modelData.color
            onDeletionRequested: root.deleteLocation(modelData.locationId, modelData.name)
            onClicked: {
                var page = stackView.push(Qt.resolvedUrl("EditLocation.qml"), {
                    'site': root.site,
                    'locationId': modelData.locationId,
                    'editable': root.editable,
                    'name': modelData.name,
                    'description': modelData.description,
                })
                page.done.connect(function() {
                    root.refresh()
                    root.StackView.view.pop()
                })
            }
        }

        ScrollBar.vertical: ScrollBar {}
    }

    Component {
        id: createLocationComponent
        Button {
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("Crea un nove location")
            onClicked: {
                var page = stackView.push(Qt.resolvedUrl("EditLocation.qml"), {
                    'site': root.site,
                })
                page.done.connect(function() {
                    root.refresh()
                    root.StackView.view.pop()
                })
            }
        }
    }

    Dialog {
        id: deleteConfirmationDialog
        property string locationName: ""
        property int locationId: -1

        title: qsTr("Confirma le remotion")
        standardButtons: Dialog.Yes | Dialog.No
        modal: true
        anchors.centerIn: parent

        Label {
            text: qsTr("Esque tu es secur que tu vole deler le location «%1»?").
                arg(deleteConfirmationDialog.locationName)
            wrapMode: Text.Wrap
        }

        onAccepted: {
            site.deleteLocation(locationId, function(reply) {
                if (reply.status == 200) {
                    root.refresh()
                }
            })
        }
    }

    function deleteLocation(locationId, locationName) {
        deleteConfirmationDialog.locationName = locationName
        deleteConfirmationDialog.locationId = locationId
        deleteConfirmationDialog.open()
    }

    function refresh() {
        var filter = {}
        site.queryLocations(filter, function(reply) {
            if (reply.status == 200) {
                listView.model = reply.json.data
            }
        })
    }
}
