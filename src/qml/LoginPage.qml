import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import Qt.labs.settings 1.0

Page {
    id: root

    property var site: null
    property string userName: ""
    property string loginName: ""
    property string password: ""

    signal authenticated()

    states: [
        State {
            name: "passwordLogin"
            when: root.loginName.length == 0
            PropertyChanges { target: passwordLoginLayout; visible: true }
            PropertyChanges { target: pinLoginLayout; visible: false }
        },
        State {
            name: "pinLogin"
            when: root.loginName.length > 0
            PropertyChanges { target: passwordLoginLayout; visible: false }
            PropertyChanges { target: pinLoginLayout; visible: true }
            PropertyChanges {
                target: scrollableItem
                loginLayout: pinLoginLayout
                errorLabel: pinErrorLabel
            }
        }
    ]

    ScrollView {
        anchors.fill: parent
        contentWidth: width

        Item {
            id: scrollableItem
            anchors { left: parent.left; right: parent.right; top: parent.top; margins: 12 }
            implicitWidth: loginLayout.implicitWidth
            implicitHeight: loginLayout.implicitHeight
            property var loginLayout: passwordLoginLayout
            property var errorLabel: passwordErrorLabel

            ColumnLayout {
                id: pageLayout
                anchors.fill: parent
                enabled: !site.waiting

                Label {
                    Layout.fillWidth: true
                    Layout.topMargin: 16
                    Layout.bottomMargin: 16
                    wrapMode: Text.Wrap
                    text: site.displayName
                    horizontalAlignment: Qt.AlignHCenter
                    font.pointSize: 20
                }

                ColumnLayout {
                    id: passwordLoginLayout
                    Layout.alignment: Qt.AlignHCenter
                    Layout.minimumWidth: 200
                    Layout.maximumWidth: parent.width
                    visible: false
                    onVisibleChanged: if (visible) loginField.forceActiveFocus()

                    Label {
                        Layout.fillWidth: true
                        text: qsTr("Identificativo de accesso:")
                    }

                    TextField {
                        id: loginField
                        Layout.fillWidth: true
                        placeholderText: qsTr("Scribe tu \"login\"")
                    }

                    Label {
                        Layout.topMargin: 8
                        Layout.fillWidth: true
                        text: qsTr("Contrasigno:")
                    }

                    TextField {
                        id: passwordField
                        Layout.fillWidth: true
                        placeholderText: qsTr("Scribe tu contrasigno")
                        echoMode: TextInput.Password
                        onAccepted: root.doLogin()
                    }

                    ErrorLabel {
                        id: passwordErrorLabel
                        Layout.fillWidth: true
                    }

                    Button {
                        Layout.topMargin: 12
                        Layout.bottomMargin: 12
                        Layout.alignment: Qt.AlignHCenter
                        Layout.minimumWidth: parent.width / 2
                        text: qsTr("Entra")
                        onClicked: root.doLogin()
                    }
                }

                ColumnLayout {
                    id: pinLoginLayout
                    Layout.alignment: Qt.AlignHCenter
                    Layout.maximumWidth: parent.width
                    visible: false
                    onVisibleChanged: if (visible) pinField.forceActiveFocus(Qt.TabFocusReason)

                    Label {
                        Layout.fillWidth: true
                        Layout.topMargin: 16
                        Layout.bottomMargin: 16
                        wrapMode: Text.Wrap
                        text: qsTr("Benvenite, %1").arg(root.userName)
                        horizontalAlignment: Qt.AlignHCenter
                        font.pointSize: 22
                    }

                    HelperLabel {
                        Layout.topMargin: 4
                        wrapMode: Text.Wrap
                        text: qsTr("Scribe tu PIN pro entrar")
                    }

                    PinInput {
                        id: pinField
                        Layout.fillWidth: true
                        errorLabel: pinErrorLabel
                        onAccepted: root.doPinLogin()
                    }

                    ErrorLabel {
                        id: pinErrorLabel
                        Layout.fillWidth: true
                    }

                    Label {
                        Layout.topMargin: 12
                        Layout.fillWidth: true
                        visible: pinErrorLabel.visible
                        wrapMode: Text.Wrap
                        textFormat: Text.StyledText
                        text: qsTr("<a href=\"pw\">Clicca hic pro entrar con nomine de usator e contrasigno</a>")
                        onLinkActivated: root.state = "passwordLogin"
                    }
                }
            }

            BusyIndicator {
                anchors.centerIn: parent
                running: site.waiting
            }

            Settings {
                id: settings
                property alias userName: root.userName
                property alias loginName: root.loginName
            }
        }
    }

    Connections {
        target: root.site
        onAuthenticationFailed: {
            scrollableItem.errorLabel.error = error
            passwordField.clear()
            pinField.clear()
        }
        onAuthenticated: {
            root.password = passwordField.text
            root.authenticated()
            root.userName = site.authenticatedUser.name
            root.loginName = site.authenticatedUser.login
        }
    }

    function doLogin() {
        passwordErrorLabel.clear()
        site.login(loginField.text, passwordField.text)
    }

    function doPinLogin() {
        pinErrorLabel.clear()
        site.login(root.loginName, '', pinField.text)
    }
}
