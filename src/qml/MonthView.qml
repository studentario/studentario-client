import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import Qt.labs.calendar 1.0

ListView {
    id: root

    property bool selectMultiple: false
    property var minDate
    property var maxDate
    property var selectedDates: []
    property var selectedDate: new Date(NaN)

    property bool _blockPropagation: false

    implicitWidth: 200; implicitHeight: 200
    snapMode: ListView.SnapOneItem
    orientation: ListView.Horizontal
    highlightRangeMode: ListView.StrictlyEnforceRange
    highlightMoveDuration: 100

    model: CalendarModel {
        id: calendarModel
        from: root.yearStart(root.minDate)
        to: root.yearEnd(root.maxDate)
    }

    delegate: MonthGrid {
        id: monthGrid
        width: root.width
        height: root.height
        delegate: DayDelegate {
            enabled: root.dateIsEnabled(model.date)
            selected: root.dateIsSelected(model.date)
            inCurrentMonth: model.month == monthGrid.month
            text: model.day
        }

        month: model.month
        year: model.year
        onClicked: if (root.dateIsEnabled(date)) {
            date.setHours(0)
            date.setMinutes(0)
            date.setSeconds(0)
            date.setMilliseconds(0)
            root._blockPropagation = true
            var pos = root._findSelectedDate(date)
            var index = pos[0]
            if (index < 0) {
                var tmp
                if (root.selectMultiple) {
                    tmp = root.selectedDates
                    tmp.splice(pos[1], 0, date)
                } else {
                    tmp = []
                    tmp.push(date)
                }
                root.selectedDates = tmp
            } else if (root.selectMultiple) {
                var tmp = root.selectedDates
                tmp.splice(index, 1)
                root.selectedDates = tmp
            }
            root._blockPropagation = false
        }
    }

    ScrollIndicator.horizontal: ScrollIndicator { }

    onSelectedDatesChanged: {
        selectedDate = selectedDates.length > 0 ?
            selectedDates[0] : new Date(NaN)
    }
    onSelectedDateChanged: if (!_blockPropagation && !isNaN(selectedDate)) {
        _blockPropagation = true // evita recursion infinite
        selectedDates = [selectedDate]
        _blockPropagation = false
    }

    function dateIsSelected(date) {
        return _findSelectedDate(date)[0] >= 0
    }

    function dateIsEnabled(d) {
        if (!isNaN(minDate) && d.getTime() < minDate.getTime()) return false;
        if (!isNaN(maxDate) && d.getTime() > maxDate.getTime()) return false;
        return true
    }

    function dateCompare(d0, d1) {
        var yearDiff = d0.getFullYear() - d1.getFullYear()
        if (yearDiff != 0) return yearDiff
        var monthDiff = d0.getMonth() - d1.getMonth()
        if (monthDiff != 0) return monthDiff
        return d0.getDate() - d1.getDate()
    }

    function _findSelectedDate(date) {
        for (var i = 0; i < selectedDates.length; i++) {
            var d = selectedDates[i]
            var dateDiff = dateCompare(date, d)
            if (dateDiff == 0) {
                return [i, i]
            } else if (dateDiff < 0) {
                return [-1, i]
            }
        }
        return [-1, selectedDates.length]
    }

    function activateMonth(year, month) {
        if (month < 0) month = currentItem.month
        if (year < 0) year = currentItem.year
        positionViewAtIndex(model.indexOf(year, month), ListView.SnapPosition)
    }

    function yearStart(d) {
        return new Date(d.getFullYear(), 0, 1)
    }

    function yearEnd(d) {
        return new Date(d.getFullYear(), 11, 31, 23, 59, 59)
    }
}
