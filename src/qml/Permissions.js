function userIsAtLeast(user, role) {
    var u = user
    if (!u) return false
    if (role == Roles.Master) {
        return u.isMaster
    } else if (role == Roles.Director) {
        return u.isMaster || u.isDirector
    } else if (role == Roles.Admin) {
        return u.isMaster || u.isDirector || u.isAdmin
    } else if (role == Roles.Teacher) {
        return u.isMaster || u.isDirector || u.isAdmin || u.isTeacher
    } else if (role == Roles.Parent) {
        return u.isMaster || u.isDirector || u.isAdmin || u.isTeacher ||
            u.isParent
    }
    return false
}

function roleObject(roleId, targetUser, mainRole) {
    var r = Roles.info(roleId)
    return {
        'name': r.name,
        'active': mainRole == roleId || targetUser[r.field] || false,
        'enabled': mainRole != roleId,
        'field': r.field,
    }
}

/* Defini le rolos que actor pote assignar a targetUser quando on le modifica
 * in le pagina pro mainRole */
function grantableRoles(actor, targetUser, mainRole) {
    var roles = []
    if (userIsAtLeast(actor, Roles.Admin)) {
        roles.push(roleObject(Roles.Student, targetUser, mainRole))
        // TODO: adde hic le parente
    }

    if (userIsAtLeast(actor, Roles.Director)) {
        roles.push(roleObject(Roles.Teacher, targetUser, mainRole))
        roles.push(roleObject(Roles.Admin, targetUser, mainRole))
    }

    if (userIsAtLeast(actor, Roles.Master)) {
        roles.push(roleObject(Roles.Director, targetUser, mainRole))
    }

    return roles
}
