import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Page {
    id: root

    property var site: null
    property string password: ""
    property var userName: site.authenticatedUser.name

    signal done()

    states: [
        State {
            name: "pinCreated"
            PropertyChanges { target: pinInputLabel; visible: false }
            PropertyChanges { target: pinInputLayout; visible: false }
            PropertyChanges { target: pinCreatedLabel; visible: true }
            PropertyChanges { target: doneTimer; running: true }
        }
    ]

    onActiveFocusChanged: pinField.forceActiveFocus()

    ScrollView {
        anchors.fill: parent
        contentWidth: width

        Item {
            anchors { left: parent.left; right: parent.right; top: parent.top; margins: 12 }
            implicitWidth: loginLayout.implicitWidth
            implicitHeight: loginLayout.implicitHeight

            ColumnLayout {
                id: loginLayout
                anchors.fill: parent
                enabled: !site.waiting

                Label {
                    Layout.fillWidth: true
                    Layout.topMargin: 16
                    Layout.bottomMargin: 16
                    text: qsTr("Benvenite, %1").arg(root.userName)
                    horizontalAlignment: Qt.AlignHCenter
                    font.pointSize: 20
                }

                Label {
                    id: pinInputLabel
                    Layout.fillWidth: true
                    text: qsTr("Selige un PIN pro activar le entrata rapide")
                    wrapMode: Text.Wrap
                }

                ColumnLayout {
                    id: pinInputLayout
                    Layout.fillWidth: false
                    Layout.alignment: Qt.AlignHCenter

                    HelperLabel {
                        Layout.topMargin: 4
                        text: qsTr("Scribe tu nove PIN")
                    }

                    PinInput {
                        id: pinField
                        Layout.fillWidth: true
                        errorLabel: errorLabel
                        onAccepted: pinVerificationField.forceActiveFocus()
                    }

                    HelperLabel {
                        Layout.topMargin: 4
                        text: qsTr("Repete le PIN")
                    }

                    PinInput {
                        id: pinVerificationField
                        Layout.fillWidth: true
                        errorLabel: errorLabel
                        onAccepted: root.createPin()
                    }
                }

                ErrorLabel {
                    id: errorLabel
                }

                Label {
                    id: pinCreatedLabel
                    visible: false
                    text: qsTr("Le nove PIN ha essite authorisate")
                    wrapMode: Text.Wrap
                }
            }

            BusyIndicator {
                anchors.centerIn: parent
                running: site.waiting
            }
        }
    }

    Timer {
        id: doneTimer
        interval: 2000
        onTriggered: root.done()
    }

    function createPin() {
        errorLabel.clear()

        var pin = pinField.validatedPin()
        if (!pin) return

        var pin2 = pinVerificationField.validatedPin()
        if (!pin2) return

        if (pin != pin2) {
            errorLabel.message = qsTr("Le PINs non es equal!")
            return
        }

        site.updatePin(root.password, pin, function(reply) {
            if (reply.status == 200) {
                root.state = "pinCreated"
            } else {
                errorLabel.error = reply.json
            }
        })
    }
}
