import QtQuick 2.12
import QtQuick.Controls 2.12

Flickable {
    id: root

    property int rowHeight: 64
    property int timeWidth: textMetrics.width + 10
    property int columnWidth: Math.max(120, (width - timeWidth) / Math.max(numColumns, 1))
    property int numColumns: 0
    property alias timeFont: textMetrics.font
    property var activities: []
    property Component delegate

    signal createLessonRequested(int column, double startRatio)

    contentHeight: contents.height
    contentWidth: contents.width
    pixelAligned: true

    Canvas {
        id: contents

        width: root.timeWidth + root.columnWidth * root.numColumns
        height: root.rowHeight * 24
        onPaint: {
            var ctx = getContext("2d")
            ctx.font = '%1pt "%2"'.arg(timeFont.pointSize).arg(timeFont.family)
            ctx.textBaseline = "hanging"
            ctx.textAlign = "center"
            var evenBackground = Qt.rgba(0.8, 0.8, 1.0, 1)
            var oddBackground = Qt.rgba(0.9, 0.9, 1.0, 1)
            var black = Qt.rgba(0, 0, 0, 1)

            var time = new Date()

            for (var i = 0; i < 24; i++) {
                var x = 0
                var y = i * root.rowHeight + 0.5
                ctx.fillStyle = i % 2 ? evenBackground : oddBackground
                ctx.fillRect(x, y, x + width, y + root.rowHeight)

                ctx.lineWidth = 1
                ctx.strokeStyle = black

                time.setHours(i, 0, 0, 0)
                ctx.strokeText(Qt.formatTime(time), x + timeWidth / 2, y)

                ctx.beginPath()
                ctx.lineWidth = 1
                ctx.moveTo(x, y)
                ctx.lineTo(width, y)
                ctx.stroke()

                var half = y + root.rowHeight / 2
                ctx.beginPath()
                ctx.lineWidth = 0.2
                ctx.moveTo(x, half)
                ctx.lineTo(width, half)
                ctx.stroke()
            }

            ctx.beginPath()
            ctx.lineWidth = 1
            for (var i = 0; i < root.numColumns; i++) {
                var x = root.timeWidth + i * root.columnWidth + 0.5
                var y = 0
                ctx.moveTo(x, y)
                ctx.lineTo(x, height)
            }
            ctx.stroke()
        }

        Repeater {
            model: root.activities
            Loader {
                property var activity: modelData
                x: root.timeWidth + root.columnWidth * modelData.column
                y: contents.height * modelData.startRatio
                width: root.columnWidth + 1
                height: contents.height * (modelData.endRatio - modelData.startRatio)
                sourceComponent: root.delegate

                Behavior on x { NumberAnimation { duration: 300; easing.type: Easing.InOutQuad }}
                Behavior on width { NumberAnimation { duration: 300; easing.type: Easing.InOutQuad }}
            }
        }

        function columnFromPixel(x) {
            return (x - root.timeWidth) / root.columnWidth
        }

        function ratioFromPixel(y) {
            return y / contents.height
        }
    }

    MouseArea {
        anchors.fill: parent
        z: -1
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onClicked: {
            if (mouse.button === Qt.RightButton)
            root.createLessonRequested(contents.columnFromPixel(mouse.x),
                                       contents.ratioFromPixel(mouse.y))
        }
        onPressAndHold: {
            if (mouse.source === Qt.MouseEventNotSynthesized)
                root.createLessonRequested(contents.columnFromPixel(mouse.x),
                                           contents.ratioFromPixel(mouse.y))
        }
    }

    TextMetrics {
        id: textMetrics
        font.pointSize: 8
        text: Qt.formatTime(new Date())
    }
}
