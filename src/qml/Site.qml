import Qt.labs.settings 1.0
import QtQml 2.12
import it.mardy.studentario 1.0
import "Permissions.js" as Permissions

QtObject {
    id: root

    readonly property string displayName: settings.displayName
    readonly property url baseUrl: settings.baseUrl
    property string authenticationToken: ""
    property var authenticatedUser: null
    property var userCache: UserCache { site: root }
    property bool waiting: false
    property bool canEditStudents: isAtLeast(Roles.Admin)
    property bool canEditTeachers: isAtLeast(Roles.Director)
    property bool canEditAdmins: isAtLeast(Roles.Director)
    property bool canEditDirectors: isAtLeast(Roles.Master)
    property bool canEditGroups: canEditStudents
    property bool canEditLessons: canEditStudents
    property bool canEditLocations: isAtLeast(Roles.Director)
    property bool canEditTags: isAtLeast(Roles.Director)

    signal authenticated()
    signal authenticationFailed(var error)

    signal lessonsChanged()

    property var configFile: FileIO {
        filePath: root.configFilePath()
    }

    property var settings: JSON.parse(configFile.contents)

    property var tags: TagCache {
        site: root
    }

    function isAtLeast(role) {
        return Permissions.userIsAtLeast(root.authenticatedUser, role)
    }

    function roleCanLogin(role) {
        // In futuro, isto essera configurabile in le sito
        return role == Roles.Master ||
            role == Roles.Director ||
            role == Roles.Admin ||
            role == Roles.Teacher
    }

    function availableRolesForUser(user, mainRole) {
        return Permissions.grantableRoles(root.authenticatedUser,
                                          user, mainRole)
    }

    function configFilePath() {
        var args = Qt.application.arguments
        return args.length > 1 ? args[1] : embeddedConfigFile
    }

    function request(verb, path, params, callback) {
        var http = new XMLHttpRequest()

        http.open(verb, root.baseUrl + path, true)
        http.setRequestHeader('Content-type','application/json; charset=utf-8')
        if (root.authenticationToken) {
            http.setRequestHeader('Authorization',
                                  'Basic ' + root.authenticationToken)
        }
        http.onreadystatechange = function() {
            if (http.readyState === XMLHttpRequest.HEADERS_RECEIVED) {
                console.log("Response Headers -->")
                console.log(http.getAllResponseHeaders())
            } else if (http.readyState === XMLHttpRequest.DONE) {
                root.waiting = false

                var reply = {
                    'status': http.status,
                }
                var json = {}
                try {
                    json = JSON.parse(http.responseText)
                } catch (e) {
                    console.log('Failed to parse reply: ' + http.responseText)
                    json = {
                        'status': 'error',
                        'code': 1000, // nulle internet
                    }
                }
                reply['json'] = json
                callback(reply)
            }
        }
        root.waiting = true
        http.send(JSON.stringify(params))
    }

    function dictToQuery(params) {
        var query = ''
        for (var name in params) {
            var entry = name + '=' + encodeURIComponent(params[name])
            if (query != '') {
                query += '&'
            }
            query += entry
        }
        return query
    }

    function post(path, params, callback) {
        request('POST', path, params, callback)
    }

    function put(path, params, callback) {
        request('PUT', path, params, callback)
    }

    function get(path, query, callback) {
        var fullPath = path
        if (query) {
            fullPath += "?" + query
        }
        request('GET', fullPath, {}, callback)
    }

    function remove(path, callback) {
        request('DELETE', path, {}, callback)
    }

    function login(userName, password, pin) {
        var params = {
            'login': userName,
        }
        if (password) params['password'] = password
        if (pin) params['pinCode'] = pin

        var callback = function(reply) {
            console.log("Login reply: " + JSON.stringify(reply))
            if (reply.status == 200) {
                var data = reply.json.data
                root.authenticationToken = data.authenticationToken
                root.authenticatedUser = data.user
                root.userCache.refresh()
                root.authenticated()
            } else {
                root.authenticationFailed(reply.json)
            }
        }
        post("/api/v1/login", params, callback)
    }

    function updatePin(password, newPin, callback) {
        var params = {
            'password': password,
            'newPin': newPin,
        }
        post("/api/v1/users/me/updatePin", params, callback)
    }

    function userCacheCb(callback) {
        return function(reply) {
            callback(reply)
            userCache.refresh()
        }
    }

    function queryUsers(filter, callback) {
        var q = dictToQuery(filter)
        get("/api/v1/users", q, callback)
    }

    function loadUser(userId, callback) {
        get("/api/v1/users/" + userId, "", callback)
    }

    function createUser(user, callback) {
        post("/api/v1/users", user, userCacheCb(callback))
    }

    function updateUser(userId, user, callback) {
        put("/api/v1/users/" + userId, user, userCacheCb(callback))
    }

    function deleteUser(userId, callback) {
        remove("/api/v1/users/" + userId, callback)
    }

    function loadUserStatusChanges(userId, callback) {
        get("/api/v1/statuschanges", "userId=" + userId, callback)
    }

    function addUserStatusChange(userId, change, callback) {
        post("/api/v1/users/" + userId + "/status", change, callback)
    }

    function deleteUserStatusChange(changeId, callback) {
        remove("/api/v1/statuschanges/" + changeId, callback)
    }

    /* Gruppos */

    function queryGroups(filter, callback) {
        var q = dictToQuery(filter)
        get("/api/v1/groups", q, callback)
    }

    function loadGroup(groupId, callback) {
        get("/api/v1/groups/" + groupId, "", callback)
    }

    function createGroup(group, callback) {
        post("/api/v1/groups", group, callback)
    }

    function updateGroup(groupId, group, callback) {
        put("/api/v1/groups/" + groupId, group, callback)
    }

    function deleteGroup(groupId, callback) {
        remove("/api/v1/groups/" + groupId, callback)
    }

    /* Gruppos <-> membros */

    function loadGroupMembers(groupId, roleId, callback) {
        var role = Roles.info(roleId)
        get("/api/v1/groups/" + groupId + '/' + role.api, "", callback)
    }

    function addGroupMembers(groupId, roleId, userIds, callback) {
        var role = Roles.info(roleId)
        var req = { "userIds": userIds }
        post("/api/v1/groups/" + groupId + '/' + role.api, req, callback)
    }

    function removeGroupMembers(groupId, roleId, userIds, callback) {
        var role = Roles.info(roleId)
        remove("/api/v1/groups/" + groupId + '/' + role.api + '/' + userIds,
               callback)
    }

    /* Lectiones */

    function queryLessons(filter, callback) {
        var q = dictToQuery(filter)
        get("/api/v1/lessons", q, callback)
    }

    function createLesson(lesson, callback) {
        post("/api/v1/lessons", lesson, lessonsChangedCb(callback))
    }

    function updateLesson(lessonId, lesson, callback) {
        put("/api/v1/lessons/" + lessonId, lesson, lessonsChangedCb(callback))
    }

    function deleteLesson(lessonId, callback) {
        remove("/api/v1/lessons/" + lessonId, lessonsChangedCb(callback))
    }

    function lessonsChangedCb(callback) {
        return function(reply) {
            if (reply.status == 200) lessonsChanged()
            callback(reply)
        }
    }

    /* Lectiones <-> membros */

    function inviteToLesson(lessonId, invitees, callback) {
        post("/api/v1/lessons/" + lessonId + '/invitees', invitees, callback)
    }

    function uninviteFromLesson(lessonId, req, callback) {
        var q = dictToQuery(req)
        remove("/api/v1/lessons/" + lessonId + '/invitees?' + q, callback)
    }

    function queryLessonAttendance(lessonId, filter, callback) {
        var q = dictToQuery(filter)
        get("/api/v1/lessons/" + lessonId + '/attendance', q, callback)
    }

    function updateLessonAttendance(lessonId, participants, callback) {
        put("/api/v1/lessons/" + lessonId + '/attendance', participants,
            callback)
    }

    /* Locationes */

    function queryLocations(filter, callback) {
        var q = dictToQuery(filter)
        get("/api/v1/locations", q, callback)
    }

    function loadLocation(locationId, callback) {
        get("/api/v1/locations/" + locationId, "", callback)
    }

    function createLocation(location, callback) {
        post("/api/v1/locations", location, callback)
    }

    function updateLocation(locationId, location, callback) {
        put("/api/v1/locations/" + locationId, location, callback)
    }

    function deleteLocation(locationId, callback) {
        remove("/api/v1/locations/" + locationId, callback)
    }

    /* Etiquettas */

    function queryTags(filter, callback) {
        var q = dictToQuery(filter)
        get("/api/v1/tags", q, callback)
    }

    function loadTag(tagId, callback) {
        get("/api/v1/tags/" + tagId, "", callback)
    }

    function createTag(tag, callback) {
        post("/api/v1/tags", tag, callback)
    }

    function updateTag(tagId, tag, callback) {
        put("/api/v1/tags/" + tagId, tag, callback)
    }

    function deleteTag(tagId, callback) {
        remove("/api/v1/tags/" + tagId, callback)
    }

    /* Assignation de etiquettas */

    function assignTags(tagIds, targets, callback) {
        post("/api/v1/tags/" + tagIds + '/entities', targets, callback)
    }

    function unassignTags(tagIds, targets, callback) {
        var q = dictToQuery(targets)
        remove("/api/v1/tags/" + tagIds + '/entities?' + q, callback)
    }

}
