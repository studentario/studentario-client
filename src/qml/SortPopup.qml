import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Popup {
    id: root

    property string sortOrder: "name"
    margins: 1
    y: parent.height

    property var model: [
        { "label": qsTr("Ordine alphabetic"), "order": "name", },
        { "label": qsTr("Ordine alphabetic inverse"), "order": "-name", },
        { "label": qsTr("Prima le plus nove"), "order": "-id", },
        { "label": qsTr("Prima le plus vetule"), "order": "id" },
    ]

    ColumnLayout {
        Repeater {
            model: sortPopup.model
            Button {
                Layout.fillWidth: true
                text: modelData.label
                onClicked: root.sort(modelData.order)
                flat: true
            }
        }
    }

    function sort(order) {
        root.sortOrder = order
        root.close()
    }
}
