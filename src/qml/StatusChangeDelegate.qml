import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

SwipeDelegate {
    id: root

    property var statusChange: ({})

    signal deletionRequested()

    property string _statusText: ""

    swipe.right: Button {
        anchors.right: parent.right
        height: parent.height
        flat: true
        icon {
            source: "qrc:/icons/delete"
            color: "transparent"
        }
        onClicked: root.deletionRequested()
    }
    text: qsTr("%1: %2").arg(Qt.formatDate(statusChange.time)).arg(_statusText)

    onStatusChangeChanged: {
        var s = root.statusChange.status
        console.log("Status change: " + JSON.stringify(root.statusChange))
        console.log("Status change string: " + s)
        var text
        switch (s) {
        case "active":
            text = qsTr("\u{2705} Usator activate")
            root.background.color = "#cfc"
            break
        case "inactive":
            text = qsTr("\u{274C} Usator deactivate")
            root.background.color = "#fcc"
            break
        default:
            text = qsTr("Evento non recognoscite (%1)").arg(s)
        }
        root._statusText = text
    }
}
