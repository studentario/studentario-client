import QtQuick 2.12
import QtQuick.Controls 2.12

Item {
    id: root

    property var palette: null
    default property alias data: contentItem.data

    signal resized(int newWidth)

    implicitWidth: contentItem.implicitWidth
    implicitHeight: contentItem.implicitHeight

    Item {
        id: contentItem
        anchors { fill: parent; rightMargin: 8 }
        implicitWidth: children[0].implicitWidth
        implicitHeight: children[0].implicitHeight
    }

    MouseArea {
        id: dragArea
        x: contentItem.width
        width: 8
        height: parent.height
        drag {
            target: handle
            axis: Drag.XAxis
            minimumX: -x + 3
        }
        onPressed: handle.anchors.right = undefined
    }

    Canvas {
        id: handle
        anchors { right: dragArea.right }
        width: 8
        height: parent.height
        onPaint: {
            var ctx = getContext("2d")
            ctx.strokeStyle = palette.mid
            ctx.linewidth = 1
            ctx.beginPath()
            var x1 = Math.floor(width / 3)
            var x2 = Math.floor(width * 2 / 3)
            ctx.moveTo(x1, 1)
            ctx.lineTo(x1, height - 1)
            ctx.moveTo(x2, 1)
            ctx.lineTo(x2, height - 1)
            ctx.closePath()
            ctx.stroke()
        }

        onXChanged: if (dragArea.drag.active) root.resized(x + width)
    }
}
