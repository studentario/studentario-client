import QtQml 2.12

QtObject {
    id: root

    property int levelTagId: -1
    property var site: null

    property var connection1: Connections {
        target: root.site
        onAuthenticated: {
            loadDefaultTag("levelTagId", "LevelTags")
        }
    }

    function loadDefaultTag(prop, name) {
        var filters = {
            "name": name,
            "parentTagId": -1,
        }
        site.queryTags(filters, function(reply) {
            if (reply.status == 200) {
                var tags = reply.json.data
                for (var i = 0; i < tags.length; i++) {
                    var tag = tags[i]
                    if (tag.name == name) {
                        root[prop] = tag.tagId
                        break
                    }
                }
                if (i >= tags.length) { // non trovate
                    createDefaultTag(prop, name)
                }
            } else if (reply.status == 404) {
                createDefaultTag(prop, name)
            }
        })
    }

    function createDefaultTag(prop, name) {
        if (!site.canEditTags) {
            console.warn("Le etiquetta " + name + " non existe!")
            return
        }
        site.createTag({
            "name": name,
            "parentTagId": -1,
        }, function(reply) {
            if (reply.status == 200) {
                var tag = reply.json.data
                console.log("Etiquetta create: " + JSON.stringify(tag))
                root[prop] = tag.tagId
            }
        })
    }
}
