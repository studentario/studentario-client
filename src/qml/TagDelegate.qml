import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

SwipeDelegate {
    id: root

    property bool editable: false
    property string name
    property string description
    property color color
    property int childrenCount: 0
    text: childrenCount > 0 ?
        qsTr("<font color=\"%3\">\u{1F5C0}</font> <b>%1</b> - %2")
            .arg(name).arg(description).arg(color) :
        qsTr("<font color=\"%3\">\u{25A0}</font> <b>%1</b> - %2")
            .arg(name).arg(description).arg(color)

    signal additionRequested()
    signal deletionRequested()
    signal editRequested()

    swipe.enabled: root.editable
    swipe.left: Button {
        anchors.left: parent.left
        height: parent.height
        enabled: root.editable
        flat: true
        text: '+'
        font.pointSize: 14
        onClicked: root.additionRequested()
    }
    swipe.right: Row {
        anchors.right: parent.right
        height: parent.height
        Button {
            height: parent.height
            visible: root.editable && root.childrenCount > 0
            flat: true
            text: "\u{1f589}" // Pencil
            onClicked: root.editRequested()
        }
        Button {
            height: parent.height
            enabled: root.editable
            flat: true
            icon {
                source: "qrc:/icons/delete"
                color: "transparent"
            }
            onClicked: root.deletionRequested()
        }
    }
}
