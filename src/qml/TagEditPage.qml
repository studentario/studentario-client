import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Page {
    id: root

    property var site: null
    property int parentTagId: tagData ? tagData.parentTagId : -1
    property int tagId: tagData && tagData.tagId ? tagData.tagId : -1
    property var tagData: null

    property var tagType

    signal done()

    property bool _editing: tagId >= 0

    header: TitleHeader {}
    title: _editing ? tagType.titleEditing : tagType.titleNew

    Component.onCompleted: {
        if (tagId > 0 && !tagData) {
            site.loadTag(tagId, function(reply) {
                if (reply.status == 200) {
                    root.tagData = reply.json.data
                }
            })
        }
        nameField.forceActiveFocus(Qt.TabFocusReason)
    }

    ScrollView {
        anchors.fill: parent
        contentWidth: width

        ColumnLayout {
            anchors {
                fill: parent
                leftMargin: 12; rightMargin: 12
            }

            HelperLabel {
                Layout.fillWidth: true
                text: root.tagType.nameLabel
            }
            TextField {
                id: nameField
                Layout.fillWidth: true
                Layout.bottomMargin: 10
                text: root.tagData ? root.tagData.name : ""
            }

            HelperLabel {
                Layout.fillWidth: true
                text: qsTr("Description")
            }
            TextArea {
                id: descriptionField
                Layout.fillWidth: true
                Layout.bottomMargin: 10
                KeyNavigation.priority: KeyNavigation.BeforeItem
                KeyNavigation.tab: saveButton
                placeholderText: root.tagType.descriptionPlaceholder
                wrapMode: TextEdit.Wrap
                text: root.tagData ? root.tagData.description : ""
            }

            RowLayout {
                Layout.fillWidth: true
                Layout.bottomMargin: 10

                Label {
                    text: qsTr("Color:")
                }

                ColorPicker {
                    id: colorPicker
                    selectedColor: root.tagData ? root.tagData.color : undefined
                }
            }

            ErrorLabel {
                id: errorLabel
                Layout.fillWidth: true
            }

            Button {
                id: saveButton
                Layout.topMargin: 12
                Layout.bottomMargin: 12
                Layout.alignment: Qt.AlignHCenter
                Layout.minimumWidth: parent.width / 2
                text: qsTr("Salva")
                onClicked: root.save()
            }
        }
    }

    function save() {
        errorLabel.clear()
        if (nameField.text.length < 2) {
            errorLabel.message = qsTr("Le nomine debe haber al minus 2 litteras")
            return
        }

        var tag = {
            'parentTagId': root.parentTagId,
            'name': nameField.text,
            'description': descriptionField.text,
            'color': colorPicker.selectedColor.toString(),
        }

        var callback = function(reply) {
            root.tagData = tag
            if (reply.status == 200) {
                root.done()
            } else {
                errorLabel.error = reply.json
            }
        }

        if (!root._editing) {
            site.createTag(tag, callback)
        } else {
            site.updateTag(root.tagId, tag, callback)
        }
    }
}
