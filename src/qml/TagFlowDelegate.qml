import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Rectangle {
    id: root

    property color tagColor
    property string name: ""
    property bool editable: true
    property int maxWidth: 300

    signal removeRequested()

    width: contents.width + 4 + radius
    height: contents.height + 4
    color: Qt.rgba(tagColor.r, tagColor.g, tagColor.b, 0.1)
    radius: height / 2
    border { color: root.tagColor; width: 2 }

    RowLayout {
        id: contents
        anchors.centerIn: parent
        spacing: 4
        Label {
            id: tagLabel
            width: Math.min(root.maxWidth, implicitWidth)
            verticalAlignment: Text.AlignVCenter
            text: root.name
            elide: Text.ElideRight
        }
        Label {
            verticalAlignment: Text.AlignVCenter
            color: "red"
            text: "\u{2BBE}"
            visible: root.editable
        }
    }

    MouseArea {
        anchors.fill: parent
        enabled: root.editable
        onClicked: root.removeRequested()
    }
}
