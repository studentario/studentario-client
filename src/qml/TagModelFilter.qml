import QtQml 2.12

JsonModelFilter {
    id: root
    property alias allTags: root.allItems

    filterItemCb: function(tag, pattern) {
        var words = tag.name.split(' ').concat(tag.description.split(' '))
        return anyStringStartsWith(words, pattern)
    }
}
