import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import it.mardy.studentario 1.0
import "TagType.js" as TagType
import "Tag.js" as Tag

ColumnLayout {
    id: root

    property var site: null
    property alias title: titleLabel.text
    property bool editable: true
    property var allTagsData: []
    property var tagType

    property int parentTagId: TagType.parentTagId(site, tagType)
    property alias currentTags: changeRecorder.currentItems
    property alias addedTags: changeRecorder.addedItemIds
    property alias removedTags: changeRecorder.removedItemIds

    RowLayout {
        Layout.fillWidth: true

        Label {
            id: titleLabel
            Layout.fillWidth: true
            font.pointSize: 12
            text: root.tagType.titleList
        }

        Button {
            text: '+'
            font.pointSize: 14
            flat: true
            visible: root.editable
            onClicked: root.openPicker()
        }
    }

    Frame {
        Layout.fillWidth: true

        Item {
            anchors.fill: parent
            implicitWidth: visibleChildren.length > 0 ?
                visibleChildren[0].implicitWidth : children[0].implicitWidth
            implicitHeight: visibleChildren.length > 0 ?
                visibleChildren[0].implicitHeight : children[0].implicitHeight

            Flow {
                id: tagView
                anchors.fill: parent
                visible: tagsRepeater.count > 0
                spacing: 8

                Repeater {
                    id: tagsRepeater
                    model: root.currentTags

                    TagFlowDelegate {
                        maxWidth : tagView.width - 16
                        editable: root.editable
                        name: modelData.name
                        tagColor: modelData.color
                        onRemoveRequested: changeRecorder.removeItem(modelData.tagId)
                    }
                }
            }

            Column {
                anchors.fill: parent
                visible: tagsRepeater.count == 0
                padding: 24
                spacing: 2

                Label {
                    anchors { left: parent.left; right: parent.right }
                    font.italic: true
                    horizontalAlignment: Qt.AlignHCenter
                    wrapMode: Text.Wrap
                    text: root.tagType.noTagLabel
                }

                Label {
                    anchors { left: parent.left; right: parent.right }
                    visible: root.editable
                    font.italic: true
                    horizontalAlignment: Qt.AlignHCenter
                    wrapMode: Text.Wrap
                    textFormat: Text.StyledText
                    text: qsTr("Pressar sur le symbolo <b>\"+\"</b> pro adder un.")
                }
            }
        }
    }

    CollectionChangeRecorder {
        id: changeRecorder
        idField: "tagId"
        initialItems: filterTags(allTagsData)
    }

    SystemPalette { id: inactivePalette; colorGroup: SystemPalette.Disabled }

    function filterTags(allTags) {
        var neededParentTagId = root.parentTagId
        var tmp = []
        for (var i = 0; i < allTags.length; i++) {
            var tag = allTags[i]
            if (tag.parentTagId == neededParentTagId) {
                tmp.push(tag)
            }
        }
        return tmp
    }

    function openPicker() {
        var page = stackView.push(Qt.resolvedUrl("FindTags.qml"), {
            'site': root.site,
            'tagType': root.tagType,
            'parentTagId': root.parentTagId,
            'excludedTagIds': Tag.ids(root.currentTags),
        })
        page.done.connect(function() {
            for (var i = 0; i < page.selectedTags.length; i++) {
                changeRecorder.addItem(page.selectedTags[i])
            }
            page.StackView.view.pop()
        })
    }
}
