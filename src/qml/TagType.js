var tagTypes = [
    {
        typeId: "tags",
        parentTagName: "",
        titleEditing: qsTr("Modifica etiquetta"),
        titleList: qsTr("Etiquettas"),
        titleNew: qsTr("Nove etiquetta"),
        titleFind: qsTr("Recerca etiquettas"),
        nameLabel: qsTr("Nomine del etiquetta"),
        descriptionPlaceholder: qsTr("Informationes libere super le etiquetta"),
        newTagLabel: qsTr("Crea un nove etiquetta"),
        deleteConfirmationText: qsTr("Esque tu es secur que tu vole deler le etiquetta «%1»?"),
        noTagLabel: qsTr("Nulle etiquetta assignate"),
    },
    {
        typeId: "level",
        parentTagName: "levelTagId",
        titleEditing: qsTr("Modifica nivello"),
        titleList: qsTr("Nivellos"),
        titleNew: qsTr("Nove nivello"),
        titleFind: qsTr("Recerca nivellos"),
        nameLabel: qsTr("Nomine del nivello"),
        descriptionPlaceholder: qsTr("Informationes libere super le nivello"),
        newTagLabel: qsTr("Crea un nove nivello"),
        deleteConfirmationText: qsTr("Esque tu es secur que tu vole deler le nivello «%1»?"),
        noTagLabel: qsTr("Nulle nivello assignate"),
    },
]

function byTypeId(typeId) {
    for (var i = 0; i < tagTypes.length; i++) {
        if (tagTypes[i].typeId == typeId) return tagTypes[i]
    }
    return tagTypes[0]
}

function parentTagId(site, tagType) {
    return tagType.parentTagName ? site.tags[tagType.parentTagName] : -1
}
