import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Page {
    id: root

    property var site: null
    property int parentTagId: -1
    property bool editable: false

    property var tagType

    header: TitleHeader {}
    title: tagType.titleList

    Component.onCompleted: refresh()

    ListView {
        id: listView
        anchors.fill: parent
        header: root.editable ? createTagComponent : null
        delegate: TagDelegate {
            width: listView.width
            editable: root.editable
            name: modelData.name
            description: modelData.description
            color: modelData.color
            childrenCount: modelData.children.length
            onAdditionRequested: root.addTag(modelData.tagId)
            onDeletionRequested: root.deleteTag(modelData.tagId, modelData.name)
            onEditRequested: root.editTag(modelData)
            onClicked: {
                if (childrenCount > 0) {
                    stackView.push(Qt.resolvedUrl("TagsPage.qml"), {
                        'site': root.site,
                        'editable': root.editable,
                        'tagType': root.tagType,
                        'parentTagId': modelData.tagId,
                    })
                } else {
                    root.editTag(modelData)
                }
            }
        }

        ScrollBar.vertical: ScrollBar {}
    }

    Component {
        id: createTagComponent
        Button {
            anchors.horizontalCenter: parent.horizontalCenter
            text: root.tagType.newTagLabel
            onClicked: root.addTag(root.parentTagId)
        }
    }

    Dialog {
        id: deleteConfirmationDialog
        property string tagName: ""
        property int tagId: -1

        title: qsTr("Confirma le remotion")
        standardButtons: Dialog.Yes | Dialog.No
        modal: true
        anchors.centerIn: parent

        Label {
            text: root.tagType.deleteConfirmationText.
                arg(deleteConfirmationDialog.tagName)
            wrapMode: Text.Wrap
        }

        onAccepted: {
            site.deleteTag(tagId, function(reply) {
                if (reply.status == 200) {
                    root.refresh()
                }
            })
        }
    }

    function addTag(parentTagId) {
        var page = stackView.push(Qt.resolvedUrl("TagEditPage.qml"), {
            'site': root.site,
            'tagType': root.tagType,
            'parentTagId': parentTagId,
        })
        page.done.connect(function() {
            root.refresh()
            root.StackView.view.pop()
        })
    }

    function editTag(tagData) {
        var page = stackView.push(Qt.resolvedUrl("TagEditPage.qml"), {
            'site': root.site,
            'tagType': root.tagType,
            'tagData': tagData,
        })
        page.done.connect(function() {
            root.refresh()
            root.StackView.view.pop()
        })
    }

    function deleteTag(tagId, tagName) {
        deleteConfirmationDialog.tagName = tagName
        deleteConfirmationDialog.tagId = tagId
        deleteConfirmationDialog.open()
    }

    function refresh() {
        console.log("Parent tag id: " + root.parentTagId)
        var filter = {
            'parentTagId': root.parentTagId,
            'fields': 'children',
        }
        site.queryTags(filter, function(reply) {
            if (reply.status == 200) {
                listView.model = reply.json.data
            }
        })
    }
}
