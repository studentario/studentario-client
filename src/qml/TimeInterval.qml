import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

RowLayout {
    id: root

    property alias startMinutes: time0.minutes
    property alias endMinutes: time1.minutes

    Label {
        text: qsTr("Horario initial:")
        wrapMode: Text.Wrap
    }

    TimeSelector {
        id: time0
        Layout.rightMargin: 24
    }

    Label {
        text: qsTr("Horario final:")
        wrapMode: Text.Wrap
    }

    TimeSelector {
        id: time1
    }
}
