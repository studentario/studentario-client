import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Button {
    id: root

    property int minutes: 60 * 12

    text: "%1:%2".arg(Math.floor(minutes / 60).toString().padStart(2, '0'))
                 .arg((minutes % 60).toString().padStart(2, '0'))
    onClicked: popup.open()

    Popup {
        id: popup

        Row {
            id: row

            Tumbler {
                id: hoursTumbler
                model: 24
                visibleItemCount: 7
                delegate: delegateComponent
            }

            Tumbler {
                id: minutesTumbler
                model: [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55]
                visibleItemCount: hoursTumbler.visibleItemCount
                delegate: delegateComponent
            }
        }

        onOpened: {
            hoursTumbler.positionViewAtIndex(root.minutes / 60, Tumbler.Center)
            minutesTumbler.positionViewAtIndex(Math.floor((root.minutes % 60) / 5), Tumbler.Center)
        }
        onClosed: root.minutes = hoursTumbler.currentIndex * 60 + minutesTumbler.currentIndex * 5
    }

    FontMetrics {
        id: fontMetrics
    }

    Component {
        id: delegateComponent

        Label {
            text: formatText(Tumbler.tumbler.count, modelData)
            opacity: 1.0 - Math.abs(Tumbler.displacement) / (Tumbler.tumbler.visibleItemCount / 2)
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter

            function formatText(count, data) {
                return data.toString().padStart(2, '0')
            }
        }
    }
}
