import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

ToolBar {
    id: root

    property bool canGoBack: true
    property var stackView: ApplicationWindow.contentItem ?
        ApplicationWindow.contentItem.children[0] : null
    default property alias rightSideElements: rightSide.data

    RowLayout {
        anchors.fill: parent

        BackButton {
            visible: root.canGoBack
            onClicked: root.stackView.pop()
        }

        Label {
            Layout.fillWidth: true
            fontSizeMode: Text.VerticalFit
            horizontalAlignment: Text.AlignHCenter
            text: root.stackView ? root.stackView.currentItem.title : ""
            elide: Text.ElideRight
        }

        Row {
            id: rightSide
            Layout.rightMargin: 4
            Layout.leftMargin: 4
            spacing: 4
        }
    }
}
