import QtQml 2.12

QtObject {
    id: root

    property var site: null
    property var allUsers: []
    property bool ready: false

    function refresh() {
        var filters = {
            "sort": "id",
        }
        site.queryUsers(filters, function(reply) {
            if (reply.status == 200) {
                allUsers = reply.json.data
                ready = true
            }
        })
    }

    function usersFromIds(userIds) {
        var ret = []
        if (!ready) return ret

        var userIndex = 0
        for (var i = 0; i < userIds.length; i++) {
            for (; userIndex < allUsers.length; userIndex++) {
                var user = allUsers[userIndex]
                if (user.userId == userIds[i]) {
                    ret.push(user)
                    break;
                }
            }
            if (userIndex == allUsers.length) {
                console.warn("non trovate " + userIds[i])
                // non trovate, retorna array vacue pro signalar le error
                return []
            }
        }
        return ret
    }

    function userFromId(userId) {
        var users = usersFromIds([userId])
        return users.length == 1 ? users[0] : {}
    }
}
