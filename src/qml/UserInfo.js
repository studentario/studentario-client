var infoTypes = [
    {
        fieldName: "contactInfo",
        description: qsTr("Informationes de contacto"),
    },
    {
        fieldName: "birthDate",
        description: qsTr("Data de nascentia"),
    },
    {
        fieldName: "created",
        description: qsTr("Data de creation"),
    },
]

function byFieldName(name) {
    for (var i = 0; i < infoTypes.length; i++) {
        if (infoTypes[i].fieldName == name) return infoTypes[i]
    }
    return {}
}
