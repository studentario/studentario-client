import QtQml 2.12

JsonModelFilter {
    id: root
    property alias allUsers: root.allItems

    filterItemCb: function(user, pattern) {
        var words = user.name.split(' ')
        return anyStringStartsWith(words, pattern)
    }
}
