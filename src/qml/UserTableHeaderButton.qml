import QtQml.Models 2.12
import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "UserInfo.js" as UserInfo

HeaderButton {
    id: root

    property var wantedFields: []

    icon.source: "qrc:/icons/table"
    ToolTip.text: qsTr("Modifica aspecto del tabula")
    onClicked: chooserPopup.open()

    Dialog {
        id: chooserPopup

        property var selection: root.wantedFields

        title: qsTr("Datos a visualisar")
        width: 500
        height: 400
        y: parent.height
        margins: 4
        modal: true
        standardButtons: Dialog.Ok | Dialog.Cancel
        onAccepted: updateFields()

        ColumnLayout {
            anchors.fill: parent

            Label {
                Layout.fillWidth: true
                text: qsTr("Selige le columnas que debe esser monstrate")
                wrapMode: Text.Wrap
            }

            DraggableListView {
                id: listView
                Layout.fillWidth: true
                Layout.fillHeight: true
                clip: true
                model: sortedModel
                delegate: DraggableDelegate {
                    width: ListView.view.width
                    index: model.index
                    objectName: "checkbox " + index
                    CheckDelegate {
                        leftPadding: dragArea.width + 2
                        text: model.description
                        checked: chooserPopup.selection.indexOf(model.fieldName) >= 0
                        onCheckedChanged: chooserPopup.toggleField(model.fieldName, checked)

                        MouseArea {
                            id: dragArea
                            anchors {
                                margins: 4; left: parent.left;
                                top: parent.top; bottom:parent.bottom
                            }
                            width: height
                            enabled: !listView.moving
                            preventStealing: true
                            onPressed: listView.startDragging(this, mouse)

                            ToolButton {
                                anchors.fill: parent
                                z: -1
                                enabled: false
                                icon.source: "qrc:/icons/drag-handle"
                                flat: true
                            }
                        }
                    }
                }
                onMoveRequested: model.move(indexFrom, indexTo, 1)
            }
        }

        ListModel {
            id: sortedModel

            Component.onCompleted: {
                var source = UserInfo.infoTypes
                for (var i = 0; i < source.length; i++) {
                    append(source[i])
                }
            }
        }

        function toggleField(name, checked) {
            var i = selection.indexOf(name)
            if (i >= 0) {
                if (checked) return
                selection.splice(i, 1)
            } else if (i < 0) {
                if (!checked) return
                selection.push(name)
            }
        }

        function updateFields() {
            var fields = ["name"]
            var model = sortedModel
            for (var i = 0; i < model.count; i++) {
                var name = model.get(i).fieldName
                if (selection.indexOf(name) >= 0) {
                    fields.push(name)
                }
            }
            root.wantedFields = fields
        }
    }
}

