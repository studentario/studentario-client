import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import it.mardy.studentario 1.0

Page {
    id: root

    property var site: null
    property bool editable: false
    property int role: -1
    property alias sortOrder: sortButton.sortOrder

    header: TitleHeader {
        HeaderSearchButton { id: searchWidget }

        HeaderSortUsersButton {
            id: sortButton
            onSortChanged: root.refresh()
        }

        UserTableHeaderButton {
            id: tableButton
            visible: usersView.multiColumn
            wantedFields: ["name", "birthDate", "contactInfo"]
        }

        HeaderButton {
            text: "\u{1f4bb}" // Personal computer
            ToolTip.text: qsTr("Importa de un archivo CSV")
            visible: root.editable
            onClicked: {
                var page = stackView.push(Qt.resolvedUrl("ImportUsers.qml"), {
                    'site': root.site,
                    'role': root.role,
                })
                root.refreshOnClose(page)
            }
        }
    }
    title: {
        switch (root.role) {
        case Roles.Student: return qsTr("Studentes")
        case Roles.Parent: return qsTr("Parentes")
        case Roles.Teacher: return qsTr("Inseniantes")
        case Roles.Admin: return qsTr("Administratores")
        case Roles.Director: return qsTr("Directores")
        }
    }
    onRoleChanged: refresh()

    UsersView {
        id: usersView

        anchors.fill: parent
        editable: root.editable
        createUserComponent: createUserComponent
        wantedFields: tableButton.wantedFields
        model: userModel.filtered
        onDeletionRequested: root.deleteUser(modelData.userId, modelData.name)
        onUserClicked: {
            var page = stackView.push(Qt.resolvedUrl("ViewUser.qml"), {
                'site': root.site,
                'role': root.role,
                'userId': modelData.userId,
                'name': modelData.name,
                'editable': root.editable,
            })
            root.refreshOnClose(page)
        }
    }

    UserModel {
        id: userModel
        searchPattern: searchWidget.searchText
    }

    Component {
        id: createUserComponent
        Button {
            anchors.horizontalCenter: parent.horizontalCenter
            text: {
                switch (root.role) {
                case Roles.Student: return qsTr("Adde un nove studente")
                case Roles.Parent: return qsTr("Adde un nove parente")
                case Roles.Teacher: return qsTr("Adde un nove inseniante")
                case Roles.Admin: return qsTr("Adde un nove administrator")
                case Roles.Director: return qsTr("Adde un nove director")
                }
            }
            onClicked: {
                var page = stackView.push(Qt.resolvedUrl("EditUser.qml"), {
                    'site': root.site,
                    'role': root.role,
                })
                page.done.connect(function() {
                    root.refresh()
                    root.StackView.view.pop()
                })
            }
        }
    }

    Dialog {
        id: deleteConfirmationDialog
        property string userName: ""
        property int userId: -1

        title: qsTr("Confirma le remotion")
        standardButtons: Dialog.Yes | Dialog.No
        modal: true
        anchors.centerIn: parent

        Label {
            text: qsTr("Esque tu es secur que tu vole deler «%1»?").
                arg(deleteConfirmationDialog.userName)
            wrapMode: Text.Wrap
        }

        onAccepted: {
            site.deleteUser(userId, function(reply) {
                if (reply.status == 200) {
                    root.refresh()
                }
            })
        }
    }

    function deleteUser(userId, userName) {
        deleteConfirmationDialog.userName = userName
        deleteConfirmationDialog.userId = userId
        deleteConfirmationDialog.open()
    }

    function refresh() {
        var r = Roles.info(root.role)
        var filter = {
            "sort": root.sortOrder,
            [r.field]: 1,
        }
        site.queryUsers(filter, function(reply) {
            if (reply.status == 200) {
                userModel.allUsers = reply.json.data
            }
        })
    }

    function refreshOnClose(page) {
        page.done.connect(function() {
            root.refresh()
            root.StackView.view.pop()
        })
    }
}
