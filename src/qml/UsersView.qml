import QtQuick 2.12
import QtQuick.Controls 2.12
import it.mardy.studentario 1.0

ListView {
    id: root

    property bool editable: false
    property Component createUserComponent: null
    property bool multiColumn: formatter.wantedFields.length > 1
    property var wantedFields: []

    signal userClicked(int index, var modelData)
    signal deletionRequested(int index, var modelData)

    header: root.editable ? root.createUserComponent : null
    delegate: multiColumn ? wideUserDelegateComponent : userDelegateComponent

    ScrollBar.vertical: ScrollBar {}

    UserDataFormatter {
        id: formatter

        // TODO: obtene le dimensiones del configuration o in base al largor
        property var columnSizes: Array(wantedFields.length)
            .fill(root.width / wantedFields.length, 0)
        wantedFields: root.width > 300 ? root.wantedFields : ["name"]
    }

    Component {
        id: userDelegateComponent

        UserDelegate {
            width: root.width
            editable: root.editable
            name: modelData.name
            onDeletionRequested: root.deletionRequested(index, modelData)
            onClicked: root.userClicked(index, modelData)
        }
    }

    Component {
        id: wideUserDelegateComponent

        UserDelegate {
            id: wideUserDelegate

            property var userData: formatter.wantedFields, formatter.formatUserData(modelData)
            property var columnSizes: formatter.columnSizes

            width: root.width
            editable: root.editable
            onDeletionRequested: root.deletionRequested(index, modelData)
            onClicked: root.userClicked(index, modelData)

            contentItem: Row {
                spacing: 2

                Repeater {
                    model: wideUserDelegate.userData

                    Column {
                        anchors { top: parent.top; bottom: parent.bottom }
                        width: wideUserDelegate.columnSizes[index]

                        Label {
                            width: parent.width
                            elide: Text.ElideRight
                            text: modelData.title
                        }
                        Label {
                            width: parent.width
                            elide: Text.ElideRight
                            font.pointSize: 8
                            textFormat: Text.StyledText
                            text: modelData.subTitle
                            onLinkActivated: Qt.openUrlExternally(link)
                        }
                    }
                }
            }
        }
    }
}
