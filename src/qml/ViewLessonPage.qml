import QtQml.Models 2.12
import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import it.mardy.studentario 1.0

Page {
    id: root

    property var site: null
    property var lessonData: ({})
    property int lessonId: _existing ? lessonData.lessonId : -1
    property bool editable: true

    signal done()
    signal updated()

    property bool _editing: lessonId >= 0
    property bool _existing: !objectIsEmpty(lessonData)
    property int _pendingOps: 0

    header: TitleHeader {}
    title: _editing ?  qsTr("Modifica lection") : qsTr("Nove lection")

    onLessonDataChanged: update()

    ScrollView {
        anchors.fill: parent
        contentWidth: width
        contentHeight: layout.implicitHeight + 24

        ColumnLayout {
            id: layout
            anchors { fill: parent; margins: 12 }

            FlowLayout {
                Layout.fillWidth: true
                visible: _editing
                spacing: 24

                RowLayout {
                    Layout.fillWidth: true

                    Label {
                        text: qsTr("Data del lection:")
                    }

                    DatePicker {
                        id: datePicker
                        minDate: new Date()
                        selectedDate: new Date(root._existing ? lessonData.startTime : NaN)
                    }
                }

                TimeInterval {
                    id: timeInterval
                    Layout.fillWidth: true
                }
            }

            EditLessonMembers {
                id: memberEditor
                Layout.fillWidth: true
                site: root.site
                editable: root.editable
            }

            ErrorLabel {
                id: errorLabel
                Layout.fillWidth: true
            }

            Button {
                id: saveButton
                Layout.topMargin: 12
                Layout.bottomMargin: 12
                Layout.alignment: Qt.AlignHCenter
                Layout.minimumWidth: parent.width / 2
                text: _editing ? qsTr("Salva") : qsTr("Proxime")
                enabled: _pendingOps == 0
                onClicked: {
                    memberEditor.sync()
                    if (_editing) {
                        root.save()
                    } else {
                        root.openDatesPage()
                    }
                }
            }
        }
    }

    ListModel {
        id: intervalsModel
    }

    function objectIsEmpty(obj) {
        for (var i in obj) return false
        return true
    }

    function timeInMinutes(dateString) {
        var d = new Date(dateString)
        return d.getMinutes() + d.getHours() * 60
    }

    function update() {
        timeInterval.startMinutes = timeInMinutes(lessonData.startTime)
        timeInterval.endMinutes = timeInMinutes(lessonData.endTime)
        memberEditor.setupMembers(lessonData)
    }

    function openDatesPage() {
        var page = stackView.push(Qt.resolvedUrl("EditLessonDates.qml"), {
            'model': intervalsModel,
        })
        page.done.connect(function() {
            console.log("EditLessonDates returned done")
            createLessons()
            stackView.pop()
        })
    }

    function createLessons() {
        var lessons = []
        var n_lessons = intervalsModel.count
        for (var i = 0; i < n_lessons; i++) {
            var interval = intervalsModel.get(i)
            lessons.push({
                'startTime': Qt.formatDateTime(interval.start, Qt.ISODate),
                'endTime': Qt.formatDateTime(interval.end, Qt.ISODate),
            })
        }

        var request = {
            'lessons': lessons,
            'invitees': {
                'groups': memberEditor.groupIds,
                'students': memberEditor.studentIds,
                'teachers': memberEditor.teacherIds,
            }
        }
        site.createLesson(request, function(reply) {
            if (reply.status == 200) {
                root.done()
            } else {
                errorLabel.error = reply.json
            }
        })
    }

    function setMinutes(d, minutes) {
        var t = new Date(d)
        t.setHours(0)
        t.setMinutes(minutes)
        return t
    }

    function extractIds(listName, objName, keyName) {
        var invitees = lessonData[listName]
        var tmp = []
        for (var i = 0; i < invitees.length; i++) {
            var invitee = invitees[i]
            if (listName == 'groups' || invitees.invited) {
                tmp.push(invitee[objName][keyName])
            }
        }
        return tmp
    }

    function findInvitee(invitees, objName, keyName, itemId) {
        for (var i = 0; i < invitees.length; i++) {
            var invitee = invitees[i]
            var inviteeId = invitee[objName][keyName]
            if (inviteeId == itemId) return true
        }
        return false
    }

    function computeNewInvitees(changes, target, objName, idKey, itemIds) {
        var tmp = []
        for (var i = 0; i < itemIds.length; i++) {
            var itemId = itemIds[i]
            if (!findInvitee(lessonData[target], objName, idKey, itemId)) {
                tmp.push(itemId)
            }
        }
        if (tmp.length > 0) changes[target] = tmp
    }

    function computeLostInvitees(changes, target, objName, idKey, itemIds) {
        var tmp = []
        var oldItems = lessonData[target]
        for (var i = 0; i < oldItems.length; i++) {
            var invitee = oldItems[i]
            if (objName != 'group' && !invitee.invited) continue

            var inviteeId = invitee[objName][idKey]
            if (itemIds.indexOf(inviteeId) < 0) {
                tmp.push(inviteeId)
            }
        }
        if (tmp.length > 0) changes[target] = tmp
    }

    function save() {
        errorLabel.clear()
        _pendingOps = 0

        var callback = function(reply) {
            _pendingOps--
            if (reply.status == 200) {
                if (_pendingOps == 0) {
                    root.updated()
                    if (!errorLabel.visible) root.done()
                }
            } else {
                errorLabel.error = reply.json
            }
        }

        var startDate = datePicker.selectedDate
        var startTime = setMinutes(startDate, timeInterval.startMinutes)
        var endTime = setMinutes(startDate, timeInterval.endMinutes)
        var lesson = {}
        if (lessonData.startTime != startTime) {
            lesson['startTime'] = Qt.formatDateTime(startTime, Qt.ISODate)
        }
        if (lessonData.endTime != endTime) {
            lesson['endTime'] = Qt.formatDateTime(endTime, Qt.ISODate)
        }

        if (!objectIsEmpty(lesson)) {
            _pendingOps++
            site.updateLesson(root.lessonId, lesson, callback)
        }

        var newInvitees = {}
        computeNewInvitees(newInvitees, 'groups', 'group', 'groupId',
                           memberEditor.groupIds)
        computeNewInvitees(newInvitees, 'teachers', 'user', 'userId',
                           memberEditor.teacherIds)
        computeNewInvitees(newInvitees, 'students', 'user', 'userId',
                           memberEditor.studentIds)
        if (!objectIsEmpty(newInvitees)) {
            console.log("New invitees: " + JSON.stringify(newInvitees))
            _pendingOps++
            site.inviteToLesson(root.lessonId, newInvitees, callback)
        }

        var lostInvitees = {}
        computeLostInvitees(lostInvitees, 'groups', 'group', 'groupId',
                            memberEditor.groupIds)
        computeLostInvitees(lostInvitees, 'teachers', 'user', 'userId',
                            memberEditor.teacherIds)
        computeLostInvitees(lostInvitees, 'students', 'user', 'userId',
                            memberEditor.studentIds)
        if (!objectIsEmpty(lostInvitees)) {
            console.log("Lost invitees: " + JSON.stringify(lostInvitees))
            _pendingOps++
            site.uninviteFromLesson(root.lessonId, lostInvitees, callback)
        }
    }
}
