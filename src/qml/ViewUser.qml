import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import it.mardy.studentario 1.0
import "TagType.js" as TagType

Page {
    id: root

    property var site: null
    property int role: -1
    property int userId: -1
    property string name: ""
    property bool editable: false
    property bool changed: false

    signal done()

    property var _availableRoles: []

    header: TitleHeader {
        HeaderButton {
            text: "\u{1f589}" // Pencil
            ToolTip.text: qsTr("Modifica informationes")
            visible: root.editable
            onClicked: {
                var page = stackView.push(Qt.resolvedUrl("EditUser.qml"), {
                    'site': root.site,
                    'userId': root.userId,
                    'role': root.role,
                    'name': root.name,
                })
                page.done.connect(function() {
                    root.changed = page.changed
                    root.refresh()
                    root.StackView.view.pop()
                })
            }
        }
    }
    title: {
        switch (root.role) {
        case Roles.Student: return qsTr("Studente")
        case Roles.Parent: return qsTr("Parente (accompaniator)")
        case Roles.Teacher: return qsTr("Inseniante")
        case Roles.Admin: return qsTr("Administrator")
        case Roles.Director: return qsTr("Director")
        }
    }

    Component.onCompleted: root.refresh()

    ScrollView {
        anchors.fill: parent
        contentWidth: width

        ColumnLayout {
            anchors {
                fill: parent
                leftMargin: 12; rightMargin: 12
            }

            HelperLabel {
                Layout.fillWidth: true
                text: qsTr("Nomine complete")
            }
            Label {
                id: nameField
                Layout.fillWidth: true
                Layout.bottomMargin: 10
                horizontalAlignment: Qt.AlignHCenter
                font.pointSize: 14
                text: root.name
            }

            GroupBox {
                Layout.fillWidth: true
                Layout.bottomMargin: 10
                title: qsTr("Informationes de contacto")

                ContactInfoEditor {
                    id: contactInfoEditor
                    anchors.fill: parent
                    editable: false
                }
            }

            HelperLabel {
                Layout.fillWidth: true
                text: qsTr("Data de nascentia")
            }
            DateLabel {
                id: birthdateField
                Layout.fillWidth: true
                Layout.bottomMargin: 10
            }

            HelperLabel {
                Layout.fillWidth: true
                text: qsTr("Parolas-clave")
            }
            Label {
                id: keywordsField
                Layout.fillWidth: true
                Layout.bottomMargin: 10
                wrapMode: Text.Wrap
            }

            TagSelector {
                id: tagSelector
                Layout.fillWidth: true
                site: root.site
                editable: false
                tagType: TagType.byTypeId("level")
            }
        }
    }

    function refresh() {
        site.loadUser(userId, function(reply) {
            if (reply.status == 200) {
                var user = reply.json.data
                root.name = user.name
                contactInfoEditor.contactInfo = user.contactInfo
                keywordsField.text = user.keywords
                birthdateField.date = user.birthDate
                tagSelector.allTagsData = user.tags
            }
        })
    }
}
