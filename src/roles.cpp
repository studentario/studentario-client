/*
 * Copyright (C) 2021-2023 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Studentario.
 *
 * Studentario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Studentario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LinguaLonga.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "roles.h"

#include <QDebug>

using namespace Studentario;

Roles::Roles(QObject *parent):
    QObject(parent)
{
}

Roles::~Roles() = default;

QJsonObject Roles::info(Role role) const
{
    // Nota: tene lo synchronisate con le enum Role
    static const QJsonObject roleInfo[] = {
        {
            { "name", QT_TR_NOOP("Studente") },
            { "id", "student" },
            { "api", "students" },
            { "field", "isStudent" },
        },
        {
            { "name", QT_TR_NOOP("Genitor") },
            { "id", "parent" },
            { "api", "parents" },
            { "field", "isParent" },
        },
        {
            { "name", QT_TR_NOOP("Inseniante") },
            { "id", "teacher" },
            { "api", "teachers" },
            { "field", "isTeacher" },
        },
        {
            { "name", QT_TR_NOOP("Administrator") },
            { "id", "admin" },
            { "api", "admins" },
            { "field", "isAdmin" },
        },
        {
            { "name", QT_TR_NOOP("Director") },
            { "id", "director" },
            { "api", "directors" },
            { "field", "isDirector" },
        },
        {
            { "name", QT_TR_NOOP("Capo supreme") },
            { "id", "master" },
            { "field", "isMaster" },
        },
    };

    constexpr int roleCount = sizeof(roleInfo) / sizeof(QJsonObject);
    return (role >= 0 && role < roleCount) ? roleInfo[role] : QJsonObject {};
}
