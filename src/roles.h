/*
 * Copyright (C) 2021-2023 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Studentario.
 *
 * Studentario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Studentario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LinguaLonga.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef STUDENTARIO_ROLES_H
#define STUDENTARIO_ROLES_H

#include <QJsonObject>
#include <QObject>

namespace Studentario {

class Roles: public QObject
{
    Q_OBJECT

public:
    enum Role {
        Student = 0,
        Parent,
        Teacher,
        Admin,
        Director,
        Master,
    };
    Q_ENUM(Role)

    Roles(QObject *parent = 0);
    ~Roles();

    Q_INVOKABLE QJsonObject info(Role role) const;
};

} // namespace

#endif // STUDENTARIO_ROLES_H
