/*
 * Copyright (C) 2020-2023 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of LinguaLonga.
 *
 * LinguaLonga is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LinguaLonga is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LinguaLonga.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "standard_paths.h"

#include <QDebug>
#include <QDir>

using namespace LinguaLonga;

namespace LinguaLonga {

class StandardPathsPrivate
{
    Q_DECLARE_PUBLIC(StandardPaths)

public:
    StandardPathsPrivate(StandardPaths *q);

private:
    StandardPaths::StandardLocation m_base;
    StandardPaths *q_ptr;
};

} // namespace

StandardPathsPrivate::StandardPathsPrivate(StandardPaths *q):
    m_base(StandardPaths::HomeLocation),
    q_ptr(q)
{
}

StandardPaths::StandardPaths(QObject *parent):
    QObject(parent),
    d_ptr(new StandardPathsPrivate(this))
{
}

StandardPaths::~StandardPaths()
{
}

void StandardPaths::setBase(StandardPaths::StandardLocation base)
{
    Q_D(StandardPaths);
    d->m_base = base;
    Q_EMIT baseChanged();
}

StandardPaths::StandardLocation StandardPaths::base() const
{
    Q_D(const StandardPaths);
    return d->m_base;
}

QString StandardPaths::basePath() const
{
    Q_D(const StandardPaths);
    return QStandardPaths::writableLocation(
        static_cast<QStandardPaths::StandardLocation>(d->m_base));
}

QString StandardPaths::filePath(const QString &relativePath) const
{
    return QDir(basePath()).filePath(relativePath);
}
