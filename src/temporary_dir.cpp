/*
 * Copyright (C) 2020-2023 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of LinguaLonga.
 *
 * LinguaLonga is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LinguaLonga is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LinguaLonga.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "temporary_dir.h"

#include <QDebug>
#include <QDir>
#include <QTemporaryDir>

using namespace LinguaLonga;

namespace LinguaLonga {

class TemporaryDirPrivate
{
    Q_DECLARE_PUBLIC(TemporaryDir)

public:
    TemporaryDirPrivate(TemporaryDir *q);

    void update();

private:
    QScopedPointer<QTemporaryDir> m_tmpDir;
    QString m_parentPath;
    TemporaryDir *q_ptr;
};

} // namespace

TemporaryDirPrivate::TemporaryDirPrivate(TemporaryDir *q):
    m_tmpDir(new QTemporaryDir),
    q_ptr(q)
{
}

void TemporaryDirPrivate::update()
{
    QString templatePath;
    if (!m_parentPath.isEmpty()) {
        templatePath = m_parentPath + "/ll-XXXXXX";
        // Ensure that the parent directory exists
        if (Q_UNLIKELY(!QDir::root().mkpath(m_parentPath))) {
            qWarning() << "Could not create parent for temporary dir!";
            return;
        }
    }

    m_tmpDir.reset(new QTemporaryDir(templatePath));
}

TemporaryDir::TemporaryDir(QObject *parent):
    QObject(parent),
    d_ptr(new TemporaryDirPrivate(this))
{
}

TemporaryDir::~TemporaryDir()
{
}

void TemporaryDir::setParentPath(const QString &parentPath)
{
    Q_D(TemporaryDir);

    if (d->m_parentPath == parentPath) return;
    d->m_parentPath = parentPath;
    d->update();
    Q_EMIT parentPathChanged();
}

QString TemporaryDir::parentPath() const
{
    Q_D(const TemporaryDir);
    return d->m_parentPath;
}

QString TemporaryDir::path() const
{
    Q_D(const TemporaryDir);
    return d->m_tmpDir->path();
}
