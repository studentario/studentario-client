/*
 * Copyright (C) 2020-2023 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of LinguaLonga.
 *
 * LinguaLonga is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LinguaLonga is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LinguaLonga.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LINGUALONGA_TEMPORARY_DIR_H
#define LINGUALONGA_TEMPORARY_DIR_H

#include <QObject>

namespace LinguaLonga {

class TemporaryDirPrivate;
class TemporaryDir: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString parentPath READ parentPath WRITE setParentPath
               NOTIFY parentPathChanged)
    Q_PROPERTY(QString path READ path NOTIFY parentPathChanged)

public:
    TemporaryDir(QObject *parent = 0);
    ~TemporaryDir();

    void setParentPath(const QString &parentPath);
    QString parentPath() const;

    QString path() const;

Q_SIGNALS:
    void parentPathChanged();

private:
    QScopedPointer<TemporaryDirPrivate> d_ptr;
    Q_DECLARE_PRIVATE(TemporaryDir)
};

} // namespace

#endif // LINGUALONGA_TEMPORARY_DIR_H
