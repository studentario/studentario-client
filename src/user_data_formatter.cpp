/*
 * Copyright (C) 2024 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Studentario.
 *
 * Studentario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Studentario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Studentario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "user_data_formatter.h"

#include <QDate>
#include <QDebug>
#include <QJsonValue>
#include <QLocale>

using namespace Studentario;

namespace Studentario {

class UserDataFormatterPrivate {
    struct EntryData {
        QString title;
        QString subTitle;
    };

public:
    UserDataFormatterPrivate();

    static QString formatLink(const QStringRef &text, const QString &uri);
    EntryData extractContactInfo(const QJsonArray &array) const;
    QJsonValue formatField(const QJsonObject &userData,
                           const QString &field) const;
    QJsonArray format(const QJsonObject &userData) const;

private:
    Q_DECLARE_PUBLIC(UserDataFormatter)

    QStringList m_wantedFields;
    UserDataFormatter *q_ptr;
};

} // namespace

UserDataFormatterPrivate::UserDataFormatterPrivate()
{
}

QString UserDataFormatterPrivate::formatLink(const QStringRef &text,
                                             const QString &link)
{
    return QStringLiteral("<a href=\"%1\">%2</a>").arg(link).arg(text);
}

UserDataFormatterPrivate::EntryData
UserDataFormatterPrivate::extractContactInfo(const QJsonArray &array) const
{
    EntryData e;
    for (const QJsonValue &value: array) {
        QString text = value.toString();
        int colon = text.indexOf(':');
        if (Q_UNLIKELY(colon < 0)) continue;

        const QStringRef key = text.leftRef(colon);
        const QStringRef data = text.midRef(colon + 1);
        if (key == QStringLiteral("parentName")) {
            QString parentName = QUrl::fromPercentEncoding(data.toUtf8());
            if (e.title.isEmpty()) {
                e.title = parentName;
            }
        } else if (key == QStringLiteral("tel") ||
                   key == QStringLiteral("mailto")) {
            if (e.subTitle.isEmpty()) {
                e.subTitle = formatLink(data, text);
            }
        }
    }
    if (e.title.isEmpty()) {
        e.title = QStringLiteral("\u2014");
    }
    return e;
}

QJsonValue UserDataFormatterPrivate::formatField(const QJsonObject &userData,
                                                 const QString &field) const
{
    const QJsonValue value = userData[field];
    EntryData e;
    if (value.isString()) {
        e.title = value.toString();
        if (field == QStringLiteral("birthDate")) {
            QDate birthDate = QDate::fromString(e.title, Qt::ISODate);
            if (birthDate.isValid()) {
                QLocale locale;
                e.title = locale.toString(birthDate, QLocale::ShortFormat);
                QDate today = QDate::currentDate();
                int years = today.year() - birthDate.year();
                QDate thisYearsBirthday = birthDate.addYears(years);
                if (thisYearsBirthday > today) {
                    years--;
                }
                e.subTitle = QObject::tr("%1 annos", "Annos del usator", years)
                    .arg(years);
            }
        }
    } else if (value.isArray()) {
        const QJsonArray a = value.toArray();
        if (field == QStringLiteral("contactInfo")) {
            e = extractContactInfo(a);
        }
    }

    return QJsonObject {
        { QStringLiteral("title"), e.title },
        { QStringLiteral("subTitle"), e.subTitle },
    };
}

QJsonArray UserDataFormatterPrivate::format(const QJsonObject &userData) const
{
    QJsonArray ret;
    for (const QString &field: m_wantedFields) {
        ret.append(formatField(userData, field));
    }
    return ret;
}

UserDataFormatter::UserDataFormatter(QObject *parent):
    QObject(parent),
    d_ptr(new UserDataFormatterPrivate())
{
}

UserDataFormatter::~UserDataFormatter() = default;

QJsonArray UserDataFormatter::formatUserData(const QJsonObject &userData) const
{
    Q_D(const UserDataFormatter);
    return d->format(userData);
}

void UserDataFormatter::setWantedFields(const QStringList &wantedFields)
{
    Q_D(UserDataFormatter);

    d->m_wantedFields = wantedFields;
    Q_EMIT wantedFieldsChanged();
}

QStringList UserDataFormatter::wantedFields() const
{
    Q_D(const UserDataFormatter);
    return d->m_wantedFields;
}
