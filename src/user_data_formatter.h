/*
 * Copyright (C) 2024 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Studentario.
 *
 * Studentario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Studentario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Studentario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STUDENTARIO_USER_DATA_FORMATTER_H
#define STUDENTARIO_USER_DATA_FORMATTER_H

#include <QJsonArray>
#include <QJsonObject>
#include <QObject>
#include <QScopedPointer>
#include <QStringList>
#include <QUrl>

namespace Studentario {

class UserDataFormatterPrivate;
class UserDataFormatter: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QStringList wantedFields READ wantedFields WRITE setWantedFields
               NOTIFY wantedFieldsChanged)

public:
    UserDataFormatter(QObject *parent = nullptr);
    ~UserDataFormatter();

    Q_INVOKABLE QJsonArray formatUserData(const QJsonObject &userData) const;

    void setWantedFields(const QStringList &wantedFields);
    QStringList wantedFields() const;

Q_SIGNALS:
    void wantedFieldsChanged();

private:
    Q_DECLARE_PRIVATE(UserDataFormatter)
    QScopedPointer<UserDataFormatterPrivate> d_ptr;
};

} // namespace

#endif // STUDENTARIO_USER_DATA_FORMATTER_H
