/*
 * Copyright (C) 2023 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Studentario.
 *
 * Studentario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Studentario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Studentario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "utils.h"

#include <QDebug>

using namespace Studentario;

Utils::Utils(QObject *parent):
    QObject(parent)
{
}

Utils::~Utils() = default;

QString Utils::formatDuration(int milliseconds) const
{
	int totalSeconds = milliseconds / 1000;
	int totalMinutes = totalSeconds / 60;
    int totalHours = totalMinutes / 60;
    int days = totalHours / 24;
	int hours = totalHours % 24;
	int minutes = totalMinutes % 60;

    if (days > 0) {
        return tr("%1d %2h").arg(days).arg(hours);
    } else {
        return (hours > 0 ?  tr("%1h %2m").arg(hours) : tr("%1m"))
            .arg(minutes, 2, 10, QChar('0'));
    }
}
