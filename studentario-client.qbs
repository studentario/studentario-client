import qbs 1.0

Project {
    name: "studentario"

    property bool buildTests: false
    property bool enableCoverage: false
    property string localConf: ""
    property bool buildAppImage: false

    references: [
        "tests/tests.qbs",
    ]
    qbsSearchPaths: "qbs"

    QtGuiApplication {
        name: "it.mardy.studentario.client"
        targetName: "studentario"
        install: true

        files: [
            "data/i18n/*.ts",
            "data/icons/icons.qrc",
            "data/studentario.desktop",
            "src/application.cpp",
            "src/application.h",
            "src/collection_change_recorder.cpp",
            "src/collection_change_recorder.h",
            "src/csv_model.cpp",
            "src/csv_model.h",
            "src/csv_parser.cpp",
            "src/csv_parser.h",
            "src/error.cpp",
            "src/error.h",
            "src/file_io.cpp",
            "src/file_io.h",
            "src/json_storage.cpp",
            "src/json_storage.h",
            "src/main.cpp",
            "src/planner_model.cpp",
            "src/planner_model.h",
            "src/roles.cpp",
            "src/roles.h",
            "src/standard_paths.cpp",
            "src/standard_paths.h",
            "src/temporary_dir.cpp",
            "src/temporary_dir.h",
            "src/types.cpp",
            "src/types.h",
            "src/user_data_formatter.cpp",
            "src/user_data_formatter.h",
            "src/utils.cpp",
            "src/utils.h",
        ]

        Group {
            files: [
                "data/qtquickcontrols2.conf",
                "src/qml/*.js",
                "src/qml/*.qml",
            ]
            fileTags: ["qt.core.resource_data"]
        }

        Group {
            files: [
                (project.localConf != "" ?
                 project.localConf : "data/studentario.conf"),
            ]
            fileTags: ["qt.core.resource_data"]
            Qt.core.resourcePrefix: "/config/"
        }

        Group {
            fileTagsFilter: "qm"
            fileTags: ["qt.core.resource_data"]
            Qt.core.resourcePrefix: "/i18n/"
        }

        Group {
            files: "data/icons/studentario.svg"
            fileTags: "freedesktop.appIcon"
        }

        Group {
            condition: project.buildAppImage
            files: "src/desktop-integration.sh"
            fileTags: "application"
        }

        Depends { name: "Qt.quick" }
        Depends { name: "Qt.svg" }
        Depends { name: "buildconfig" }
        Depends { name: "freedesktop" }

        freedesktop.appName: "Studentario"
    }

    AutotestRunner {
        name: "check"
        environment: [ "QT_QPA_PLATFORM=offscreen" ]
        Depends { productTypes: ["coverage-clean"] }
    }
}
