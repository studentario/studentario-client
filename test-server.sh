#! /bin/bash

SERVER_BUILD_DIR="../studentario-server/build/"
PIN=

while [ $# -gt 0 ]
do
	case "$1" in
    (-b) SERVER_BUILD_DIR="$2"; shift;;
    (-p) PIN="$2"; shift;;
    esac
    shift
done

CONFIG_FILE="$(mktemp /tmp/studentario.conf.XXXXXX)"

cat > $CONFIG_FILE <<-EOF
[Cutelyst]
DatabaseName = "$HOME/studentario.db"
Testing = true
EOF

createMaster () {
    sleep 2
    read LOGIN PASSWORD < <(curl -s \
        -H "Content-Type: application/json" \
        -X POST \
        http://localhost:3000/testing/createMaster \
        | jq -j '.login," ",.password')
    if [ -n "$PIN" ]; then
        AUTH_TOKEN=$(curl -s \
            -H "Content-Type: application/json" \
            -d '{"login":"'$LOGIN'","password":"'$PASSWORD'"}' \
            http://localhost:3000/api/v1/login \
            | jq -r '.data.authenticationToken')
        curl -s \
            -H "Content-Type: application/json" \
            -H "Authorization: Basic $AUTH_TOKEN" \
            -d '{"password":"'$PASSWORD'","newPin":"'$PIN'"}' \
            http://localhost:3000/api/v1/users/me/updatePin
    fi
}

createMaster &

cleanUp() {
    echo "Cleaning up."
    rm "$CONFIG_FILE"
}

trap cleanUp EXIT INT TERM

cd "$SERVER_BUILD_DIR"
cutelyst2 -r --server --app-file src/libStudentario.so -- --chdir .. --ini "$CONFIG_FILE"

trap - EXIT
