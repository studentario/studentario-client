import qbs 1.0

Project {
    condition: project.buildTests

    UnitTest {
        classFileName: "csv_parser"
    }

    UnitTest {
        classFileName: "file_io"
    }

    UnitTest {
        classFileName: "json_storage"
        Depends { name: 'Qt.qml' }
    }

    UnitTest {
        classFileName: "planner_model"
        Depends { name: 'Qt.qml' }
    }

    UnitTest {
        classFileName: "standard_paths"
    }

    UnitTest {
        classFileName: "temporary_dir"
    }

    CoverageClean {
        condition: project.enableCoverage
    }

    CoverageReport {
        condition: project.enableCoverage
        extractPatterns: [ '*/src/*.cpp', '*/lib/*.cpp' ]
    }
}
