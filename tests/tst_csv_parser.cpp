/*
 * Copyright (C) 2023 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Studentario.
 *
 * Studentario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Studentario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Studentario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "csv_parser.h"

#include <QBuffer>
#include <QByteArray>
#include <QDebug>
#include <QIODevice>
#include <QRegularExpression>
#include <QScopedPointer>
#include <QTest>

using namespace Studentario;

class CsvParserTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testInitialStatus();
    void testProperties();
    void testParse_data();
    void testParse();
    void testParseError_data();
    void testParseError();
};

void CsvParserTest::testInitialStatus()
{
    CsvParser parser;
    QVERIFY(!parser.inputDevice());
    QCOMPARE(parser.columnTitles(), QStringList());
    QVERIFY(!parser.hasError());
    QVERIFY(parser.atEnd());
}

void CsvParserTest::testProperties()
{
    CsvParser parser;

    QBuffer device;
    device.open(QBuffer::ReadOnly);
    parser.setInputDevice(&device);
    QCOMPARE(parser.inputDevice(), &device);
}

void CsvParserTest::testParse_data()
{
    using Rows = QList<QStringList>;
    using B = QByteArray;
    QTest::addColumn<QByteArray>("csvData");
    QTest::addColumn<QStringList>("expectedTitles");
    QTest::addColumn<Rows>("expectedRows");

    QTest::newRow("empty") <<
        B("") << QStringList {} << Rows {};

    QTest::newRow("1 row, text only") <<
        B("Nomine,Familia,Pais\n"
          "Petro,Rubie,Landia") <<
        QStringList { "Nomine", "Familia", "Pais" } <<
        Rows {
            { "Petro", "Rubie", "Landia" },
        };

    QTest::newRow("more rows, text only") <<
        B("Nomine,Familia,Pais\n"
          "Mario,Iuvene,Pololandia\n"
          "Lisa,Bella,Alicubi\n"
          "Dominico,Fatigate,Semprelandia\n"
          "Petro,Rubie,Landia\n") <<
        QStringList { "Nomine", "Familia", "Pais" } <<
        Rows {
            { "Mario", "Iuvene", "Pololandia" },
            { "Lisa", "Bella", "Alicubi" },
            { "Dominico", "Fatigate", "Semprelandia" },
            { "Petro", "Rubie", "Landia" },
        };

    QTest::newRow("some empty fields") <<
        B("Nomine,Familia,Pais\n"
          "Petro,,Landia\n"
          ",Iuvene,\n") <<
        QStringList { "Nomine", "Familia", "Pais" } <<
        Rows {
            { "Petro", QString(), "Landia" },
            { QString(), "Iuvene", QString() },
        };

    QTest::newRow("with quotes") <<
        B(R"(Nomine,"Familia","Pais")""\n"
          R"(Petro,"Le Rubie",Landia)") <<
        QStringList { "Nomine", "Familia", "Pais" } <<
        Rows {
            { "Petro", "Le Rubie", "Landia" },
        };

    QTest::newRow("with quoted newline") <<
        B("Nomine,Familia,Description\n"
          "Petro,\"Le\nRubie\",\"Tel.: 12345678\nPosta: petro@rubies.net\"") <<
        QStringList { "Nomine", "Familia", "Description" } <<
        Rows {
            {
                "Petro", "Le\nRubie",
                "Tel.: 12345678\nPosta: petro@rubies.net"
            },
        };

    QTest::newRow("with quoted quote") <<
        B("Nomine,Familia,Description\n"
          R"("""initio","in le""medie","al fin""")") <<
        QStringList { "Nomine", "Familia", "Description" } <<
        Rows {
            { "\"initio", "in le\"medie", "al fin\"" },
        };
}

void CsvParserTest::testParse()
{
    QFETCH(QByteArray, csvData);
    QFETCH(QStringList, expectedTitles);
    QFETCH(QList<QStringList>, expectedRows);

    QBuffer buffer(&csvData);
    buffer.open(QBuffer::ReadOnly);
    CsvParser parser;
    parser.setInputDevice(&buffer);

    QStringList titles = parser.columnTitles();
    QCOMPARE(titles, expectedTitles);

    int i = 0;
    QStringList row;
    while (row = parser.readNextRow(), !row.isEmpty()) {
        QVERIFY2(expectedRows.length() > i,
                 "Parsed more rows than expected!");
        const auto expectedRow = expectedRows[i++];
        QCOMPARE(row, expectedRow);
    }
    QCOMPARE(i, expectedRows.length());
    QVERIFY(parser.atEnd());
}

void CsvParserTest::testParseError_data()
{
    using B = QByteArray;
    QTest::addColumn<QByteArray>("csvData");
    QTest::addColumn<QString>("expectedWarning");

    QTest::newRow("unterminated quote") <<
        B("Uno,\"duo") <<
        "Error reading line from CSV";

    QTest::newRow("extra chars") <<
        B("Uno,\"duo\" con extra") <<
        "Invalid CSV format";
}

void CsvParserTest::testParseError()
{
    QFETCH(QByteArray, csvData);
    QFETCH(QString, expectedWarning);

    if (!expectedWarning.isEmpty()) {
        QRegularExpression pattern(expectedWarning);
        QTest::ignoreMessage(QtWarningMsg, pattern);
    }

    QBuffer buffer(&csvData);
    buffer.open(QBuffer::ReadOnly);
    CsvParser parser;
    parser.setInputDevice(&buffer);

    QStringList titles = parser.columnTitles();
    QVERIFY(titles.isEmpty());

    QVERIFY(parser.hasError());
}

QTEST_GUILESS_MAIN(CsvParserTest)

#include "tst_csv_parser.moc"
