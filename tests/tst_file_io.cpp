/*
 * Copyright (C) 2020-2023 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of LinguaLonga.
 *
 * LinguaLonga is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LinguaLonga is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LinguaLonga.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "file_io.h"

#include <QDebug>
#include <QFile>
#include <QScopedPointer>
#include <QSignalSpy>
#include <QStandardPaths>
#include <QTest>

using namespace LinguaLonga;

class FileIOTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testProperties();
    void testRead();
    void testReload();
    void testWrite();
};

void FileIOTest::testProperties()
{
    QScopedPointer<FileIO> fileIO(new FileIO);

    QVERIFY(fileIO->property("filePath").toString().isEmpty());
    QVERIFY(fileIO->property("contents").toByteArray().isEmpty());

    QSignalSpy filePathChanged(fileIO.data(), &FileIO::filePathChanged);

    QString inexistingFile("/i-do-not-exist.txt");
    fileIO->setProperty("filePath", inexistingFile);
    QCOMPARE(filePathChanged.count(), 1);
    QCOMPARE(fileIO->property("filePath").toString(), inexistingFile);
    QVERIFY(fileIO->property("contents").toByteArray().isEmpty());
}

void FileIOTest::testRead()
{
    QTemporaryDir tmpDir;
    QVERIFY(tmpDir.isValid());

    QString firstFile = QStringLiteral("first");
    QString secondFile = QStringLiteral("second");

    /* Prepare the files */
    QStringList fileNames { firstFile, secondFile };
    for (const QString &fileName: fileNames) {
        QFile file(tmpDir.filePath(fileName));
        QVERIFY(file.open(QIODevice::WriteOnly));
        file.write(fileName.toUtf8());
    }

    FileIO fileIO;
    QSignalSpy filePathChanged(&fileIO, &FileIO::filePathChanged);
    QSignalSpy contentsChanged(&fileIO, &FileIO::contentsChanged);

    fileIO.setFilePath(tmpDir.filePath(firstFile));
    QCOMPARE(filePathChanged.count(), 1);
    QCOMPARE(contentsChanged.count(), 1);
    QCOMPARE(fileIO.contents(), firstFile.toUtf8());
    filePathChanged.clear();
    contentsChanged.clear();

    /* Now change the path */
    fileIO.setFilePath(tmpDir.filePath(secondFile));
    QCOMPARE(filePathChanged.count(), 1);
    QCOMPARE(contentsChanged.count(), 1);
    QCOMPARE(fileIO.contents(), secondFile.toUtf8());
}

void FileIOTest::testReload()
{
    QTemporaryDir tmpDir;
    QVERIFY(tmpDir.isValid());

    QString fileName = QStringLiteral("a file");

    {
        QFile file(tmpDir.filePath(fileName));
        QVERIFY(file.open(QIODevice::WriteOnly));
        file.write(fileName.toUtf8());
    }

    FileIO fileIO;
    QSignalSpy contentsChanged(&fileIO, &FileIO::contentsChanged);

    fileIO.setFilePath(tmpDir.filePath(fileName));
    QCOMPARE(contentsChanged.count(), 1);
    QCOMPARE(fileIO.contents(), fileName.toUtf8());
    contentsChanged.clear();

    /* Now change the file contents and call reload() */
    QSignalSpy filePathChanged(&fileIO, &FileIO::filePathChanged);
    QByteArray newContents = "Some new text";
    {
        QFile file(tmpDir.filePath(fileName));
        QVERIFY(file.open(QIODevice::WriteOnly));
        file.write(newContents);
    }
    fileIO.reload();

    QCOMPARE(contentsChanged.count(), 1);
    QCOMPARE(fileIO.contents(), newContents);
    QCOMPARE(filePathChanged.count(), 0);
}

void FileIOTest::testWrite()
{
    QTemporaryDir tmpDir;
    QVERIFY(tmpDir.isValid());

    QString firstFile = QStringLiteral("one");
    QString secondFile = QStringLiteral("two");

    FileIO fileIO;
    QSignalSpy filePathChanged(&fileIO, &FileIO::filePathChanged);
    QSignalSpy contentsChanged(&fileIO, &FileIO::contentsChanged);

    fileIO.setFilePath(tmpDir.filePath(firstFile));
    QCOMPARE(filePathChanged.count(), 1);
    filePathChanged.clear();
    contentsChanged.clear();

    // Prepare the contents
    QByteArray helloWorld = "Hello, world!\n";
    QByteArray fileContents;
    fileContents.reserve(helloWorld.length() * 5000);
    for (int i = 0; i < 5000; i++) {
        fileContents.append(helloWorld);
    }

    // Write them
    fileIO.setContents(fileContents);
    QCOMPARE(contentsChanged.count(), 1);
    QCOMPARE(fileIO.contents(), fileContents);
    filePathChanged.clear();
    contentsChanged.clear();

    /* Check that the file has actually been written */
    {
        QFile file(tmpDir.filePath(firstFile));
        QVERIFY(file.open(QIODevice::ReadOnly));
        QCOMPARE(file.readAll(), fileContents);
    }

    /* Now change the path, ensure that the contents are gone */
    fileIO.setFilePath(tmpDir.filePath(secondFile));
    QCOMPARE(filePathChanged.count(), 1);
    QCOMPARE(contentsChanged.count(), 1);
    QVERIFY(fileIO.contents().isEmpty());
    filePathChanged.clear();
    contentsChanged.clear();

    /* Back to the first file: write a shorter text */
    fileIO.setFilePath(tmpDir.filePath(firstFile));
    QCOMPARE(filePathChanged.count(), 1);
    QCOMPARE(contentsChanged.count(), 1);
    QCOMPARE(fileIO.contents(), fileContents);
    filePathChanged.clear();
    contentsChanged.clear();
    fileIO.setContents(helloWorld);
    QCOMPARE(contentsChanged.count(), 1);
    contentsChanged.clear();

    /* Check that the file has actually been written */
    {
        QFile file(tmpDir.filePath(firstFile));
        QVERIFY(file.open(QIODevice::ReadOnly));
        QCOMPARE(file.readAll(), helloWorld);
    }
}

QTEST_GUILESS_MAIN(FileIOTest)

#include "tst_file_io.moc"
