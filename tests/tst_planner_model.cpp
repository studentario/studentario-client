/*
 * Copyright (C) 2023 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Studentario.
 *
 * Studentario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Studentario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Studentario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "planner_model.h"

#include <QDebug>
#include <QScopedPointer>
#include <QTemporaryDir>
#include <QTest>

using namespace Studentario;

class PlannerModelTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testOverlapping_data();
    void testOverlapping();
};

void PlannerModelTest::testOverlapping_data()
{
    using Activities = QList<QVariantMap>;
    auto T = [](quint64 secs) { return QDateTime::fromSecsSinceEpoch(secs); };

    QTest::addColumn<QStringList>("columnValues");
    QTest::addColumn<Activities>("activities");

    QTest::newRow("no overlapping") <<
        QStringList { "col1" } <<
        Activities {
            {
                { "key", "col1" },
                { "startTime", T(10) },
                { "endTime", T(20) },
                { "expectedCount", 0 },
            },
            {
                { "key", "col1" },
                { "startTime", T(30) },
                { "endTime", T(40) },
                { "expectedCount", 0 },
            },
        };

    QTest::newRow("full overlapping") <<
        QStringList { "col1" } <<
        Activities {
            {
                { "key", "col1" },
                { "startTime", T(10) },
                { "endTime", T(20) },
                { "expectedCount", 2 },
            },
            {
                { "key", "col1" },
                { "startTime", T(10) },
                { "endTime", T(20) },
                { "expectedCount", 2 },
            },
        };

    QTest::newRow("partial overlapping") <<
        QStringList { "col1" } <<
        Activities {
            {
                { "key", "col1" },
                { "startTime", T(10) },
                { "endTime", T(20) },
                { "expectedCount", 2 },
            },
            {
                { "key", "col1" },
                { "startTime", T(15) },
                { "endTime", T(25) },
                { "expectedCount", 2 },
            },
            { // not overlapping
                { "key", "col1" },
                { "startTime", T(50) },
                { "endTime", T(100) },
                { "expectedCount", 0 },
            },
        };

    QTest::newRow("many overlappings, single col") <<
        QStringList { "col1" } <<
        Activities {
            {
                { "key", "col1" },
                { "startTime", T(16) },
                { "endTime", T(18) },
                { "expectedCount", 3 },
            },
            {
                { "key", "col1" },
                { "startTime", T(115) },
                { "endTime", T(125) },
                { "expectedCount", 2 },
            },
            {
                { "key", "col1" },
                { "startTime", T(15) },
                { "endTime", T(25) },
                { "expectedCount", 3 },
            },
            { // not overlapping
                { "key", "col1" },
                { "startTime", T(50) },
                { "endTime", T(100) },
                { "expectedCount", 0 },
            },
            {
                { "key", "col1" },
                { "startTime", T(10) },
                { "endTime", T(20) },
                { "expectedCount", 3 },
            },
            {
                { "key", "col1" },
                { "startTime", T(121) },
                { "endTime", T(135) },
                { "expectedCount", 2 },
            },
            {
                { "key", "col1" },
                { "startTime", T(21) },
                { "endTime", T(35) },
                { "expectedCount", 2 },
            },
        };

    QTest::newRow("no overlapping, two columns") <<
        QStringList { "col1", "col2" } <<
        Activities {
            {
                { "key", "col1" },
                { "startTime", T(10) },
                { "endTime", T(20) },
                { "expectedCount", 0 },
            },
            {
                { "key", "col2" },
                { "startTime", T(11) },
                { "endTime", T(21) },
                { "expectedCount", 0 },
            },
        };

    QTest::newRow("many overlappings, two columns") <<
        QStringList { "col1", "col2" } <<
        Activities {
            {
                { "key", "col1" },
                { "startTime", T(16) },
                { "endTime", T(18) },
                { "expectedCount", 0 },
            },
            {
                { "key", "col1" },
                { "startTime", T(115) },
                { "endTime", T(125) },
                { "expectedCount", 2 },
            },
            {
                { "key", "col2" },
                { "startTime", T(15) },
                { "endTime", T(25) },
                { "expectedCount", 2 },
            },
            { // not overlapping
                { "key", "col1" },
                { "startTime", T(50) },
                { "endTime", T(100) },
                { "expectedCount", 0 },
            },
            {
                { "key", "col2" },
                { "startTime", T(10) },
                { "endTime", T(20) },
                { "expectedCount", 2 },
            },
            {
                { "key", "col1" },
                { "startTime", T(121) },
                { "endTime", T(135) },
                { "expectedCount", 2 },
            },
            {
                { "key", "col2" },
                { "startTime", T(21) },
                { "endTime", T(35) },
                { "expectedCount", 2 },
            },
        };
}

void PlannerModelTest::testOverlapping()
{
    QFETCH(QStringList, columnValues);
    QFETCH(QList<QVariantMap>, activities);

    PlannerModel planner;
    planner.setStartTime(QDateTime::fromSecsSinceEpoch(0));
    planner.setEndTime(QDateTime::fromSecsSinceEpoch(1000));
    planner.setColumnKey("key");
    planner.setColumnValues(columnValues);

    for (const QVariantMap &activity: activities) {
        planner.addActivity(activity);
    }

    QTest::qWait(5); // le computation es exequite in le cyclo de eventos

    const QList<QObject*> laidOutActivities = planner.activities();
    QCOMPARE(laidOutActivities.count(), activities.count());
    for (QObject *o: laidOutActivities) {
        // Uncomment for debugging
        //qDebug() << "starttime:" << o->property("startTime").toDateTime().toSecsSinceEpoch();
        QCOMPARE(o->property("overlapCount").toInt(),
                 o->property("expectedCount").toInt());
    }
}

QTEST_GUILESS_MAIN(PlannerModelTest)

#include "tst_planner_model.moc"
