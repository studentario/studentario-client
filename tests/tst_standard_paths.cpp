/*
 * Copyright (C) 2020-2023 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of LinguaLonga.
 *
 * LinguaLonga is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LinguaLonga is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LinguaLonga.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "standard_paths.h"

#include <QDebug>
#include <QFile>
#include <QStandardPaths>
#include <QTest>

using namespace LinguaLonga;

class StandardPathsTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void initTestCase();
    void testProperties();
    void testFilePath_data();
    void testFilePath();
};

void StandardPathsTest::initTestCase()
{
    qputenv("HOME", "/mydir");
    qunsetenv("XDG_DATA_HOME");
}

void StandardPathsTest::testProperties()
{
    StandardPaths *paths = new StandardPaths;

    QCOMPARE(paths->base(), StandardPaths::HomeLocation);
    QCOMPARE(paths->basePath(), QString("/mydir"));
    paths->setBase(StandardPaths::DataLocation);
    QCOMPARE(paths->base(), StandardPaths::DataLocation);
    QCOMPARE(paths->basePath(),
             QString("/mydir/.local/share/tst_standard_paths"));
    delete paths;
}

void StandardPathsTest::testFilePath_data()
{
    QTest::addColumn<StandardPaths::StandardLocation>("base");
    QTest::addColumn<QString>("relativePath");
    QTest::addColumn<QString>("expectedFilePath");

    QDir baseDir =
        QStandardPaths::writableLocation(QStandardPaths::MusicLocation);
    QTest::newRow("hidden file") <<
        StandardPaths::MusicLocation <<
        ".index.txt" <<
        baseDir.filePath(".index.txt");

}

void StandardPathsTest::testFilePath()
{
    QFETCH(StandardPaths::StandardLocation, base);
    QFETCH(QString, relativePath);
    QFETCH(QString, expectedFilePath);

    StandardPaths paths;
    paths.setBase(base);
    QCOMPARE(paths.filePath(relativePath), expectedFilePath);
}

QTEST_GUILESS_MAIN(StandardPathsTest)

#include "tst_standard_paths.moc"
