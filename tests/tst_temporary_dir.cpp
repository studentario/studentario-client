/*
 * Copyright (C) 2020-2023 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of LinguaLonga.
 *
 * LinguaLonga is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LinguaLonga is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LinguaLonga.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "temporary_dir.h"

#include <QDebug>
#include <QFile>
#include <QSignalSpy>
#include <QTest>

using namespace LinguaLonga;

class TemporaryDirTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testProperties();
    void testAutoRemoval();
    void testParentPath();
    void testParentPathReset();
};

void TemporaryDirTest::testProperties()
{
    TemporaryDir tmpDir;
    QVERIFY(!tmpDir.property("path").toString().isEmpty());
    QVERIFY(tmpDir.property("parentPath").toString().isEmpty());
}

void TemporaryDirTest::testAutoRemoval()
{
    TemporaryDir *tmpDir = new TemporaryDir;

    QString filePath = tmpDir->path() + "/dummy.txt";

    QFile file(filePath);
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write("Hello world");
    file.close();

    QVERIFY(QFile::exists(filePath));

    // delete the object and verify that the dir is clean
    delete tmpDir;
    QVERIFY(!QFile::exists(filePath));
}

void TemporaryDirTest::testParentPath()
{
    QTemporaryDir testDir;

    TemporaryDir tmpDir;
    QSignalSpy parentPathChanged(&tmpDir, &TemporaryDir::parentPathChanged);

    QString parentPath = testDir.filePath("some/sub/dir");
    tmpDir.setParentPath(parentPath);
    QCOMPARE(parentPathChanged.count(), 1);
    QCOMPARE(tmpDir.parentPath(), parentPath);

    QVERIFY(tmpDir.path().startsWith(parentPath));

    // Set it to the same value, verify that no notification is emitted
    parentPathChanged.clear();
    tmpDir.setParentPath(parentPath);
    QCOMPARE(parentPathChanged.count(), 0);
}

void TemporaryDirTest::testParentPathReset()
{
    QTemporaryDir testDir;

    TemporaryDir tmpDir;

    QString parentPath = testDir.filePath("here");
    tmpDir.setParentPath(parentPath);

    QSignalSpy parentPathChanged(&tmpDir, &TemporaryDir::parentPathChanged);
    tmpDir.setParentPath(QString());
    QCOMPARE(parentPathChanged.count(), 1);
    QVERIFY(tmpDir.parentPath().isEmpty());

    QVERIFY(!tmpDir.path().startsWith(parentPath));
}

QTEST_GUILESS_MAIN(TemporaryDirTest)

#include "tst_temporary_dir.moc"
